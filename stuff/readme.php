<?php
  $site_root = "../";
  $page_title = 'Some information abot the edu.kde.org web site';
  
  include ( "header.inc" );
?>

<p>21st July 2001, <a href="mailto:howells AT kde DOT org">Chris Howells</a><br />
Update 19th March 2003, <a href="mailto:annma AT kde DOT org">Anne-Marie Mahfouf</a><br/>
Update 18th September 2005, <a href="mailto:toscano DOT pino AT tiscali DOT it">Pino Toscano</a></p>

<p>Please send any further questions to
<a href="mailto kde-edu AT kde DOT org">kde-edu@kde.org</a>.</p>

<p>It would be appreciated if you could bear these things in mind when working
on the web site.</p>

<ol>
<li>
The module to check out is <tt>/trunk/www/sites/edu</tt>.
<pre>
$ cd trunk
$ svn co www/sites/edu
</pre>
Don't forget to do a
<pre>$svn up</pre>
before every change you want to make.
<br />
<br />
</li>
<li>
The site is designed to validate as strict XHTML. XHTML is an
extension to HTML 4. It says that:
  <ol type="a">
    <li>Every tag must have a closing tag <code>&lt;li&gt;&lt;/li&gt;</code></li>
    <li>Tag properties must be contained in quote marks (e.g.
      <code>&lt;img src=""...</code>)</li>
    <li>The tags that don't have a closing tag (like <code>&lt;br&gt;</code> (HTML)) must be written as <code>&lt;br/&gt;</code> (XHTML)</li>
  </ol>
Two examples of those:<br/>
<code>&lt;li&gt;</code> does not have a closing tag under HTML 4, while it must
have one under XHTML (e.g. <code>&lt;li&gt;...&lt;/li&gt;</code>)<br />
The <code>&lt;img&gt;</code> tag should be closed, like:<br/>
<code>&lt;img alt="Kiten" align="top" src="pics/projects/cr16-app-kiten.png"/&gt;</code>

<br />
<br />
</li>
<li>
Please do <b>not</b> use GIF images. GIF images are evil! (no, really). This is
due to patents on the image file format. Please use PNG (or possible JPEG)
instead.
<br />
<br />
</li>
<li>
The site uses quite a lot of PHP functions and classes such as
<code>AppInfo</code>, <code>ImageGallery/EduGallery</code> (for screnshot pics
galleries), and others. Look at other pages to copy/paste the correct code.<br/>
You can find a tutorial here:
<a href="http://www.kde.org/media/readme.php">
http://www.kde.org/media/readme.php</a>.
<br />
<br />
</li>
<li>
The individual project's pages will be hosted on
<tt>http://edu.kde.org/<i>programname</i></tt>. To create such a page, simply
copy the pages of an existing project (such as KHangMan), and name the new
directory appropriately. Remember to remove the <tt>.svn</tt> directories in
the new subdirectory, otherwise you won't be able to commit it!<br/>
Please put your screenshots pics in a <tt>/pics</tt> subdir.<br />
</li>
</ol>
<br />
Alternatively, contact <a href="mailto:toscano DOT pino AT tiscali DOT it">Pino Toscano</a>
or <a href="mailto:annma AT kde DOT org">Anne-Marie Mahfouf</a> to do it for you.
<br />

<br />
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
