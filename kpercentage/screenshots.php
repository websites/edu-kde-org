<?php
  $page_title = "KPercentage - Screenshots";

  include( "header.inc" );

  $gallery = new EduGallery( "KPercentage in action" );
  $gallery->addImage("pics/screen1_tm.png", "pics/screen1.png", 160, 80,  "[Screenshot]", "", "Main window");
  $gallery->addImage("pics/screen2_tm.png", "pics/screen2.png", 160, 101,  "[Screenshot]", "", "Third exercise");
  $gallery->show();
?>

<hr width="30%" align="center" />
<p>Author: Matthias Me&szlig;mer<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include("footer.inc");
?>

