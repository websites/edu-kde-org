<?php
  $page_title = "KPercentage";

  include( "header.inc" );

  $appinfo = new EduAppInfo( "KPercentage" );
  $appinfo->setIcon( "../images/icons/kpercentage_32.png" );
  $appinfo->setVersion( "1.3" );
  $appinfo->setCopyright( "2001", "Matthias Me&szlig;mer" );
  $appinfo->setLicense("gpl");
  $appinfo->addAuthor( "Matthias Me&szlig;mer", "matthias AT familie-messmer DOT de" );
  $appinfo->addContributor( "Robert Gogolok", "mail AT robert-gogolok DOT de", "CVS, coding and makefiles" );
  $appinfo->addContributor( "Carsten Niehaus", "cniehaus AT gmx DOT de", "CVS, coding and sed scripting" );
  $appinfo->addThanksTo( "Elhai Achiam", "e_achiam AT netvision DOT net DOT il", "pixmaps" );
  $appinfo->addThanksTo( "Danny Allen", "dannya40uk AT yahoo DOT co DOT uk", "pixmaps" );
  $appinfo->addThanksTo( "Jonathan Drews", "j.e.drews AT att DOT net", "spelling and language" );
  $appinfo->addThanksTo( "Whitehawk Stormchaser", "zerokode AT gmx DOT net", "cleaning and bugfixing" );
  $appinfo->addThanksTo( "David Vignoni", "david80v AT tin DOT it", "svg icon" );
  $appinfo->showIconAndCopyright();
?>

<h2>Description</h2>
<p>
    KPercentage is a small math application that will help pupils
    to improve their skills in calculating percentages.
</p>

<p>
    KPercentage is stable. 
</p>

<?php
  $appinfo->showPeople();
  $appinfo->showLicence();
?>

<hr width="30%" align="center" />
<p>Author: Matthias Me&szlig;mer<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include("footer.inc");
?>
