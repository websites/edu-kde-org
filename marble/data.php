<?php
  $translation_file = "edu-kde-org";
  require_once "functions.inc";
  $site_root = "../";
  $page_title = i18n_noop('Marble Data');
  
  include ( "header.inc" );
  
  $submenu->show();

?>

<H4><?php i18n( "Maps" ); ?></H4>
  <p>
    <i><?php i18n( "Blue Marble Next Generation (500 m / pixel)" ); ?></i><br> 
    <?php i18n( "NASA Goddard Space Flight Center Earth Observatory" ); ?>  <a href="http://earthobservatory.nasa.gov/Newsroom/BlueMarble/">http://earthobservatory.nasa.gov/Newsroom/BlueMarble/</a>
  </p>
  <p>
    <i><?php i18n( "Earth's City Lights" ); ?></i><br>
    <?php i18n( "Data courtesy Marc Imhoff of NASA GSFC and Christopher Elvidge of NOAA NGDC. Image by Craig Mayhew and Robert Simmon, NASA GSFC." ); ?>
  </p>
  <p>
    <i><?php i18n( "Shuttle Radar Topography Mission (SRTM30, 1 km / pixel )" ); ?></i><br> 
    NASA Jet Propulsion Laboratory <a href="http://www2.jpl.nasa.gov/srtm/">http://www2.jpl.nasa.gov/srtm/</a>
  </p>
  <p>
    <i><?php i18n( "Micro World Data Bank in Polygons (\"MWDB-POLY / MWDBII\")" ); ?></i><br>
    CIA ; Global Associates, Ltd.; Fred Pospeschil and Antonio Rivera
  </p>
  <p>
    <i><?php i18n( "Temperature and Precipitation Maps (July and December)" ); ?></i><br> 
    <?php i18n( "A combination of two datasets:" ); ?>
    <ul>
        <li><?php i18n( "Legates, D.R. and Willmott, C.J. 1989. Average Monthly Surface Air Temperature and Precipitation. Digital Raster Data on a .5 degree Geographic (lat/long) 361x721 grid (centroid-registered on .5 degree meridians). Boulder CO: National Center for Atmospheric Research." ); ?> <a href="http://www.ngdc.noaa.gov/ecosys/cdroms/ged_iia/datasets/a04/lw.htm">http://www.ngdc.noaa.gov/ecosys/cdroms/ged_iia/datasets/a04/lw.htm</a></li>
        <li><?php i18n( "CRU CL 2.0: New, M., Lister, D., Hulme, M. and Makin, I., 2002: A high-resolution data set of surface climate over global land areas. Climate Research 21." ); ?> <a href="http://www.cru.uea.ac.uk/cru/data/hrg">http://www.cru.uea.ac.uk/cru/data/hrg</a></li>
    </ul>
  </p>

<H4><?php i18n( "Street Map" ); ?></H4>
  <p>
    <i><?php i18n( "OpenStreetMap" ); ?></i><br>
    <?php i18n( "The street maps used in Marble via download are provided by the <a href=\"http://www.openstreetmap.org\">OpenStreetMap</a> Project (\"OSM\"). OSM is an open community which creates free editable maps." ); ?>
    <?php i18n( "<i>License</i>: OpenStreetMap data can be used freely under the terms of the <a href=\"http://wiki.openstreetmap.org/index.php/OpenStreetMap_License\">Creative Commons Attribution-ShareAlike 2.0 license</a>." ); ?>
  </p>

<H4><?php i18n( "Cities and Locations" ); ?></H4>
  <p>
    <i><?php i18n( "World Gazetteer" ); ?></i><br> 
    Stefan Helders <a href="http://www.world-gazetteer.com">http://www.world-gazetteer.com</a>
  </p>

  <p>
    <i><?php i18n( "Czech Statistical Office" ); ?></i><br> 
    <?php i18n( "Public database" ); ?> <a href="http://www.czso.cz/eng/redakce.nsf/i/home">http://www.czso.cz</a>
  </p>

<H4><?php i18n( "Flags" ); ?></H4>
  <p>
    <i><?php i18n( "Flags of the World" ); ?></i><br>
    <?php i18n( "The flags were taken from Wikipedia (<a href=\"http://www.wikipedia.org\">http://www.wikipedia.org</a>) which in turn took a subset from <a href=\"http://www.openclipart.org\">http://www.openclipart.org</a> and reworked them. All flags are under the public domain (see comments inside the svg files)." ); ?> 
  </p>

<H4><?php i18n( "Stars" ); ?></H4>
  <p>
    <i><?php i18n( "The Bright Star Catalogue" ); ?></i><br>
    <?php i18n( "5th Revised Ed. (Preliminary Version) Hoffleit D., Warren Jr W.H., Astronomical Data Center, NSSDC/ADC (1991) " ); ?>
    <a href="http://adc.gsfc.nasa.gov/adc-cgi/cat.pl?/catalogs/5/5050">http://adc.gsfc.nasa.gov</a>
  </p>

<br><hr width="30%" align="center" />
<p><?php i18n( "Last update:" ); ?> <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>

