<?php
  $translation_file = "edu-kde-org";
  require_once "functions.inc";
  $site_root = "../";
  $page_title = i18n_noop("Marble in the Press and Awards");

  include ( "header.inc" );
  
  $submenu->show();

?>

<p>
<dl> <dt> <a href="./poster/marble_worlddomination.jpg"><img border="0" height="263" src="./poster/marble_worlddomination_thumb.jpg" alt="World domination -- one step at a time."></a> </dt></dl>
</p>

<p><?php i18n( "Marble has many interesting aspects that are worth a look. As such, it often gets covered by reviews in articles online as well as in printed magazines. Here are a few recent ones:" ); ?></p>

<h3><a name="Reviews"><?php i18n( "Reviews" ); ?></a></h3>

<!-- Quicklinks -->

<h4><a name="English">English</a></h4>
<ul>
<li><i>Linuxers 03/2010</i> - <b><a href="http://linuxers.org/article/marble-atlas-your-desktop">Marble, an Atlas for your Desktop</a></b>: <br> Shashank Sahni wrote an article about Marble: <i>"While looking for some applications similar to Google Earth I bumped into this KDE educational project Marble. Its a 3D Atlas and Virtual Globe application that aims to help students, teachers or anyone, in learing more about Earth, Mars, Moon and Venus. It provides a whole lot of features other than just the Atlas, with an interface similar to Google Earth."</i></li>
<br><br>
<li><i>LWN.net 03/2010</i> - <b><a href="http://lwn.net/Articles/378221/">Fun with free maps on the free desktop</a></b>: <br> Joe "Zonker" Brockmeier has written a comparison between map applications on Linux. Of course it also covers Marble: <i>"Marble is one of the best-known map-viewers for Linux. "</i></li>
<br><br>
<li><i>Dedoimedo 08/2009</i> - <b><a href="http://www.dedoimedo.com/computers/marble.html">Marble is a polished jewel</a></b>: <br> Dedoimedo - a site dedicated to computer education - reviews Marble and concludes: <i>"Marble is a spectacular piece of software. It is beautiful, thorough, detailed, well laid out, fast, robust, and responsive. [...]  It also seems like an excellent tool for teaching people (and kids) the wonders of our Planet in a fun and exciting way. I wish I had something like this when I was growing up!"</i></li>
<br><br>
<li><i>Free Software Magazine 10/2008</i> - <b><a href="http://www.freesoftwaremagazine.com/columns/openstreemaps_free_softwares_answer_google_and_commercially_restricted_geo_data">OpenStreetMaps: free software's answer to Google and commercially-restricted geo-data</a></b>: <br> Gary Richmond writes in the Free Software Magazine about <a href="www.openstreetmap.org>">OpenStreetMap</a> and mentions Marble: <i>"Inevitably, it was only a matter of time before the GNU/Linux community got its mitts on OSM and started to look at ways of integrating it into KDE. Yes, of course, Google Earth and Google Maps are available and they are pretty good but if, like the Free software Foundation(FSF), you believe that it is not possible to draw computer users away from proprietary software if there are no free, non-DRM alternatives you’ll be glad to hear that Marble, a Virtual Globe and World Atlas is an alternative."</i></li>
<br><br>
<li><i>Red devil's Techblog 10/2008</i> - <b><a href="http://reddevil62-techhead.blogspot.com/2008/10/marble-googe-earth-lite-comes-free-with.html">Marble: 'Googe Earth Lite' Comes Free With KDE 4</a></b>: <br> In his blog Steve Dawson from Glasgow reports that when he recently installed Arch Linux on his main desktop machine he chose KDE 4.1 as his desktop environment: <i>"And with it came a lovely little application called Marble, a desktop globe program – essentially, Google Earth Lite – which is part of the default KDE 4 Education selection."</i></li>
<br><br>
<li><i>Linux.com 8/2008</i> - <b><a href="http://www.linux.com/feature/143769">Marble provides basic engine for free Google Earth replacement</a></b>: <br> Bruce Byfield takes a good look at KDE 4.1's Marble 0.6 and suggests that <i>"The Free Software Foundation can cross off another item on its high priority list of applications that free software needs in order to compete. Version 0.6 of Marble, which ships with KDE 4.1, may not rival Google Earth just yet, but the underlying engine has the potential to do so in future versions. The main improvements needed to reach this stage are a lower level of detail and some additional views and integration into free online resources."</i>. Later on, he puts his praises into perspective by adding <i>"Marble is unlikely to rival Google Earth in certain areas. It is unlikely, for example, to offer continually updated satellite views or street images any time in the immediate future. However, by the time Marble reaches its 1.0 release, there seems no reason why it could not equal Google's efforts on a more practical level."</i></li>
<br><br>
<li><i>LinuxJournal 2/2008</i> - <b><a href="http://www.linuxjournal.com/article/9950">Cooking with Linux - It's a Virtual World</a></b>: <br> Marcel Gagné gives an entertaining in-depth introduction into the feature set of Marble 0.4 with lots of beautiful screenshots. He says that <i>"Marble's interface is easy to use"</i> and concludes: <i>"Exploring the world with Marble is great fun and educational as well."</i></li>
<br><br>
<li><i>LWN.net 10/2007</i> - <b><a href="http://lwn.net/Articles/250358/">Geographic display and input using Marble</a></b>: <br> Impressed with the fact that Marble is only 9MB insize, does not require OpenGL, or any high-end hardware support, and has a future with embedded devices, games and the incorporation of OpenStreetMap data, LWN.net speaks highly of the 0.4 release, and even provides a little information on what can be expected from the 0.5 release. <i> "...unlike the others, it does not rely upon enormous data sets accessed viathe internet; it is, instead, self-contained and fairly lightweight." </i></li>
</ul>
<h4><a name="Deutsch">Deutsch</a></h4>
<ul>
<li><i>Softonic.de 9/2008</i> - <b><a href="http://software.bild.de/ie/73633/Marble">Softonic.de: Dreh' die Murmel
</a></b>: Mit dem Review von Softonic.de zieht Marble in die Online-Seiten von Bild, FAZ, Süddeutsche und anderen Tageszeitungen ein. Die MarbleQt-Version für Windows wird als Download bereitgestellt. Das Fazit:  <i>"Das kostenlose Marble bringt eine gute Alternative für angestaubte Globen und Atlanten auf den Rechner. Die zahlreichen Anzeige-Modi und die detaillierten Karten lassen kaum Wünsche offen. Vor dem Konkurrenten Google Earth braucht sich Marble ebenfalls nicht zu verstecken. Besonders die liebevolle Indizierung für große Städte wie Berlin oder München macht die Freeware interessant. Leider sind Regionen wie Afrika oder Südamerika kaum verzeichnet."</i></li>
<br><br>
<li><i>PC Welt 7/2008</i> - <b><a href="http://www.pcwelt.de/start/dsl_voip/online/praxis/173173/online_kartendienste_und_routenplaner_vorgestellt/index5.html">Ratgeber: Online-Kartendienste und -Routenplaner vorgestellt</a></b>: <br> Hans-Christian Discherl gives an overview in German about online map services like Google Earth, Google Maps, Windows Live Local and Nasa World Wind. <a href="http://www.openstreetmap.org">OpenStreetMap</a> and Marble get promoted on the last page: <i>"Openstreetmap can be used as described before in the web browser.  Linux-, Windows- and MacOS users can also have access to the map data of OpenStreetMap via the client application Marble. Marble is part of KDE 4.1. However its usage is not restricted to KDE - it can also be used without problems within Gnome."</i></li>
</ul>


<h4><a name="Espanol">Espanol</a></h4>
<ul>
<li><i>Ciber Hacking-Lab 3/2010</i> - <b><a href="http://cbrhackinglab.wordpress.com/2010/04/05/globo-terraqueo-bajo-gnulinux/">Globo terraqueo bajo GNU/Linux</a></b>: <br> Juan Manuel Uribe Medina nos cuenta: <i>"Si utilizas el sistema operativo privativo window$, tal vez conozcas aplicaciones como Google Earth, un globo terráqueo virtual, aunque existe una versión de Google Earth para GNU/Linux, existe un problema muy grave, no es software libre. [...] Marble es una aplicación de globo terráqueo virtual y de código libre, forma parte del proyecto KDE."</i></li>
<br><br>
<li><i>Hablandodesigs 9/2008</i> - <b><a href="http://www.hablandodesigs.com/2008/09/22/648/">Marble es otra alternativa a Google Earth</a></b>: <br> Juan Manuel Uribe Medina nos cuenta: <i>"Aunque Marble está en una fase temprana de desarrollo (versión 0.6) es uno de esos programas que hay que revisar frecuentemente pues su potencial es muy alto por varias razones: facilidad de agregar nuevos datos, su desarrollo facilita agregarlo a otros sofwares como plugin, y finalmente su simplicidad ya que bastante ligero el programa y no requiere de tarjetas aceleradoras de video lo que le permite trabajar hasta en dispositivos móviles. Funciona en Windows, MacOS X y Linux, versión que fué la que yo probé [...]."</i></li>
</ul>


<h3><a name="awards">Awards</a></h3>
<ul>
<li><i>9/2007</i> - <b><a href="http://www.qtcentre.org/contest-first-edition">The Qt Centre Programming Contest 2007</a></b>: Marble won prizes in two categories: The jury chose Marble as the best entry in the Desktop Application Category and for the 3rd prize in the Custom Widget Category.
<br>
The Marble Team would like to thank all the people involved in this contest for making this possible. </li>
</ul>
<br><br>

<?php require 'footer.inc'; ?>
