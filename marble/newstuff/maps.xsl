<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
 
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
 
<xsl:template match="/knewstuff">
    <table border="0">
    <tr>
        <th>Preview</th>
        <th>Description</th>
        <th>Package</th>
    </tr>
        <xsl:apply-templates select="stuff">
        <!-- <xsl:sort select="name" /> -->
        </xsl:apply-templates>
    </table>
</xsl:template>
 
<xsl:template match="stuff">
    <tr>
        <td><img src="{preview}" border="0" style="vertical-align:middle;" /></td>
        <td style="vertical-align:middle;">
        <b><xsl:value-of select="name"/></b><br />
        <i><xsl:value-of select="summary"/></i><br />
        Author: <xsl:value-of select="author"/><br />
        License: <xsl:value-of select="licence"/>
        </td>
        <td><a href="{payload}" >Download version <xsl:value-of select="version"/></a></td>
    </tr>
</xsl:template>
 
</xsl:stylesheet>
