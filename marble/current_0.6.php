<?php
  $site_root = "../";
  $page_title = 'Marble 0.6 Release';

  include ( "header.inc" );

  $submenu->show();

?>
<h3><a name="description">Visual ChangeLog: Marble 0.6</a></h3>

 <p>Marble 0.6 got released together with KDE 4.1 on July 29th 2008. Here are the highlights of Marble 0.6:</p>

<h4>OpenStreetMap Support</h4>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_openstreetmap.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_openstreetmap_thumb.jpg" alt="The Eiffel Tower in Marble"></a> </dt> <dd><i>The Eiffel Tower in Marble - C'est magnifique!</i></dd> </dl>
</p>

<p>From the user's perspective the most important change is certainly that Marble finally got support for a global roadmap, thanks to the <a href="http://www.openstreetmap.org">OpenStreetMap</a> ("OSM") project! <b>OpenStreetMap</b> is an open community which creates <i>free, editable maps,</i> in a spirit that is somewhat similar to Wikipedia.</p>
<p>The maps of OpenStreetMap have improved dramatically during the last 3 years, especially in Europe.
Having OSM support means that you can zoom down to a resolution of a few centimeters, and view streetmaps of your neighbourhood -- as long as it has been mapped already. If this is not the case (yet), get equipped with a GPS device and <a href="http://wiki.openstreetmap.org/index.php/Beginners%27_Guide">join the OSM project!</a></p>
<p>The OpenStreetMap support currently implemented in Marble covers download of prerendered maps from the servers of the OpenStreetMap project. Every few days Marble will update the data in use. In the near future we plan to deliver more extensive OSM support which includes better rendering as well as improved functionality.</p>
<p>
<dl> <dt> <a href="./screenshots/0.6/marble_openstreetmap2.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_openstreetmap2_thumb.jpg" alt="Rome in Marble"></a> </dt> <dd><i>Rome in Marble/OSM</i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_openstreetmap3.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_openstreetmap3_thumb.jpg" alt="Rome in Marble"></a> </dt> <dd><i>Palm/World islands in Dubai as displayed by Marble/OSM</i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_openstreetmap4.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_openstreetmap4_thumb.jpg" alt="Rome in Marble"></a> </dt> <dd><i>Marble globe centered on Dubai</i></dd> </dl>
</p>

<h4>Realtime cloud cover and sun shading</h4>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_clouds.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_clouds_thumb.jpg" alt="Real-time cloud coverage in Marble"></a> </dt> <dd><i>Real-time cloud coverage in Marble</i></dd> </dl>
</p>

<p>The most notable change with regard to the satellite view is probably the addition of a <b>real-time cloud cover</b>: Every 3 hours Marble fetches the cloud data of our entire planet, and displays them on the satellite view. To make it feel even more realistic, <b>sun shading</b> has been added: There are two options:</p>
<ul>
 <li> Shading the dark side of the Earth
 <li> Viewing the <i>Earth at Night</i> map on the dark side of the Earth
</ul>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_nightlights.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_nightlights_thumb.jpg" alt="Sun shading in flat map projection. Note the the measure line ..."></a> </dt> <dd><i>Sun shading in flat map projection. Note the the measure line ...</i></dd> </dl>
</p>

<p>Using the time control, you can watch how the sun shading changes with the daytime and seasons (note how the starry sky in the background changes, too!). It's also worthwhile to take a look at the different shapes of the solar terminator (the line between night and day) for each projection.
</p>

<h4>New maps: Temperature and Precipitation</h4>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_temperature.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_temperature_thumb.jpg" alt="Legend and globe for average surface air temperature in December"></a> </dt> <dd><i>Legend and globe for average surface air temperature in December</i></dd> </dl>
</p>

<p>With Marble 0.6 we've introduced four maps:
Average Monthly Surface Air <b>Temperature and Precipitation</b>, for both <i>July and December</i>, so you can compare the data for summer and winter seasons (Credits: <a href="http://www.ngdc.noaa.gov/ecosys/cdroms/ged_iia/datasets/a04/lw.htm">Legates, D.R. and Willmott</a>, C.J. 1989, National Center for Atmospheric Research and <a href="http://www.cru.uea.ac.uk/cru/data/hrg">New, M., Lister, D., Hulme, M. and Makin, I., 2002</a>).</p>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_temperature2.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_temperature2_thumb.jpg" alt="Legend and globe for average surface air temperature in December"></a> </dt> <dd><i>Legend and flat map for average surface air temperature in July</i></dd> </dl>
</p>

<h4>New projection</h4>
<p><a href="http://en.wikipedia.org/wiki/Mercator_projection">Mercator</a> is another "flat" projection. Unlike the <a href="http://en.wikipedia.org/wiki/Equirectangular_projection">Plate carré</a> projection it <i>preserves shapes and directions</i> on the map and is therefore pretty popular.</p>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_mercator.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_mercator_thumb.jpg" alt="The new Mercator projection in Marble"></a> </dt> <dd><i>The new Mercator projection in Marble</i></dd> </dl>
</p>

<h4>Settings dialog</h4>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_settings.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_settings_thumb.jpg" alt="View settings in the new configuration dialog"></a> </dt> <dd><i>View settings in the new configuration dialog</i></dd> </dl>
</p>

<p>The KDE version of Marble has received a shiny new settings dialog (The Qt-Only version still lacks this feature). Here you can configure various settings. The map quality can be adjusted for the process of panning/zooming, as well as for the still image. On fast machines, you are able to enjoy "jumps" to the target, by enabling "Animate voyage to the target". Furthermore, there's also a dialog to adjust Cache/Proxy settings, and to configure the newly introduced plugins.</p>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_settings2.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_settings2_thumb.jpg" alt="Configuration of navigation settings"></a> </dt> <dd><i>Configuration of navigation settings</i></dd> </dl>
</p>

<h4>More features</h4>
<p>Lots of other changes have happened since Marble 0.5 was released:</p>
<ul>
    <li>A <b>Starry sky plugin</b> which features the real constellations, and adjusts to the (current) time specified in the time control.
    <li>Marble now even finds places which got entered without accents (e.g. if you type in "Malaga" instead of "Màlaga").
    <li>Addition of an <b>overview world map</b>.
    <li>Download of map data and wikipedia is about 3-4 times faster now.
    <li>Different <b>map quality levels</b>, which can be assigned to panning and still images.
    <li>A High quality level, which features bilinear filtering.
    <li>Creation of tiles for the Atlas view at compile time. This removes the long map initialization during the first start of previous versions of Marble.
<li>Custom map key for each map theme.
    <li>"Jump" Animations.
    <li>Alternative notation for degrees: Sexagesimal (degrees-minutes-seconds) or Decimal.
    <li><b>Copy Coordinates</b> feature to be able to easily copy coordinates into your text editor.
    <li>Cache &amp; proxy settings.
    <li>During startup Marble can return to where it was the last time it was used.
</ul>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_stars.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_stars_thumb.jpg" alt="Configuration of navigation settings"></a> </dt> <dd><i>The "Stars Plugin" in Marble: Watch Orion and the big dog on the left ...</i></dd> </dl>
</p>

<h4>Framework changes</h4>
In terms of framework Marble has received a huge overhaul:
<ul>
    <li>Configurable storage layouts, projections and directory structure for tiles, which will enable us to easily import tiles from other sources (e.g. NASA WorldWind) in the future.
    <li>Colorization of grayscale map data (including matching map key entries), which could be nice for people who want to use Marble in their own applications for research at university.
    <li>Introduction of an XMLUI framework for maps via the DGML 2.0 map file format.
    <li>Complete rewrite of the DGML / KML support using QXmlStreamReader.
    <li>Introduction of a plugin architecture for layers.
    <li>Initial implementation of GeoPainter. GeoPainter largely resembles QPainter, but it uses geographical coordinates instead of pixel coordinates, and adjusts to projection specific needs.
    <li>Refactoring of projection classes.
    <li>Separation of the MarbleMap class from the widget to make non-widget uses like the World Clock plasmoid easier.
</ul>

<p>
<dl> <dt> <a href="./screenshots/0.6/marble_test.jpg"><img border="0" width="410" height="307" src="./screenshots/0.6/marble_test_thumb.jpg" alt="A simple test plugin that shows GeoPainter in action"></a> </dt> <dd><i>A simple test plugin that shows GeoPainter and its annotation support in action</i></dd> </dl>
</p>

 <p>
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>

 <?php
   include "footer.inc";
 ?>












