<?php
  $site_root = "../";
  $page_title = 'Marble - Current Release';

  include ( "header.inc" );

  $submenu->show();
?>
<h3><a name="description">Visual ChangeLog: Marble 0.10</a></h3>

<p>
Marble 0.10 was released on August 10th, 2010. It is part of the KDE 4.5
Software Compilation. In the good tradition of recent years, we have collected
those changes directly visible to the user. Please enjoy looking over the new
and noteworthy:
</p>

<h4>Online Routing</h4>

<p>Do you want to plan a bicycle tour in the nearby wood? Need driving
instructions to get to an address in a foreign city? Besides searching for
places, Marble can now display possible routes between two or more of them.</p>
<p>And the best thing is: The <b>routes are draggable</b>!
<p>
<dl> <dt> <a href="./screenshots/0.10/marble_routing0.png"><img border="0"
width="410" height="293" src="./screenshots/0.10/marble_routing0_thumb.png"
alt="Routing in Marble"></a> </dt> <dd><i>Online Routing in Marble</i></dd>
</dl>
</p>

<p>
Places to travel along can be entered using search terms (e.g. addresses) in
the new Routing tab on the left. Of course Marble also allows you to input them
directly on the map. Routes are retrieved using <a
href="http://openrouteservice.org/">OpenRouteService</a> (restricted to Europe)
and displayed on the map. <b>Turn-by-turn instructions</b> are displayed on the left.
</p>

<p>
You can customize the route using preferences like transport type
(<i>car, bicycle, foot</i>). An <i>arbitrary number of via points</i> can be
added easily: Use either search terms or create stopovers quickly and
conveniently by dragging them out of the existing route and dropping them at
the desired position. While a real-time navigation mode is scheduled for Marble
0.11, you can already <i>export the route in the GPX format</i> now. This feature is
handy for using routes in conjunction with your navigation device or other
software. This feature was brought to you by <i>Dennis Nienhüser</i>
</p>

<h4>Bulk Download for Tile data in Marble for Offline Usage</h4>

<p>
For normal usage, Marble downloads the map data that is needed on the fly in the background.
It also saves the data that has been downloaded on the hard disc.
Now imagine that you make a trip to Norway, and you don't know for sure whether
you'll have internet during the trip. So you want to download the whole Oslo area
in advance. Up to now this hasn't been possible. But with Marble 0.10.0 you can click
"File->Download Region ..." and you get a dialog where you can specify the region and
the zoom levels that you want to download. This feature was brought to you by
<i>Jens-Michael Hoffmann</i>.
</p>

<p>
<dl> <dt> <a href="./screenshots/0.10/marble_downloadregion0.png"><img border="0"
width="410" height="282" src="./screenshots/0.10/marble_downloadregion0_thumb.png"
alt="Downloading two levels of the currently visible map region"></a> </dt>
<dd><i>Download of the Visible Region</i></dd>
</dl>
</p>

<h4>Support for Multiple Layers in Marble</h4>

<p>So far, Marble has had support only for displaying a single map texture on top of the globe.
(The only exception was the cloud feature which allowed having clouds displayed on top of
the satellite map. This, however, was hard-coded and not extensible.)
<p>For this release, <i>Jens-Michal Hoffmann</i> has worked on <b>Multiple Layer support</b>.
This means that maps can now be created which display multiple texture
layers.  For instance: a cloud layer on top of a street texture layer on top of
a satellite texture layer.  This is all done in a
generic way. So people who create maps for Marble can create an arbitrary amount of layers
blended on top of each other. The best thing is: Due to the way the feature was implemented
<i>the performance doesn't change</i>! And the clouds feature has been reworked to make use of the
new mechanism.

<h4>Support for Gimp-like Filters Between Layers in Marble</h4>

<p>
<dl> <dt> <a href="./screenshots/0.10/marble-wms-level-16-multiply-blending-dresden.png"><img border="0"
width="410" height="448" src="./screenshots/0.10/marble-wms-level-16-multiply-blending-dresden_thumb.png"
alt="The City of Dresden
shown in Marble with multiple layers: Satellite images provided via WMS displayed on top
of OpenStreetMap data via Multiply Blending."></a> </dt> <dd><i>The City of Dresden
shown in Marble with multiple layers: Satellite images provided via WMS displayed on top
of OpenStreetMap data via Multiply Blending.</i></dd>
</dl>
</p>

<p>As described before, Marble has support for multiple layers now. Layers can get blended
on top of each other using Gimp-style "filters": You can choose among more than 30
blending algorithms, such as: <b>Overlay</b>, <b>ColorBurn</b>, <b>Darken</b>, <b>Divide</b>,
<b>Multiply</b>, <b>HardLight</b>, <b>ColorDodge</b>, <b>Lighten</b>, <b>Screen</b>,
<b>SoftLight</b> and <b>VividLight</b>. If you've ever use an application like
Photoshop (TM), Krita or Gimp then you probably know what this means.

<h4>Quick and Dirty WMS Support and More Url Download Schemes.</h4>

Lots of map data is provided on the internet on servers via the
<a href="http://en.wikipedia.org/wiki/Web_Map_Service">Web Map Service ("WMS")</a> protocol.
<i>Bernhard Beschow</i> has added initial quick and dirty WMS support to Marble. This means
that there are now a huge number of maps that can be easily displayed using Marble.

<h4>Marble Goes Mobile: Support for Nokia's N900 and UI profiles</h4>

With KDE 4.5, we have completed the first step toward mobile platform support:
Marble will show a slightly different and simplified UI on the N900 Maemo
platform compared to the desktop. For KDE 4.6 we aim for an even better
user experience and improved performance.
<p>
<dl> <dt> <a href="./screenshots/0.10/marble_maemo0.jpg"><img border="0"
width="410" height="278" src="./screenshots/0.10/marble_maemo0_thumb.jpg"
alt="Marble running on a Nokia N900"></a> </dt> <dd><i>Marble
on a Nokia N900</i></dd>
</dl>
</p>
<p>
<dl> <dt> <a href="./screenshots/0.10/marble_maemo1.png"><img border="0"
width="400" height="240" src="./screenshots/0.10/marble_maemo1_thumb.jpg"
alt="Marble Routing on Maemo 5"></a> </dt> <dd><i>Marble Routing on Maemo5</i></dd>
</dl>
</p>

<p>For more information please visit the <a href="http://marble.garage.maemo.org/">Marble Garage Project</a>.
Next stop will be the <a href="http://gitorious.org/meegotouch-marble">MeeGo version for Marble</a>.

<h4>Display APRS (Automatic Packet Reporting System) Senders with Marble</h4>

<p>This is one of our first more specialized Online Service Plugins: The APRS plugin created by
<i>Wes Hardaker</i> shows worldwide Ham-Radio stations.
<p>HAM Radio's APRS program allows radio transmitters to send their position and other
information and is frequently used in <i>disaster relief efforts</i> for coordinating team distribution.
<p>
<dl> <dt> <a href="./screenshots/0.10/marble_aprs0.png"><img border="0"
width="410" height="290" src="./screenshots/0.10/marble_aprs0_thumb.png"
alt="APRS senders displayed in Marble"></a> </dt> <dd><i>APRS senders
displayed in Marble</i></dd>
</dl>
</p>
<p>We are still looking for programmers who would like to create
more Online-Plugins: e.g. <i>Twitter, News, Earthquakes or a social network plugin</i>.
It's easy to do and there's an <a href="http://techbase.kde.org/Projects/Marble/OnlineServices">Online Service Plugin tutorial</a>
available on our website that shows how to do it.

<h4>Performance Improvements and More Changes Under the Hood ...</h4>

<p>
In addition to these major improvements, our Marble developers have worked on
several other small features, bug fixes and performance improvements:
</p>
<ul>
<li>Two <i>additional search backends</i>: Hostip (try "planetkde.org") and <a
href="http://wiki.openstreetmap.org/wiki/Nominatim">OSM Nominatim</a> (try "ATM,
Karlsruhe") <i>(Dennis Nienhüser)</i></li>
<li>Improved <b>animation support</b> for zoom and panning <i>(Dennis Nienhüser)</i></li>
</ul>

<p>New features in earlier versions are described in prior Visual
Changelogs:</p>

<ul>
<li> <a href="http://edu.kde.org/marble/current_0.9.php">Marble 0.9</a>:
<li> <a href="http://edu.kde.org/marble/current_0.8.php">Marble 0.8</a>:
<li> <a href="http://edu.kde.org/marble/current_0.7.php">Marble 0.7</a>:
<li> <a href="http://edu.kde.org/marble/current_0.6.php">Marble 0.6</a>:
</ul>

<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
   include "footer.inc";
?>
