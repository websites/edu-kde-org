<?php
  $site_root = "../";
  $page_title = "Kiten";
  

  include( "header.inc" );

  $appinfo = new AppInfo( "Kiten" );
  $appinfo->setIcon( "../images/icons/kiten_32.png", "32", "32");
  $appinfo->setVersion( "1.2" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2001", "Jason Katz-Brown" );
  $appinfo->addMaintainer( "Joseph Kerian", "jkerian AT gmail DOT com" );
  $appinfo->addAuthor( "Jason Katz-Brown", "jason AT katzbrown DOT com" );
  $appinfo->addContributor( "David Vignoni", "david80v AT tin DOT it", "svg icon" );
  $appinfo->addContributor( "Paul Temple", "paul DOT temple AT gmx DOT net", "Port to KConfig XT and bug fixing" );
  $appinfo->show();
?>
     <h3>Description</h3>
  <p>Kiten is a Japanese reference/learning tool.</p>
  <p>Kiten features:</p>
<ul>
<li>Search with english keyword, Japanese reading, or a Kanji string on a list of EDICT files.</li>
<li>Search with english keyword, Japanese reading, number of strokes, grade number, or a Kanji on a list of KANJIDIC files.</li>
<li>Comes with all necessary files.</li>
<li>Very fast.</li>
<li>Limit searches to only common entries.</li>
<li>Nested searches of results possible.</li>
<li>Compact, small, fast interface.</li>
<li>Global KDE keybindings for searching highlighted strings.</li>
<li>Learning dialog. (One can even open up multiple ones and have them sync between each other.)</li>
<li>Browse Kanji by grade.</li>
<li>Add Kanji to a list for later learning.</li>
<li>Browse list, and get quizzed on them.</li>
</ul>

<!--
<p>See the <a href="http://www.katzbrown.com/kiten">Kiten homepage</a> for more details.</p>

<p><b>Download</b>: <a href="http://www.katzbrown.com/kiten/Download/kiten-0.5.tar.bz2">kiten-0.5.tar.bz2</a><br />
(Qt3 and KDE-3.0 required)
</p>
-->
<p>You can also find Kiten in the <tt>kdeedu</tt> module in SVN trunk.</p>


<?php
  include "footer.inc";
?>
