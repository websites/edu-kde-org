<?php
  $site_root = "../";
  $page_title = "KLatin";

  
  include ( "header.inc" );

  $appinfo = new AppInfo( "KLatin" );
  $appinfo->setIcon( "../images/icons/klatin_32.png", "32", "32" );
  $appinfo->setVersion( "0.9" );
  $appinfo->setCopyright( "2001", "George Wright" );
  $appinfo->setLicense("gpl");
  $appinfo->addAuthor( "George Wright", "gwright AT kde DOT org" );
  $appinfo->addContributor( "George Wright", "gwright AT kde DOT org", "Maintainer" );
  $appinfo->addContributor( "Neil Stevens", "neil AT qualityassistant DOT org", "Coding" );
  $appinfo->addContributor( "Anne-Marie Mahfouf", "annma AT kde DOT org", "Coding" );
  $appinfo->addContributor( "Pino Toscano", "toscano DOT pino AT tiscali DOT it", "Italian Data Translation for vocabulary files" );
  $appinfo->addContributor( "Danny Allen", "dannya40uk AT yahoo DOT co DOT uk", "SVG Icon" );
  $appinfo->show();
?>
<br />

<h3><a name="description">Description</a></h3>
  <p>KLatin is a program to help revise latin. There are three "sections" in which different aspects of the language can be revised. These are the vocabulary, grammar and verb testing sections. In addition there is a set of revision notes that can be used for self-guided revision.
  </p>
  <p>
  In the vocabulary section an XML file is loaded containing various words and their local language translations. KLatin asks you what each of these words translate into. The questions take place in a multiple-choice environment.
  </p>
   <p>In the grammar and verb sections KLatin asks for a particular part of a noun or a verb, such as the "ablative singular", or the "1st person indicative passive plural", and is not multiple choice.</p>

<p><b>KLatin is only available for KDE 3.x and has been stopped for KDE 4.0.</b></p>

<hr width="30%" align="center" />
<p>
Author: George Wright<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
