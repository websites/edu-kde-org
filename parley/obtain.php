<?php
  $page_title = "How to obtain Parley";
  $site_root = "../";

  include( "header.inc" );
?>

<h4>Linux</h4>
<p>
 Parley comes with KDE4.
 Several Linux distributions offer packages for KDE4. Parley should be included in the KDE 4 version of the kdeedu or parley package depending on the distribution.
 To <a href="./getinvolved.php#development">start developing Parley</a> drop us a note and you should instead get the sources from the KDE subversion repository.
 This will get the latest hottest version of Parley, but may be broken from time to time.
</p>
<h4>Windows</h4>
<p>
 You can find the KDE for Windows project at <a href="http://windows.kde.org/">http://windows.kde.org</a>.
 Please refer to the instructions found there.
</p>

<h4>Parley from source</h4>
<p>
 To build Parley you need to set up a working KDE4 environment as described in 
 <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">Getting Started/Build/KDE4</a>
 article. 
 You can get either the entire kdeedu directory or get the minimum requirements for Parley as described below.
</p>


1. Check out:
<pre style="overflow: visible">
cs KDE # <a href="http://techbase.kde.org/Getting_Started/Increased_Productivity_in_KDE4_with_Scripts/.bashrc">'cs' is a bash function, click here to learn more</a>
svn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdeedu -N 
cd kdeedu
svn up cmake 
svn up libkdeedu 
svn up data 
svn up parley 
</pre>

<br />

2. If you also want to install documentation:
<pre style="overflow: visible">
svn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdeedu/doc -N 
svn up doc/parley
</pre>

<br />

3. Build and install:
<pre style="overflow: visible;">
cmakekde
</pre>

<h4>Stay up to date</h4>

<p>Since Parley is under active development you will probably want to have the 
latest version of it:</p>
<pre style="overflow: visible;">cs KDE/kdeedu # <a href="http://techbase.kde.org/Getting_Started/Increased_Productivity_in_KDE4_with_Scripts/.bashrc">'cs' is a bash function, click here to learn more</a>
svn up ./*
cb # cb is not a typo
make -j2 VERBOSE=1 &amp;&amp; make install
</pre>


<br />
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
