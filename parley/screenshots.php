<?php
  $site_root = "../";
  $page_title = 'Parley Screenshots';
  
  include( "header.inc" );

  $gal = new EduGallery( "Parley Screenshots" );

  $gal->addImage( "screenshots/thumbnail/practice_image_orange_juice.png", "screenshots/practice_image_orange_juice.png", 175, 256, "[Screenshot]", "Images in Practice", "Type in what is shown to you" );

  $gal->addImage( "screenshots/thumbnail/practice_image_tomato.png", "screenshots/practice_image_tomato.png", 224, 256, "[Screenshot]", "A Tomato", "But what is it in spanish?" );

  $gal->startNewRow();


  $gal->addImage( "screenshots/thumbnail/practice_multiple_choice_frog.png", "screenshots/practice_multiple_choice_frog.png", 256, 132, "[Screenshot]", "Multiple Choice", "Click what you see." );

  $gal->addImage( "screenshots/thumbnail/practice_multiple_choice.png", "screenshots/practice_multiple_choice.png", 256, 209, "[Screenshot]", "Multiple Choice Practice", "Practice by choosing the right words..." );

  $gal->startNewRow();

  $gal->addImage( "screenshots/thumbnail/practice_written.png", "screenshots/practice_written.png", 240, 256, "[Screenshot]", "Written Test", "Practice by typing the words..." );

  $gal->addImage( "screenshots/thumbnail/practice_example.png", "screenshots/practice_example.png", 256, 143, "[Screenshot]", " Test", "Practice with example sentence..." );

  $gal->startNewRow();

  $gal->addImage( "screenshots/thumbnail/example_lord_of_the_rings.png", "screenshots/example_lord_of_the_rings.png", 256, 186, "[Screenshot]", " Test", "Practice by filling in the missing word..." );

  $gal->startNewRow();

  $gal->addImage( "screenshots/thumbnail/default.png", "screenshots/default.png", 256, 190, "[Screenshot]", "Parley", "The main interface to enter new words" );

  $gal->addImage( "screenshots/thumbnail/configure_practice.png", "screenshots/configure_practice.png", 256, 190, "[Screenshot]", "Configure Practice", "Set up your test..." );

  $gal->startNewRow();

  $gal->addImage("screenshots/thumbnail/wizard_title_page.png", "screenshots/wizard_title_page.png", 256, 138,
    "[Screenshot]", "New Document Wizard", "Enter general information for a document...");

  $gal->addImage("screenshots/thumbnail/wizard_second_language.png", "screenshots/wizard_second_language.png", 256, 138,
    "[Screenshot]", "Wizard", "...and add languages");

  $gal->startNewRow();

  $gal->addImage("screenshots/thumbnail/parley_empty_document.png", "screenshots/parley_empty_document.png", 256, 173,
    "[Screenshot]", "Parley", "An empty document");

  $gal->startNewRow();

  $gal->addImage("screenshots/thumbnail/parley_three_languages_example1.png", "screenshots/parley_three_languages_example1.png", 256, 194,
    "[Screenshot]", "Three languages", "Parley supports multiple languages");

  $gal->addImage("screenshots/thumbnail/configure_practice_three_languages1.png", "screenshots/configure_practice_three_languages1.png", 256, 228,
    "[Screenshot]", "Three languages", "Choose which language to practice");

  $gal->startNewRow();

  $gal->addImage("screenshots/thumbnail/practice_verb1.png", "screenshots/practice_verb1.png", 256, 196,
    "[Screenshot]", "Conjugation", "Practice verb conjugations.");

  $gal->addImage("screenshots/thumbnail/practice_verb2.png", "screenshots/practice_verb2.png", 256, 196,
    "[Screenshot]", "Conjugation", "I got one conjugation wrong...");

  $gal->startNewRow();

  $gal->addImage("screenshots/thumbnail/parley-accentuation.png", "screenshots/parley-accentuation.png", 256, 162,
    "[Screenshot]", "Accentuation", "A wrong accent!");

  $gal->addImage("screenshots/thumbnail/parley-summary.png", "screenshots/parley-summary.png", 256, 147,
    "[Screenshot]", "Summary", "A little summary after a practice session.");

  $gal->startNewRow();

  $gal->addImage("screenshots/thumbnail/windows-edit-entry-parley.png", "screenshots/windows-edit-entry-parley.png", 256, 192,
    "[Screenshot]", "Windows", "A first screenshot of Parley in Windows - Thanks to Saro Engels.");

  $gal->startNewRow();

  $gal->addImage("screenshots/thumbnail/parley_plasma1.png", "screenshots/parley_plasma1.png", 256, 177,
    "[Screenshot]", "Plasmoid", "Parley on your desktop!");

  $gal->addImage("screenshots/thumbnail/parley_plasma2.png", "screenshots/parley_plasma1.png", 256, 161,
    "[Screenshot]", "Solution", "It reveals the solution when you move the mouse over it.");

  $gal->show();
?>
  <p>All images used in the image practice are from <a href="http://commons.wikimedia.org">Wikimedia Commons</a>.</p>
<?php
  include( "footer.inc" );
?>


