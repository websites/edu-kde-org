<b>Features to come:</b>
<ul>
 <li>Enable sound in various areas. Especially fetching sound files from 
     <a href="http://wiktionary.org">wiktionary.org</a> automatically would be
     nice.</li>
 <li>The search has to be improved. At the moment type:noun lists all nouns, 
     but it needs better gui support.</li>
 <li>Declinations really have to be included.</li>
</ul>

<b>libkdeedu:</b>
<ul>
 <li>reading and writing of declinations</li>
</ul>