<?php
	$page_title = "Obtain KmPlot";
	
	include( "header.inc" );
	
	show_obtain_instructions( "KmPlot" );
?>

<h3>Older Tarball</h3>

<p>You will find KmPlot packages for KDE1 and KDE2 on <a href="http://kmplot.sourceforge.net/">KmPlot webpage</a>.</p>

<?php
	include( "footer.inc" );
?>
