<?php
  $page_title = "KmPlot - Screenshots";
  
  include( "header.inc" );
?>

<div id="quicklinks">
[
  <a href="#gui">User Interface</a> |
  <a href="#samples">Example Plots</a>
]
</div>


<h3><a name="gui" />User Interface</h3>
<?php
  $gallery = new EduGallery( "User Interface" );
  $gallery->addImage( "pics/kmplot_sm.png", "pics/kmplot.png", "160", "136", "Main Window", "Main Window" );
    $gallery->addImage( "pics/plotarea_sm.png", "pics/plotarea.png", "160", "144", "The Plotting Area", "The Plotting Area" );
  $gallery->show();
?>

<h3><a name="samples" />Example Plots</h3>
<?php
  $gallery = new EduGallery( "Example Plots" );
  $gallery->addImage( "pics/cycloide.jpg", "pics/cycloide.png", "140", "140", "x(t)=0.5(t-2sin t); y(t)=2(1-2cos t)", "x(t)=0.5(t-2sin t)<br />y(t)=2(1-2cos t)" );
  $gallery->addImage( "pics/lisajou.jpg", "pics/lisajou.png", "140", "140", "x(t)=5sin(3t); y(t)=3cos(5t)", "x(t)=5sin(3t)<br />y(t)=3cos(5t)" );
  $gallery->addImage( "pics/sum_of_functions.jpg", "pics/sum_of_functions.png", "140", "140", "f(x)=3sin x; g(x)=5cos(2x-pi/2); h(x)=f(x)+g(x)", "f(x)=3sin x<br />g(x)=5cos(2x-pi/2)<br />h(x)=f(x)+g(x)" );
  $gallery->addImage( "pics/spiral.jpg", "pics/spiral.png", "140", "140", "rf(x)=x", "rf(x)=x" );
  $gallery->show();

  include( "footer.inc" );
?> 
