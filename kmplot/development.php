<?php
	$page_title = "KmPlot Development";
	
	include( "header.inc" );
?>

<h3>The Development Version of KmPlot</h3>

There has been a lot of development of KmPlot in the KDE 4 branch. Here you can find screenshots of what's being worked on and an up-to-date list of big changes.<br/><br/>

<?php
  $gallery = new EduGallery( "KmPlot Development" );
  $gallery->addImage( "pics/butterflyplot-kde4.jpg", "pics/butterflyplot-kde4.png", "200", "153", "Butterfly Curve", "A parametric and an implicit curve." );
  $gallery->addImage( "pics/rootplot-kde4.jpg", "pics/rootplot-kde4.png", "200", "153", "Finding a root", "Zooming in on two close roots of a polynomial." );
  $gallery->addImage( "pics/implicit-gradient-kde4.jpg", "pics/implicit-gradient-kde4.png", "200", "156", "Implicit curves", "An implicit function shown with a variety of parameter values." );
  $gallery->startNewRow();
  $gallery->addImage( "pics/tangent-field-kde4.jpg", "pics/tangent-field-kde4.png", "200", "156", "Tangent field", "A first order explicit differential equation and its tangent field." );
  $gallery->addImage( "pics/parameter-animator-kde4.jpg", "pics/parameter-animator-kde4.png", "200", "150", "Parameter Animator", "KmPlot can now step through values of a function's parameter." );
  $gallery->addImage( "pics/equation-editor-kde4.jpg", "pics/equation-editor-kde4.png", "200", "150", "Equation Editor", "The new dialog for editing expressions." );
  $gallery->show();
?>

<h4>User Interface</h4>
<ul>
<li>All the plot dialogs have been merged, with their functionality now placed in a dock widget in the main view.</li>
<li>KmPlot now has a specialized widget for editing equations that does syntax highlighting and shows errors in red. The widget also comes with a dialog available via the click of a button for advanced editing of equations, with buttons to insert special symbols and predefined functions.</li>
<li>Diagrams now take up all of the available space, instead of being confined to a square.</li>
<li>The fonts and lines in the diagram now remain fixed in size when KmPlot is resized.</li>
<li>Make the precision (i.e. number of decimal places) of the current mouse position in the statusbar dependent on the zoom, so that moving the mouse by one pixel is guaranteed to change the coordinate-string displayed by at least one digit.</li>
<li>Merged many other dialogs together - mostly into the Configure KmPlot dialog.</li>
<li>Simplified the Scaling and Coordinate System dialogs.</li>
</ul>

<h4>Plots</h4>
<ul>
<li>Implicit plots (e.g. <i>x^2 + y^2 = 9</i>).</li>
<li>Explicit differential equations (e.g. <i>f''(x) = -f</i>).</li>
<li>Can now trace integrals, polar and parametric plots (i.e. all plots are now traceable).</li>
<li>Use antialiasing and floating-point precision for drawing.</li>
<li>Can use a variety of different styles (e.g. solid, dots, dashes, etc).</li>
<li>The selection of 10 default plot colors are now sexy, contrast well with the default white background, and are easily distinguishable.</li>
<li>A group of plots for different parameter values can now have an associated color-gradient, so that each curve in the group has a subtly different color.</li>
<li>Can show the osculating circle, normal and tangent while tracing.</li>
<li>Added option to show the extrema (minimum and maximum) points on a curve.</li>
<li>Can draw the function name on the plots.</li>
</ul>

<h4>New Functions</h4>
<ul>
<li>Legendre polynomials (up to n = 6).</li>
<li>Heaviside step function H(x) (evaluates to 1 for x > 0, 0 for x &lt; 0 and &frac12; for x = 0).</li>
<li>Floor - round down to nearest integer.</li>
<li>Ceil - round up to nearest integer.</li>
<li>Round - round to nearest integer.</li>
<li>Min - minimum of a set of numbers.</li>
<li>Max - maximum of a set of numbers.</li>
<li>Mod - the modulus (euclidean length) of a set of numbers.</li>
</ul>

<h4>Internals</h4>
<ul>
<li>Use better method for numerical integration (fourth-order Runge-Kutta).</li>
<li>Use Newton's method for root-finding.</li>
<li>The names of constants can be reasonably arbitrary (not confined to one letter long, can have other characters, e.g. Greek letters).</li>
<li>When tracing plots, linearly interpolate closest click position (before, was taking the vertical intersection of the click position with the plot).</li>
<li>When drawing plots, use intelligent and adaptive stepping value for x or t, ensuring that each increment gives a line a few pixels long. This ensures that drawing unimportant bits is fast, and drawing stuff at high-zoom is smooth.</li>
</ul>

<h4>Convenience</h4>
<ul>
<li>Now have a Parameter Animator tool that steps controls the value of a parameter of a function and steps through it in time.</li>
<li>Many new symbols (e.g. &#177;, &#8730;, &#8734;, &#960;, &#215;, &#247;, &#188;, &#189;, &#190;, &#178;, &#179;, etc) are now recognized.</li>
<li>Functions can now be copied via drag-and-drop between KmPlot instances.</li>
<li>Undo/redo system.</li>
<li>More image export options.</li>
</ul>

<h4>Zooming</h4>
<ul>
<li>Unified "Zoom Rectangular" and "Zoom in" under one operation "Zoom In".</li>
<li>Added zoom-out-rectangular equivalent for "Zoom Out".</li>
<li>Animate zooming operation as a visual aid to how the function is transformed.</li>
<li>Removed "Center Point" operation and replaced with translating the view via dragging (Google maps style).</li>
<li>Made the zooming operations single shot and added the actions to the toolbar.</li>
<li>Show crosshairs when selecting the initial point of a zoom.</li>
<li>Use a semi-transparent rectangle for selecting the zoom region.</li>
</ul>


<?php
	include( "footer.inc" );
?>
