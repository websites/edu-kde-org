<?php
  $translation_file = "edu-kde-org";
  include_once( "functions.inc" );
  $page_title = i18n_noop( 'The KDE Education Project' );
  $teaser = true;
  
  include( "header.inc" );
  include( "site_includes/show_news.inc" );
  include_once("classes/class_teaser.inc");
  $teaserImages = new teaserImageBuilder();
  
?>
<style type="text/css">
#main h2 {
	position: absolute;
	top: 400px;
	width: 90%;
}
</style>
	<script type="text/javascript">
	
		(function($) {
			var cache = [];
			// Arguments are image paths relative to the current page.
			$.preLoadImages = function() {
			    var args_len = arguments.length;
			    for (var i = args_len; i--;) {
				var cacheImage = document.createElement('img');
				cacheImage.src = arguments[i];
				cache.push(cacheImage);
			    }
			 }
		})(jQuery)
		
		$(document).ready(function() {
			$('.sub').hide();
			$('.toggle').show();
			$('.teaser_hide').css('display', 'block');
			<?php if (isset($teaser)) {	?>
			// You need to add a comma after the php function output if there are more entries
			jQuery.preLoadImages(<?php $teaserImages->imageList(); ?>,
								 "/media/images/plasmaMenu/menubox_top.png", 
								 "/media/images/plasmaMenu/menubox_body.png",
								 "/media/images/plasmaMenu/menubox_bottom.png");
			$('.teaser').cycle({
				fx: '<?php echo $teaserImages->options['fx']; ?>',
				timeout: <?php echo $teaserImages->options['timeout']; ?>,
				speed: <?php echo $teaserImages->options['speed']; ?>,
				next : $('#next'),
				prev : $('#previous')
			});

			var show = function(e) {
				var teaserPosition = $('.teaser').offset();
				var next = $('#next');
				var prev = $('#previous');
				var left = (teaserPosition.left + $('.teaser').width()) / 2;
				next.css({top:teaserPosition.top, left:left}).show();
				prev.css({top:teaserPosition.top+$('.teaser').height()-prev.height(), left:left}).show();
			};

			var hide = function(time) {
				$('#next').hide();
				$('#previous').hide();
			};

			var pause = function() {
				$('.teaser').cycle('pause');
			}

			var resume = function() {
				$('.teaser').cycle('resume');
			}

			$('.teaser').mouseover(show);
			$('.teaser').mouseout(hide);
			$('.teaser').mouseover(pause);
			$('.teaser').mouseout(resume);

			$('#next').mouseover(show);
			$('#previous').mouseover(show);
			
			<?php } ?>
		});
	</script>
<!--	<?php if (isset($teaser)) $teaserImages->generate(); ?>-->
<div class="intro">
<img src="images/tree.png" alt="Draw of a tree" title="<?php i18n( "Grow knowledge" ); ?>"/><h3 style="background: url(); text-indent: 0; font-size: 2em;"><?php i18n( "Welcome to KDE Edu!" ); ?></h3>
<p><?php i18n( "We make free educational software for children and adults, students and teachers. We make software for you." ); ?></p>
<p><?php i18n( "Grow your knowledge with our applications." ); ?></p> 
<p><?php i18n( "Or get involved with us in development apps." ); ?></p>
<div class="buttons"><a href="applications/all/" title="<?php i18n( "Our applications" ); ?>" class="apps"><?php i18n( "Applications" ); ?></a> <a href="get_involved/" title="<?php i18n( "Get involved" ); ?>" class="involved"><?php i18n( "Get involved" ); ?></a></div>
</div>

<p><?php i18n( "<strong>Free Educational Software based on the KDE technologies:</strong> students, parents, children, teachers, adults, you can freely use our software, copy it, modify it to your needs and enjoy learning!" ); ?></p>

<p><?php i18n( 'Please have a look at the <a href="applications/all/">applications overview page</a> to get a quick preview of our programs which are translated in more than 65 languages.' ); ?></p>

<p><?php i18n( "Our primary focus is on schoolchildren aged 3 to
18, and the specialized user interface needs of young users.  However,
we also have programs to aid teachers in planning lessons, and others
that are of interest to university students and anyone else with a
desire to learn!" ); ?></p>
<div class="columns">
<div id="rss"><h3><?php i18n( "Keep up to date" ); ?></h3>

<p><?php i18n( 'Contributors of the KDE Education Project provide news in their blogs. An aggregation of all these posts is available as <a href="rss.php" title="RSS formatted news feed">RSS feed&nbsp;</a>.' ); ?><br /><!-- <img src="pics/rss.png" class="inline" width="22" height="22" alt="RSS"/> -->
<?php i18n( 'Have a look at the KDE feed reader <a href="http://kde.org/applications/internet/akregator/" title="aKregator RSS/Atom reader">aKregator</a>.'); ?></p>
</div>

<div id="required-area">
<h3><?php i18n( "KDE-Edu on Microsoft Windows" ); ?></h3>

<p><?php i18n( 'Most KDE-Edu applications are also available on Microsoft Windows. They are provided by the KDE on Windows Initiative and are still in beta state, but already usable. Read more on the <a href="http://windows.kde.org">KDE on Windows</a> project page!' ); ?></p>
</div>
<div class="info-area">

  <h4><?php i18n( "Latest News from our Blogs" ); ?></h4>
  <?php show_news( 2 ); ?>
  <p><a href="./news/"><?php i18n( "More News &raquo;" ); ?></a></p>
</div>
</div>

<?php
  include "footer.inc";
?>
