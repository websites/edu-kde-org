 <?php
  $appinfo = new AppInfo("LabPlot");
  $appinfo->setIcon("../images/icons/labplot_32.png", "32", "32");
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2007","LabPlot Developers" );
  $appinfo->addAuthor( "Stefan Gerlach", "" );
  $appinfo->addAuthor( "Alexander Semke", "" );
  $appinfo->addAuthor( "Andreas Kainz", "" );
?>
