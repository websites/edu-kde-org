 <?php
  $appinfo = new AppInfo( "KLettres" );
  $appinfo->setIcon( "../pics/projects/ox32-apps-klettres.png", "32", "32" );
  $appinfo->setVersion( "2.0" );
  $appinfo->setCopyright( "2001", "Anne-Marie Mahfouf" );
  $appinfo->setLicense("gpl");
  $appinfo->addAuthor( "Anne-Marie Mahfouf", "annma AT kde DOT org" );
  $appinfo->addMaintainer( "Anne-Marie Mahfouf", "annma AT kde DOT org" );
  $appinfo->addContributor( "Geert Stams", "gstams DOT 1 AT hccnet DOT nl", "Dutch sounds" );
  $appinfo->addContributor( "Erik Kj&aelig;r Pedersens", "erik AT binghamton DOT edu", "Danish sounds" );
  $appinfo->addContributor( "Ludovic Grossard", "grossard AT kde DOT org", "French sounds" );
  $appinfo->addContributor( "Eva Mikul&#269;&iacute;kov&aacute;", "evmi AT seznam DOT cz", "Czech sounds" );
  $appinfo->addContributor( "Silvia Moty&#269;kov&aacute; &amp; Jozef &#344;&iacute;ha", "silviamotycka AT seznam DOT cz", "Slovak sounds" );
  $appinfo->addContributor( "Robert Wadley", "robntina AT juno DOT com", "English sounds" );
  $appinfo->addContributor( "Pietro Pasotti", "pietro AT itopen DOT it", "Italian sounds" );
  $appinfo->addContributor( "Ana Bel&eacute;n Caballero", "neneta AT iquis DOT com", "Spanish sounds" );
  $appinfo->addContributor( "Juan Pedro Paredes", "juampe AT iquis DOT com", "Spanish sounds with Ana" );
  $appinfo->addContributor( "Vikas Kharat", "kharat AT sancharnet DOT in", "Romanized Hindi sounds" );
  $appinfo->addContributor( "John Magoye and Cormac Lynch", "cormaclynch AT eircom DOT net", "Luganda sounds" );
  $appinfo->addContributor( "Helmut Kriege", "helmutkriege AT freenet DOT de", "German sounds" );
  $appinfo->addContributor( "Renaud Blanchard", "kisukuma AT chez DOT com", "Graphics" );
  $appinfo->addContributor( "Whitehawk Stormchaser", "zerokode AT gmx DOT net", "Original icon, background" );
  $appinfo->addContributor( "Robert Gogolok", "robertgogolok AT web DOT de", "Support" );
  $appinfo->addContributor( "Chris Luetchford", "chris AT os11 DOT com", "SVG icon" );
  $appinfo->addContributor( "Peter Hedlund", "peter AT peterandlinda DOT com", "Code for generating special characters icons" );
  $appinfo->addContributor( "Waldo Bastian", "bastian AT kde DOT org", "Port to KConfig XT, coding help" );
  $appinfo->addContributor( "Danny Allen", "dannya40uk AT yahoo DOT co DOT uk", "Kids and grownup SVG icons" );
  $appinfo->addContributor( "Michael Goettsche", "michael DOT goettsche AT kdemail DOT net", "Timer widget in Settings" );
?>
