<?php
  $page_title = "How to obtain KLettres";
  $site_root = "../";
  

  include( "header.inc" );
?>
<p>
KLettres should be included with your distribution in the kdeedu package.
<br />
Latest development code is in SVN repository trunk (will ship with KDE 4.4).
</p>
<?php
  show_obtain_instructions( "KLettres", "kdeedu", "false" );
?>

<hr width="30%" align="center" />
<p>
Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


