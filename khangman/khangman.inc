 <?php
  $appinfo = new AppInfo( "KHangMan" );
  $appinfo->setIcon( "../pics/projects/ox32-apps-khangman.png", "32", "32");
  $appinfo->setVersion( "2.0" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2001", "Anne-Marie Mahfouf" );
  $appinfo->addAuthor( "Anne-Marie Mahfouf", "annma AT kde DOT org" );
?>
