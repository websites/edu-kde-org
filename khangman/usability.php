<?php
  $site_root = "../";
  $page_title = 'KHangMan - Usability';
  
  include ( "header.inc" );
  ?>
<p>
A <b>Usability Report</b> about KHangMan is available <a href="http://www.obso1337.org/misc/khangman.pdf">here</a> in pdf. This report has been done by Celeste Paul the 19th August 2004. Thanks a lot for that usability study on KHangMan. 
</p>
<p>On this page are written all the improvements that are done in KHangMan following this report and following the discussions on the KDE usability mailing list. See the <a href="http://lists.kde.org/?l=kde-usability&amp;r=1&amp;w=2">archives</a> of that list for further information.<br />
These changes will be available in KHangMan version 1.5 shipped with KDE 3.4. You can also find them currently in cvs HEAD, in the kdeedu module.
</p>
<h4>Current work (not committed) - After a game</h4>
Currently: after a game is won, a dialog (message box) appears. Yes brings a new game while No terminates (quits) the programs.<br />
After a game is lost, a message box appear  and Yes brings a new game while No terminates the program.<br />
<b>Proposals</b><br />
<ul>
  <li>&nbsp;</li>
</ul>

<h4>Fixed : minor issues</h4>
<ul>
<li>Input field too large: has been reduced</li>
<li>Labels font too small: have been made bigger</li>
<li>A Guess button to enter the letter (Enter key pressed remains) has been added. </li>
<li><i>Already guessed letter</i>.<br />
To improve minor issue: intrusive dialog message. A messagebox was shown each time the user tried a letter that was already guessed. The user had to click on OK to close the dialog. Now, when an already guessed letter is tried, the letter is shown in red for 1 second with a passive popup (like a balloon). During this second, input is suspended for the user in the letter field (i.e. the user cannot type anything during this second).
</li>
<li>Implemented a suggestion from Charles: when the game is lost, in Milder pictures mode, change the message from "You are dead" to "You lost" in order to also have a milder message.</li>
</ul>

<h4>Fixed : medium issues</h4>
<ul>
<li>Level menu has been added in the Menubar (was previously only present as a combobox in the toolbar)</li>
<li><i>Choose a background</i>: this was done only in a combobox on the toolbar. The Settings->Configure KHangMan dialog has been rearranged to add this setting. <br /><br />
</li>
</ul>

<h4>Fixed : severe issues</h4>
<ul>
<li>language default: has been done for KDE 3.3. All languages data are in the corresponding kde-i18n module, this ensure that an Italian user for example, using KDE in Italian, will have Italian as words language.</li>
</ul>
<h4>Fixed: Settings dialog</h4>
<ul>
<li>Suppressed Languages menu and added Languages as a combobox in the Settings dialog </li>
<li>Better group items in the Settings dialogs.  Things to remember (thanks to Thomas) a) never ever put more then one group of radio-buttons in a (visual) groupbox and b) the title of the groupbox defines what should go in there.</li>
</ul>
<h5>Things to remember in Settings dialogs</h5>
<ul>
<li>Use correct capitalization style: for these checkbox items "Sentence style capitalization" should be used.(see <a href="http://developer.kde.org/documentation/standards/kde/style/basics/labels.html">http://developer.kde.org/documentation/standards/kde/style/basics/labels.html</a>) </li>
<li>Be careful with the accel keys: not to use twice the same in a dialog, not to use one already used in the buttons like 'o' </li>
</ul>
 <br />
 <hr width="30%" align="center" />
 <p>
 Author: Anne-Marie Mahfouf<br />
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>