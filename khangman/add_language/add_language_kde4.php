<?php
  $site_root = "../";
  $page_title = 'KHangMan - How to add a new language for KDE4';
  
  include ( "header.inc" );
  ?>
 <h3>Dear Translator,</h3>

<p>Here is a an outline of the steps necessary to translate the words of
the game to your language. Please note that you can adapt the words as you want as long as you keep words corresponding to the level.</p>
<p>KHangMan shares its files with KAnagram so you might also want to have a look at KAnagram website.</p>
<p>The words are stored in 4 separate files, one for each level. The files are in
<tt>/kvtml/data/</tt>. The file
<tt>easy.kvtml</tt> is for easy-to-guess words, the file
<tt>medium.kvtml</tt> is for level medium, the file
<tt>animals.kvtml</tt> is for guessing animals names and the file
<tt>hard.kvtml</tt> is for difficult-to-guess words.</p>
<p>English is the default and thus the only language to be shipped with KHangMan. All other language data are put in the corresponding l10n-kde4 module.
</p>
<p>You can download the source with the English files here: <a href="khangman-data.tar.gz">khangman-data.tar.gz</a>.

<p>The files use the <b>kvtml format</b>. The tag &lt;text&gt; is for the word and the tag &lt;comment&gt; is for the hint, those are within a &lt;entry&gt; tag. Please try to match the hint with the level of difficulty. The level 'Easy' will require an easy hint but the level 'Hard' will require the definition in the dictionary. Try not to use words of the same family in the hint, that would give the word away too easily!<br /><br />
An example of a kvtml file is as follow:</p>
<pre>
&lt;?xml version="1.0"?&gt; 
&lt;DOCTYPE kvtml PUBLIC "kvtml2.dtd" "http://edu.kde.org/kanagram/kvtml2.dtd"&gt; 
&lt;kvtml version="2.0"&gt; 
&lt;information&gt;
    &lt;generator&gt;converter&lt;/generator&gt;
    &lt;title&gt;Animals&lt;/title&gt;&lt;!--Translate tag content--&gt;
    &lt;comment&gt;Animals from across the planet&lt;/comment&gt;&lt;!--Translate tag content--&gt;
  &lt;/information&gt;
  &lt;identifiers&gt;
    &lt;identifier id="0" &gt;
      &lt;locale&gt;en&lt;/locale&gt;
    &lt;/identifier&gt;
  &lt;/identifiers&gt;
...
&lt;entry id="0" &gt;
      &lt;inactive&gt;false&lt;/inactive&gt;
      &lt;inquery&gt;false&lt;/inquery&gt;
      &lt;translation id="0" &gt;
        &lt;text&gt;bear&lt;/text&gt;&lt;!--Translate all text tags content--&gt;
        &lt;comment&gt;Large heavy animal with thick fur&lt;/comment&gt;&lt;!--Translate all comment tags content--&gt;
      &lt;/translation&gt;
    &lt;/entry&gt;
...
&lt;/kvtml&gt;
 </pre>
 </p>
<p>At the beginning of the file, you should translate the content of the &lt;title&gt; and &lt;comment&gt; tags which are within the &lt;information&gt; tag. The title will be the one that will appear in the Category menu of the game (where the user chooses the file to play with).
</p>
<p>Then within the &lt;identifier&gt; tag, please put your language code instead of <tt>en</tt> in &lt;locale&gt;.</p>
<p>Please don't translate the file name itself as filenames should never contain any special charaters. Thus please save your file with the same English filenames.</p>

<p>Edit the kvtml files with a text editor (it will be easier if you use the XML syntax highlighting)
   and replace each word inside the  &lt;text&gt; tag with a translated word and each hint inside a &lt;comment&gt; with a translated hint. It is not really important that the exact meaning is preserved, but try to keep the length and
   level of difficulty roughly the same.<br />
   You can include words with white space or - in them, in that case the white space or the -
   will be shown instead of the _.<br />
   Please contact <a href="mailto:annma AT kde DOT org">me</a> if there is anything special related to your language so I can adapt the code to it (especially the special and accented characters).
</p>
   <p>You can just translate the words but you can also adapt them following the level
   and add new words if you want. For example, "table" is in level easy in English but
   in your language, it can be level medium. Feel free to adapt the files to your
   language needs. The number of words in a file is not important, you can add some if you
   want.</p>

<p><b>If you have special characters</b> in your language such the accented letters in French, you can write them in a file named &lt;yourlang&gt;.txt, one character per line and save that as UTF-8. Then you also have to add in the CMakeLists.txt the following:<br />
<tt>install( FILES  &lt;yourlang&gt;.txt  DESTINATION ${DATA_INSTALL_DIR}/khangman )</tt>.</p>

   <p>Note that <b>you must use UTF-8 encoding</b> when editing the files. If your
   editor can't do this, try using kwrite or kate. When opening a file in
   kwrite or kate you can select utf8 encoding with the top right combo box.</p>

<p>You can then commit your files in  <tt>l10n-kde4/<lang_code>/data/kdeedu/khangman</tt>. Don't forget to update the CMakeLists.txt file as well.<br />
Please contact me by email (annma AT kde DOT org) if you need further information.
</p>
Good luck and many thanks!<br />

  <p>
 Anne-Marie Mahfouf<br />
 Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
 </p>
 <?php
   include "footer.inc";
 ?>
