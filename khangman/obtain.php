<?php
  $page_title = "How to obtain KHangMan";
  $site_root = "../";
  
  include( "header.inc" );
  #show_obtain_instructions( "KHangMan" );
?>

<p>
KHangMan should be included with your distribution in the kdeedu package.
<br />
Latest stable version (2.4) is in SVN branches/4.4/KDE, in the kdeedu module and was shipped with KDE 4.4.2
</p>


<p>For advanced users who want to build KHangMan from svn stable (4.4 branch), here are the instructions:</p>
<?php
  show_obtain_instructions( "KHangMan", "kdeedu", "true" );
?>

<br />
<hr width="30%" align="center" />
<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


