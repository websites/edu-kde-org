<?php
  $site_root = "../";
  $page_title = 'Support for KDE-Edu';
  include( "header.inc" );
?>

<p>The KDE Education Project is present on many places to give you support in many ways:</p>

<h3>Documentation</h3>

<p>All handbooks are online as html pages or PDF files.</p>

<p>Find <a href="http://docs.kde.org/stable/en/kdeedu/">handbooks for KDE-Edu applications</a>.</p>


<h3>UserBase Wiki</h3>

<p>KDE provides a Wiki for users. Here you get latest information about all KDE applications, tutorials and howtos.</p>

<p>Look at the <a href="http://userbase.kde.org/Applications/Education">KDE Education section on UserBase</a>.</p>


<h3>TechBase Wiki</h3>

<p>KDE provides a Wiki for developers. Here you get information about compiling and contributing KDE applications with different kind of work.</p>

<p>Look at the <a href="http://techbase.kde.org/Projects/Edu">KDE Education section on TechBase</a>.</p>

<h3>Contributions</h3>

<p>There is an extensive collection of contributions.</p>

<p>Look at the <a href="../contrib/">Contribution Section</a>.</p>

<?php
  include "footer.inc";
?> 
