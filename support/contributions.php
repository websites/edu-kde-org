<?php
  $site_root = "../";
  $page_title = 'Contributions';
  
  include ( "header.inc" );
?>

<p>Here we present some contributions to our applications. </p>

<p>All the files are distributed under free licenses. If not otherwise stated this is the GNU General Public License. You are encouraged to download and use the files according to their license.</p>

<p>And you are even more encouraged to contribute your own files or improve the existing ones! Visit the <a href="../get_involved/">Get Involved page</a>.</p>

<ul>
  <li><a href="kvtml/">Vocabulary Files</a> (old KDE3 style)</li>
  <li><a href="kvtml2/">Vocabulary Files</a> (current KDE4 style)</li>
  <li><a href="kturtle/">Logo Scripts</a></li>
  <li><a href="kig/">Kig Macros</a></li>
</ul>

<?php
  include "footer.inc";
?>
