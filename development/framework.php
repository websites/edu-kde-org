<?php
  $site_root = "../";
  $page_title = 'KDE-Edu Framework';
  
  include ( "header.inc" );
?>
<p>
A discussion was going on on #kde-edu (IRC) on 20th January 2004, you can find the log <a href="edu-jan-2005.log">here</a>. A framework for KDE-Edu is a discussion that occurs from time to time on the mailing list so we want to record here all the ideas and see which could be implemented and how.<br />
If you want to add things on this page and if you don't have CVS access, please send your ideas to <a href="mailto:annma AT kde DOT org">annma</a> and she'll put them online.
</p>

<h3>Modularity</h3>
KPart or plugin? lib? 
<ul>
<li>An interesting feature of the KParts is that they bring part of the menu in form of a GUI-XML file with them. So the menu of the framework will be adopted to the topic of the part.</li>
<li>Kontact has a structure that could be very useful for our needs.</li>
<li>A feature list for the different parts (not KParts!) of the framework would be helpful
    <ul>
    <li>a set of buttons that is used to navigate throug the program</li>
    <li>a feedback with statistic functions</li>
    <li>the kdeedupart</li>
    </ul>
</li>
</ul>

<h3>Network capabilities</h3>
<ul>
<li>login screen and saving of current user results (config file or xml?) What apps might be concerned? <em>Note:</em> Arnold started implement a login screen in KVerbos in kdenonbeta</li>
<li>how can we display the results for the user? Arnold has a visual feedback idea. See <a href="pics/feedback.png">this screenshot</a> from KVerbos.</li>
<li>Learning in teams in a network. Let's say that the class is devided in two teams. The 'blue' teams starts and every pupil of this team gets a task. Whenever a pupil has solved its task. A waiting member of the other team gets one.</li>
</ul>
<h3>GUI</h3>
<ul>
<li>use of svg format for graphics, see for a start <a href="svg.php">Albert's code</a>.</li>
<li>full-screen start?</li>
<li>different themes aimed at different ages (bigger fonts)</li>
</ul>

<h3>Already existing programs</h3>
<ul>
<li><b>KMathTool</b> (<a href="http://edu.kde.org/kmathtool">http://edu.kde.org/kmathtool</a>)</li>
<li><b>eduKator</b> (<a href="http://www.kde-apps.org/content/show.php?content=17602">http://www.kde-apps.org/content/show.php?content=17602</a>) is an attempt of an interactive program which aims to let the user experiment by animation then proposes a course (Theory) and then some exercises. The design is based on tabs.</li>
</ul>
So we could build a modular app which could hold different levels and subjects. The frame could be in kdeedu and the different subjects on a server, accessed by KNewStuff from the frame for example.
<p>
We could have such a frame for advanced topics and the same concept for KidsPlay.</p>

<br/>
<br/>
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
