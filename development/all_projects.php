<?php
  $site_root = "../";
  $page_title = 'Developed Projects';
  
  include ( "header.inc" );
?>
<h2>4.0</h2>
<p>
<table width="100%" border="0">
  <tr>
    <td><b>Program</b></td>
    <td><b>Maintainer</b></td>
    <td><b>Webpage</b></td>
  </tr>
  <tr>
    <td>Blinken</td>
    <td><a href="mailto:aacid@kde.org">Albert Astals Cid</a> </td>
    <td><a href="../blinken/">http://edu.kde.org/blinken</a></td>
  </tr>
  <tr>
    <td>Kalzium</td>
    <td><a href="mailto:CNiehaus DOT gmx DOT de">Carsten Niehaus</a><br />
           <a href="mailto:mail AT robert-gogolok DOT de">Robert Gogolok </a> </td>
    <td><a href="../kalzium/">http://edu.kde.org/kalzium</a></td>
  </tr>
  <tr>
    <td>KBruch</td>
    <td><a href="mailto:kbruch AT hpfsc DOT de">Sebastian Stein </a></td>
    <td><a href="../kbruch/">http://edu.kde.org/kbruch</a></td>
  </tr>
  <tr>
    <td>KHangMan</td>
    <td><a href="mailto:annma AT kde DOT org">Anne-Marie Mahfouf</a></td>
    <td><a href="../khangman/">http://edu.kde.org/khangman</a></td>
  </tr>
   <tr>
    <td>Kig</td>
    <td><a href="mailto:pino AT kde DOT org">Pino Toscano</a></td>
    <td><a href="../kig/">http://edu.kde.org/kig</a></td>
  </tr>
  <tr>
    <td>Kiten</td>
    <td><a href="mailto:jkerian AT gmail DOT com">Joseph Kerian</a></td>
    <td><a href="../kiten/">http://edu.kde.org/kiten</a></td>
  </tr>
  <tr>
    <td>KLettres</td>
    <td><a href="mailto:annma AT kde DOT org">Anne-Marie Mahfouf</a></td>
    <td><a href="../klettres/">http://edu.kde.org/klettres</a></td>
  </tr>
  <tr>
    <td>KmPlot</td>
    <td><a href="mailto:kd.moeller AT t-online DOT de">Klaus-Dieter M&ouml;ller</a><br />
          <a href="mailto:bmlmessmer AT web DOT de">Matthias Messmer</a></td>
    <td><a href="../kmplot/">http://edu.kde.org/kmplot</a></td>
  </tr>
  <tr>
    <td>KPercentage</td>
    <td><a href="mailto:bmlmessmer AT web DOT de">Matthias Messmer</a><br />
           <a href="mailto:mail AT robert-gogolok DOT de">Robert Gogolok </a></td>
    <td><a href="../kpercentage/">http://edu.kde.org/kpercentage</a></td>
  </tr>
  <tr>
    <td>KStars</td>
    <td><a href="mailto:kstars AT 30doradus DOT org">KStars Team</a></td>
    <td><a href="../kstars/">http://edu.kde.org/kstars</a></td>
  </tr>
  <tr>
    <td>KTouch</td>
    <td><a href="mailto:havard AT student DOT unsw DOT edu DOT au">Haavard Froeiland</a><br />
      <a href="mailto:Andreas DOT Nicolai AT gmx DOT net">Andreas Nicolai</a></td> 
    <td><a href="../ktouch/">http://edu.kde.org/ktouch</a></td>
  </tr>
  <tr>
    <td>KTurtle</td>
    <td><a href="mailto:piacentini AT kde DOT org">Mauricio Piacentini</a></td>
    <td><a href="../kturtle/">http://edu.kde.org/kturtle</a></td>
  </tr>
 <tr>
    <td>KWordQuiz</td>
    <td><a href="mailto:peter AT peterandlinda DOT com">Peter Hedlund</a></td>
    <td><a href="../kwordquiz/">http://edu.kde.org/kwordquiz</a></td>
  </tr>
 <tr>
    <td>Marble</td>
    <td><a href="mailto:tackat AT kde DOT org">Torsten Rahn</a><br />
    <a href="mailto:inge AT lysator DOT liu DOT se">Inge Wallin</a></td>
    <td><a href="../marble/">http://edu.kde.org/marble</a></td>
  </tr>
 <tr>
    <td>Parley</td>
    <td><a href="mailto:frederik.gladhorn AT kdemail DOT net">Frederik Gladhorn</a></td>
    <td><a href="../parley/">http://edu.kde.org/parley</a></td>
  </tr>
</table>
</p>

<h2>playground/edu (for inclusion in KDE 4.1)</h2>
<!--Kard <a href="mailto:dr_maux@users.sourceforge.net">Josef Spillner</a><br />
KLanguager <a href="mailto:zerokode@yahoo.com">Primoz Anzur</a><br />-->
<table width="100%" border="0">
  <tr>
    <td><b>Program</b></td>
    <td><b>Maintainer</b></td>
    <td><b>Webpage</b></td>
  </tr>
    <tr>
    <td>KEduca</td>
    <td><a href="mailto:klas.kalass AT gmx DOT de">Klas Kalass</a></td>
    <td><a href="../keduca/">http://edu.kde.org/keduca</a></td>
  </tr>
  <tr>
    <td>KVerbos</td>
    <td><a href="mailto:arnold.kraschinski AT t-online DOT de">Arnold Krashinski</a></td>
    <td><a href="../kverbos/">http://edu.kde.org/kverbos</a></td>
  </tr>
  </table>

<h2>Planned projects</h2>
<table width="100%" border="0">
  <tr>
    <td><b>Program</b></td>
    <td><b>Maintainer</b></td>
    <td><b>Webpage</b></td>
  </tr>
  <tr>
    <td>KMathTool</td>
    <td><a href="mailto:Schulz.B AT gmx DOT de">Birgit Schulz</a></td>
    <td><a href="../kmathtool/">http://edu.kde.org/kmathtool</a></td>
  </tr>
</table>

<h2>Were in KDE 3.5</h2>
<table width="100%" border="0">
  <tr>
    <td><b>Program</b></td>
    <td><b>Maintainer</b></td>
    <td><b>Webpage</b></td>
  </tr>
  <tr>
    <td>KLatin</td>
    <td><a href="mailto:gwright AT kde DOT org">George Wright</a></td>
    <td><a href="../klatin">http://edu.kde.org/klatin</a>  </td>
  </tr>
  <tr>
    <td>KMessedWords</td>
    <td><a href="mailto:annma AT kde DOT org">Anne-Marie Mahfouf</a></td>
    <td><a href="../kmessedwords/">http://edu.kde.org/kmessedwords</a></td>
  </tr>
  <tr>
    <td>KVocTrain</td>
    <td><a href="mailto:kvoctrain AT ewald-arnold DOT de">Ewald Arnold</a><br />
          <a href="mailto:khz AT klaralvdalens-datakonsult DOT se">Karl-Heinz Zimmer</a></td>
    <td><a href="../kvoctrain/">http://edu.kde.org/kvoctrain</a></td>
  </tr>
  </table>
 <br />
<hr width="30%" align="center" />
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
