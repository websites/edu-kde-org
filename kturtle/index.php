<?php
  $site_root = "../";
  $page_title = 'KTurtle';
  
  include ( "header.inc" );

  $appinfo = new AppInfo( "KTurtle" );
  $appinfo->setIcon( "../images/icons/kturtle_32.png", "32", "32" );
  $appinfo->setVersion( "0.8" );
  $appinfo->setCopyright( "2003", "Cies Breijs" );
  $appinfo->setLicense("gpl");
//   $appinfo->addAuthor( "Cies Breijs", "cies DOT breijs AT gmail DOT com" );
//   $appinfo->addContributor( "Cies Breijs", "cies DOT breijs AT gmail DOT com", "maintainer, main developer" );
//   $appinfo->addContributor( "Walter Schreppers", "Walter.Schreppers # ua DOT ac DOT be", "Author of \"wsbasic\" (wsbasic.sf.net) the base for the interpreter of KTurtle" );
//   $appinfo->addContributor( "Anne-Marie Mahfouf", "annma AT kde DOT org", "Editor part, doc, bugs fixes" );
//   $appinfo->addContributor( "Albert Astals Cid", "aacid@kde.org", "Parser Cyrillic support" );
//   $appinfo->addContributor( "Rafael Beccar", "rafael DOT beccar AT kdemail DOT net", "Perl script for generating examples from keyword file" );
  $appinfo->show();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#current">Current Development</a> |
  <a href="#news">News</a> |
  <a href="#screenshots">Screenshots</a>
]
</div>

<br />

<h3><a name="description">Description</a></h3>
<p>KTurtle is an educational programming environment for the <a href="http://kde.org">KDE Desktop</a>. KTurtle aims to make programming as easy and touchable as possible, and therefore can be used to teach kids the basics of math, geometry and... programming.</p>
<p>The programming language used in KTurtle is loosely based on <a href="http://en.wikipedia.org/wiki/Logo_programming_language">Logo</a>. 
KTurtle allows, just like some implementations of Logo, to translate the programming language (the commands, the documentation and the error messages) to the native language of the programmer. 
For more information on translating KTurtle please read <a href="translation.php">the translation how to</a>. 
Translating the programming language to the native language of the programmer is one of the many ways KTurtle tries to make learning to programming more simple. 
Other features to help to achieve this goal are: intuitive syntax highlighting, simple error messages, integrated canvas to make drawings on, integrated help function, slow-motion or step execution, and more.</p>


<br />

<h3><a name="current">Current Development</a></h3>
<p>The next release of KDE will be KDE 4.4, this release is currently in development. KTurtle follows the KDE release cycle, the next release of KTurtle will be part of KDE4.4. The KDE 4 release is a full rewrite of the 'old' 
KTurtle as it shipped with KDE 3.x. All current development effort is put into this next release of KTurtle.</p>
<p>A lot of things changed in this version of KTurtle in KDE 4. For instance the syntax of the KTurtle programming language changed: most old programs will not run. 
Also the way KTurtle is translated changed. All these changes are made to set straight mistakes that were made in the previous release, right now the KTurtle version that is in development will almost certainly grow into a 1.0 release.</p>
<p>To build the development version of KTurtle have a look at <a href="obtain.php#current">the obtain page</a>.</p>
<p>There is also a <a href="http://groups.google.com/group/kdeedu-kturtle">KTurtle google group</a> to discuss current KTurtle development issues.</p>

<br />

<?php
  kde_general_news("./news.rdf", 10, true);
?>
<a href="news.php">more news...</a><br />

<br />

<h3><a name="screenshots">Screenshots</a></h3>
<p>Some screenshots to give you an idea of the possibilities of KTurtle:</p>
<?php
  $gallery = new EduGallery("KTurtle - Screenshots");
  $gallery->addImage("pics/kturtle1_sm.png", "pics/kturtle1.png", 200, 102,  "[Screenshot]", "", "KTurtle at first start");
  $gallery->addImage("pics/kturtle2_sm.png", "pics/kturtle2.png", 200, 122,  "[Screenshot]", "", "A flower in the examples");
  $gallery->startNewRow();
  $gallery->addImage("pics/kturtle3_sm.png", "pics/kturtle3.png", 200, 149,  "[Screenshot]", "", "Nice arrow");
  $gallery->addImage("pics/kturtle4_sm.png", "pics/kturtle4.png", 200, 122,  "[Screenshot]", "", "File -> Open Example -> curly.logo");
  $gallery->show();
?>

<br />
<br />

<hr width="30%" align="center" />

<p>Authors: Anne-Marie Mahfouf and Cies Breijs<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
