<?php
  $page_title = "How to obtain KTurtle";
  $site_root = "../";
  

  include( "header.inc" );
?>

<br />

<h3><a name="stable">Getting the current stable version of KTurtle</a></h3>
<p>
If you want to use KTurtle it is best to use the packages that come with your distribution (this assumes you're running some sort of free operating system, for instance Linux). Most distributions ship KTurtle, some as a separate package, some as part of the <b>kde-edu</b> package (a package containing all <a href="http://kde.org">KDE</a>'s educational projects).</p>

<p>
If you are running Microsoft Windows or MacOSX you consider:
<ol>
<li>installing a Linux distribution or running a so called <a href="http://en.wikipedia.org/wiki/Livecd">liveCD</a>, or</li>
<li>waiting for a while, since the next version of <a href="http://kde.org">KDE</a> (KDE 4) will most likely run on Microsoft Windows and MacOSX as well.
</ol>
</p>

<br />

<h3><a name="current">Getting the current development (trunk) version of KTurtle</a></h3>

<p>
If you are interested in building the latest KTurtle (for those who don't know: KDE4 is not released yet, it is indevelopment, it takes some time/experience with builing code/courage to build), it is not that difficult, there is a nice tutorial here:<br />

<a href="http://techbase.kde.org/index.php?title=Getting_Started/Build/KDE4">http://techbase.kde.org/index.php?title=Getting_Started/Build/KDE4</a><br />

In the end of the tutorial you'll find that the writer is building "kanagram", instead of kanagram you'll be building "kturtle" ofcourse (and you dont need to build "libkdeedu") after all changes this last code block will be:
</p>

<pre>
cd
svn co -N $REPOSITORY/trunk/KDE/kdeedu
cd kdeedu
svn up cmake
svn up kturtle
ls -d */ | sed 's|\(.\+\)/|/\1/s/^#NOTCHECKEDOUT //|' | \
sed -e'/^add_subdirectory/s/^/#NOTCHECKEDOUT /' \
-f- -i CMakeLists.txt
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$KDEDIR ..
make -k
make install
</pre>


<p>
I maintain a bit of a <a href="http://websvn.kde.org/trunk/KDE/kdeedu/kturtle/KDE3_TO_KDE4_CHANGELOG?view=auto">changlog</a> from KDE3's KTurtle to the KTurtle in kde trunk (KDE4). There is also a <a href="http://groups.google.com/group/kdeedu-kturtle">google group</a> devoted to discussing KTurtle's development.
</p>

<br />
<br />

<hr width="30%" align="center" />

<p>Author: Cies Breijs<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


