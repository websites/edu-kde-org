<table border="0" cellpadding="5" cellspacing="10">

<tr>
<th colspan="2" class="contentheader">Science</th>
<th class="contentheader" style="text-align:center">Since KDE</th>
</tr>

<tr>
<td valign="top"><a href="../kalzium/index.php"><img alt="Kalzium" src="../images/icons/kalzium_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kalzium/index.php">Kalzium</a> is an application which will show you some information about
the periodic system of the elements.
</td>
<td style="text-align:center">3.1</td>
</tr>

<tr>
<td valign="top"><a href="../kstars/index.php"><img alt="KStars" align="top" src="../images/icons/kstars_32.png"  width="32" height="32"/></a></td>
<td valign="top"><a style="font-weight:bold" href="../kstars/index.php">KStars</a> is a Desktop Planetarium program.</td>
<td style="text-align:center">3.0</td>
</tr>


<tr>
<td valign="top"><a href="../marble/index.php"><img alt="Marble" align="top" src="../images/icons/marble_32.png"  width="32" height="32" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../marble/index.php">Marble</a> is a generic geographical map widget which shows the earth as a sphere.</td>
<td style="text-align:center">4.0</td>
</tr>


<tr>
<td valign="top"><a href="../step/index.php"><img alt="Step" align="top" src="../images/icons/step_32.png"  width="32" height="32" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../step/index.php">Step</a> is an interactive physical simulator. Step allows the user to easily construct his own experiments using items such as particles, rigid bodies, springs, ropes, gravitational and coulomb forces etc.</td>
<td style="text-align:center">4.1</td>
</tr>

<tr><td><br/></td><td><br/></td></tr>
<tr>
<th colspan="2" class="contentheader">New in KDE 4.4</th>
<th class="contentheader" style="text-align:center"> - </th>
</tr>
<tr>
<td valign="top"><a href="../rocs/index.php"><img alt="Rocs" align="top" src="../images/icons/rocs_32.png"  width="32" height="32" /></a></td>
<td valign="top"><a style="font-weight:bold" href="../rocs/index.php">Rocs</a> is a graph theory tool.</td>
<td style="text-align:center"> - </td>
</tr>
</table>
