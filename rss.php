<?php
// here we collect the posts into 
// a simplePie class instance $feedu
require( "site_includes/rss.inc" );

// adjust the content type to get a valid rss feed 
header( "Content-Type: application/rss+xml" );

  $entity_custom_from = false;
  $entity_custom_to = false;
  function html_entity_decode_encode_rss($data) {
    global $entity_custom_from, $entity_custom_to;
    if(!is_array($entity_custom_from) || !is_array($entity_custom_to)){
      $array_position = 0;
      foreach (get_html_translation_table(HTML_ENTITIES) as $key => $value) {
        //print("<br />key: $key, value: $value <br />\n");
        switch ($value) {
          // These ones we can skip
          case '&nbsp;':
            break;
          case '&gt;':
          case '&lt;':
          case '&quot;':
          case '&apos;':
          case '&amp;':
            $entity_custom_from[$array_position] = $key;
            $entity_custom_to[$array_position] = $value;
            $array_position++;
            break;
          default:
            $entity_custom_from[$array_position] = $value;
            $entity_custom_to[$array_position] = $key;
            $array_position++;
        }
      }
    }
    return str_replace($entity_custom_from, $entity_custom_to, $data);
  } 
  
// enclosing tags and channel tags
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
	  <title>KDE-Edu News</title>
	  <link>http://edu.kde.org/news/</link>
	  <description>News from KDE-Edu blogs</description>
	  <language>en-us</language>
	  <atom:link href="http://edu.kde.org/rss.php" rel="self" type="application/rss+xml" />
	  
<?php
	// go through the newest 20 posts and output some some valid xml 
	foreach($feedu->get_items(0,20) as $item) {
?>
	  <item>
	    <title><?php echo $item->get_title(); ?></title>
	    <link><?php echo $item->get_permalink(); ?></link>
	    <description>
	      <?php echo html_entity_decode_encode_rss( $item->get_content() ) . "\n"; ?>
	    </description>
<?php
    // go through the authors, email required 
    foreach($item->get_authors() as $author) { 
      if ($author->get_email()) echo "    <author>".$author->get_email()." (".$author->get_name().")</author>\n";
	  }
?>
	    <pubDate><?php echo $item->get_date("r"); ?></pubDate>
	    <guid isPermaLink="false"><?php echo $item->get_id(); ?></guid>
	  </item>

<?php } /* foreach end */ ?>
	</channel>
</rss>
