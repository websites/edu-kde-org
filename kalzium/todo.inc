<ul>

<li><span style="font-weight:bold">CalulationDlg()</span><br/>
<ul>
<li>    MWG works not at all</li>
<li>    we need more modes like the Nernst-Equiation</li>
<li>    new look (use .ui)</li>
</ul>
</li>

<li><span style="font-weight:bold">ValueVisualisation()</span><br/>
<ul>
<li>I think we should add a QTabWidget() in which we put perhaps 4 different views. This could be atomradius, ionradius, electronegativity, ionisationener&iacute;e</li>
<li>Perhaps we can use KSvg? At least the whole KDialog should be precious.  What about colourgradients?</li>
</ul>
</li>

<li><span style="font-weight:bold">General</span><br/>
<ul>
<li>http://www.webelements.com/webelements/elements/text/Si/key.html
There you see a small table with the neigbours of the current
element. This will be in the InfoDlg of Kalzium</li>
<li>Improve documention</li>
<li>Have more than one mode in the quiz: The user should be able
to decide a level (easy - moderate - difficult). Furthermore
the whole dialog will be redesigned</li>
<li>Printing: The user should be able to print all the information
in a table and/or in the infodlg.cpp-style</li>
</ul>
</li>

</ul>