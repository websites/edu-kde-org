<?php
  $page_title = "Kalzium - Screenshots";
  $site_root = "../";
  

  include( "header.inc" );

  $gallery = new EduGallery( "Kalzium in action" );
  $gallery->addImage( "pics/kalzium-eq-solver_thumb.png",   "pics/kalzium-eq-solver.png",     300, 161, "[Screenshot]", "The equation solver", "note that it even able to solve equations with charges" );
  $gallery->addImage( "pics/screen1_thumb.png",   "pics/screen1.png",     300, 161, "[Screenshot]", "The periodic table", "note the nice tooltip of bromine" );
  $gallery->startNewRow();
  $gallery->addImage( "pics/screen2_thumb.png",   "pics/screen2.png",     300, 211, "[Screenshot]", "Energy Information", "" );
  $gallery->addImage( "pics/screen3_thumb.png",   "pics/screen3.png",     300, 211, "[Screenshot]", "Miscellaneous Information", "In KDE 3.5 you will learn, where the names of the elements come from" );
  $gallery->startNewRow();
  $gallery->addImage( "pics/screen4_thumb.png",   "pics/screen4.png",     300, 291, "[Screenshot]", "Chemical Data", "Note the table of isotopes" );
  $gallery->addImage( "pics/screen5_thumb.png",   "pics/screen5.png",     300, 214, "[Screenshot]", "Glossary", "You can search for words or tools. The words and tools are linked with each other (see the blue 'Element')" );
  $gallery->startNewRow();
  $gallery->addImage( "pics/screen6.png",   "pics/screen6.png",     300, 266, "[Screenshot]", "Calculator", "In the toolbar on the right you can see the molecular weight of carbon dioxide gas" );
  $gallery->addImage( "pics/screen7_thumb.png",   "pics/screen7.png",     300, 98, "[Screenshot]", "Isotopes", "" );
  $gallery->show();

  include("footer.inc");
?>

