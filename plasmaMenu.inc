<?php
global $document_root, $plasmaMenu, $applicationIndex;

$plasmaMenu->iconPath = "/images/icons/";

  
  
$plasmaMenu->addMenu( i18n_var( "Applications" ), "/applications/", "red.icon.png", "#ff96af");

  $plasmaMenu->addMenuEntry( i18n_var( "By Age" ), "/applications/age/");
  foreach( $applicationIndex->getAgeCategories() as $age ) {
    $plasmaMenu->addSubMenuEntry( 
      i18n_var( $age ), 
      "/applications/" . strtolower( $age ),
      "categories/education-" . strtolower( $age ) . "_22.png"
      );
  }

  $plasmaMenu->addMenuEntry( i18n_var( "By Subject" ), "/applications/subject/");
  foreach( $applicationIndex->getSubjectCategories() as $subject ) {
    $plasmaMenu->addSubMenuEntry( 
      i18n_var( $subject ), 
      "/applications/" . strtolower( $subject ),
      "categories/education-" . strtolower( $subject ) . "_22.png"
    );
  }

  $plasmaMenu->addMenuEntry( i18n_var( "All Applications" ), "/applications/all/" );
  foreach( $applicationIndex->getAllApplications() as $application ) {
    $plasmaMenu->addSubMenuEntry( 
      $applicationIndex->getName( $application ), 
      "https://www.kde.org/applications/education/".$applicationIndex->getFileName( $application ), 
      $applicationIndex->getFileName( $application )."_22.png"
    );
  }

$plasmaMenu->addMenu( i18n_var( "Support" ), "/support/", "purple.icon.png", "#e285ff");
  $plasmaMenu->addMenuEntry( i18n_var( "Documentation" ), "http://docs.kde.org/stable/en/kdeedu/", false);   
  $plasmaMenu->addMenuEntry( i18n_var( "Forums" ), "http://forum.kde.org/viewforum.php?f=21", false);  
  $plasmaMenu->addMenuEntry( i18n_var( "User Information" ), "http://userbase.kde.org/Applications/Education", false);  
  $plasmaMenu->addMenuEntry( i18n_var( "Technical Information" ), "http://techbase.kde.org/Projects/Edu", false);
  $plasmaMenu->addMenuEntry( i18n_var( "Contributions" ), "/contrib/");
    $plasmaMenu->addSubMenuEntry( i18n_var( "Vocabulary Files (KDE3)" ), "/contrib/kvtml.php");
    $plasmaMenu->addSubMenuEntry( i18n_var( "Vocabulary Files (KDE4)" ), "/contrib/kvtml2.php");
    $plasmaMenu->addSubMenuEntry( i18n_var( "KTurtle Logo Scripts" ), "/contrib/kturtle/");
    $plasmaMenu->addSubMenuEntry( i18n_var( "Kig Macros" ), "/contrib/kig/");
  
$plasmaMenu->addMenu( i18n_var( "About Us" ), "/about_us/", "green.icon.png", "#acff08");
  $plasmaMenu->addMenuEntry( i18n_var( "News" ), "/news/");  
  $plasmaMenu->addMenuEntry( i18n_var( "Project's History" ), "/about_us/history.php");  
  $plasmaMenu->addMenuEntry( i18n_var( "Users Stories" ), "/about_us/users_stories.php"); 
  $plasmaMenu->addMenuEntry( i18n_var( "Get Involved" ), "/get_involved/");    
    $plasmaMenu->addSubMenuEntry( i18n_var( "Vocabulary Files" ), "/get_involved/kvtml.php");
    $plasmaMenu->addSubMenuEntry( i18n_var( "Logo Scripts" ), "/get_involved/kturtle.php");
    $plasmaMenu->addSubMenuEntry( i18n_var( "Kig Macros" ), "/get_involved/kig.php");
    $plasmaMenu->addSubMenuEntry( i18n_var( "Parley Themes" ), "/get_involved/parley.php");   
  $plasmaMenu->addMenuEntry( i18n_var( "Graphic Resources" ), "/contact_us/graphics.php");
    
    
$plasmaMenu->addMenu( i18n_var( "Contact Us" ), "/contact_us/", "orange.icon.png", "#ffae00");
  $plasmaMenu->addMenuEntry( i18n_var( "Mailing List" ), "http://mail.kde.org/pipermail/kde-edu/", false);
?>
