<?php
  $site_root = "../";
  $page_title = "KDE-Edu slideshows";
  

  include( "header.inc" );
?>

<p><b>You can use these talks if you need to do a presentation of KDE-Edu.</b></p>
<ol>
  <li><p>Catalan slideshow VI Jornades de Programari Lliure
Girona, juliol del 2007<br />
<a href="edu-jpl-catalan-july2007.kpr">edu-jpl-catalan-july2007.kpr</a> by Xavier Batlle i Pèlach</p></li>
  <li><p>French talk at Toulouse in Toulouse: <br />
<a href="kde-eduToulibre-june2007.pdf">kde-eduToulibre-june2007.pdf</a> by Anne-Marie Mahfouf<br />
LaTeX Beamer sources with screenshots:<br /> <a href="eduToulouse-june2007-tex-sources.tar.bz2">eduToulouse-june2007-tex-sources.tar.bz2</a></p></li>
  <li><p>KDE-Edu on Windows - KDE-Edu meeting in Paris December 2007<br />
<a href="kde-edu-meeting paris.pdf">kde-edu-meeting paris.pdf</a> by Patrick Spendrin<br />
<a href="kde-edu-meeting paris.odp">ODP file</a></p></li>
  <li><p>KDE 4.0 Release Event - KDE-Edu presentation<br />
<a href="4.0EventPresentation.odp">4.0EventPresentation.odp</a> by Jeremy Whiting</p></li>
</ol>
<br />
<br />
<?php
  include( "footer.inc" );
?>








