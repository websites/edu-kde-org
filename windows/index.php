<?php
  $translation_file = "edu-kde-org";
  require_once "functions.inc";
  $site_root = "../";
  $page_title = i18n_noop( "KDE-Edu on Windows" );
  
  include( "header.inc" );
?>

<p>Although KDE on Windows in general is not ready for public yet, KDE-Edu has nearly no bugs left and is already usable for bleeding-edge users.</p>

<p>Please refer to <a href="http://windows.kde.org/">the KDE on Windows website</a> to find out more on how to get the installer and how to build them.</p>

<br />

<div style="text-align: center;">
<?php
  $gallery = new EduGallery("Windows - Screenshot");
  $gallery->addImage("pics/kgeography_sm.png", "pics/kgeography.png", 330, 248,  "[Screenshot]", "", "KGeography on Windows XP");
  $gallery->show();
?>
</div>

<br /> <br />

<hr width="30%" align="center" />
<p>Last update: <?php echo date("Y-m-d", filemtime(__FILE__)); ?></p>

<?php
  include("footer.inc");
?>

