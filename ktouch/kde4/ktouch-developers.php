<?php
	$site_root = "../";
	$page_title = 'KTouch KDE 4 - Developer Information';

	include ( "header.inc" );
?>

<h3>General information about the program</h3>
<p>
The program has been written in C++ (of course) with the help of KDevelop and QT designer. The programming interface (API) is very similar to the API used in the QT and KDE libs, so it shouldn't be difficult to understand the source code.
<p>
The source code is available as part of the KDE source code repository, or as isolated tarball from the sourceforge release page. Note, however, that the development happens nowadays in the KDE trunk and the sourceforge releases may be well out of date.
</p>
<p>Once you have a source code copy on your compute, you can fire up the cmake based build system and start coding right away. Of course there are also numerous other ways to contribute to the project (which would be much appreciated).
<p>
<h3>Contributing to KTouch</h3>
<p>
There are several ways to help us improve KTouch. You could
<ul>
<li>code <a href="#patches">patches</a> or join the project permanently as developer</li>
<li><a href="ktouch-lectures.php">write lectures/training files</a> or improve the existing ones</li>
<li>create new <a href="keyboards-layouts.php">keyboard layouts</a></li>
<li>draw some nice icons/images for the toolbars and the preferences dialog</li>
<li>write and keep the program documentation up-to-date</li>
<li>do anything else you think that could be beneficial to the project</li>
</ul>
<p>
You are more then welcome to contribute to this (in my humble opinion) GREAT program :-)
<br>
<p>
<h1><a name="patches">&nbsp;</a>Source code contributions/Patches</h1>
<p>
If you want to fix a bug, implement a feature or just contribute and source 
code change you like to the program, feel free to send me (Andreas) a patch,
so that I can include it into the official SVN source code.
<p>
The easiest way to do that is to get a local copy of the source code on SVN (see developer into 
at <a href="http://techbase.kde.org">techbase.kde.org</a> on how to get a the KDE source code). 
If you already have the source code, make sure you work with an up-to-date version, using
<pre>
  svn update
</pre>
<p>
inside the kdeedu/ktouch directory. 
<p>
Now let's assume you fix a bug and modify the files
<pre>
    src/ktouch.cpp
    src/ktouch.h
</pre>
<p>After your changes are complete and tested, type
<pre>
    svn status
</pre>
in the kdeedu/ktouch directory. You will get an output like:
<pre>
    M      src/ktouch.h
    M      src/ktouch.cpp
</pre>
and possibly some other lines. Unless you added files yourself, ignore the lines with ? in front (these are temporary files). Note the lines
with an M in front of the file names. These lines show you the files that you modified. Now type
<pre>
    svn diff -u ktouch.h ktouch.cpp &gt; ktouch_featureX_implementation_added.patch
</pre>
or use a different descriptive name for your patch.
<p>After <b>-u</b> add all files you have modified, then a <b>&gt;</b> and afterwards the file name of your patch file. 
Send the patch file to me (preferably gzipped together with some ChangeLog info) or the kdeedu mailing list and we'll check and apply 
the patch to the SVN source code.
<br>
<p>
<br>
<h1><a name="graphics">&nbsp;</a>Designing icons and graphics</h1>
<p>
Any contributions of nice looking icons and graphics are highly appreciated. If you can draw icons that looks better than the current icons or are more fitting, please send it to us per mail (see main page for developer emails, or use the kdeedu mailing list)! 
<p>
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
	
<?php
	include "footer.inc";
?>
