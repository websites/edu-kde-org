 <?php
  include_once("../site_includes/applicationpage.inc");
  include_once("../site_includes/class_edugallery.inc");

  $app = new EduAppData("cantor");
  $appinfo = new AppInfo( "Cantor" );
  $appinfo->setIcon( "../images/icons/cantor_32.png", "32", "32");
  $appinfo->setVersion( "19.08" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2009", "Cantor Developers" );
  $appinfo->addAuthor( "Alexander Rieder", "alexanderrieder AT gmail DOT com" );
?>
