<?php
  $page_title = "How to obtain KBruch";
  $site_root = "../";
  

  include( "header.inc" );
?>
<p>
<p>
KBruch should be included with your distribution in the kdeedu package.
<br />
Latest stable version (2.0) is in SVN branches/4.0/KDE, in the kdeedu module and was shipped with KDE 4.0.0
</p>


<p>For advanced users who want to build KBruch from svn, here are the instructions:</p>
<?php
  show_obtain_instructions( "KBruch", "kdeedu", "false" );
?>


<br />
<hr width="30%" align="center" />
<p>Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


