<?php
$translation_file = "edu-kde-org";
include_once( "functions.inc" );
$page_title = i18n_noop( "Application Categories" );
include "header.inc";


echo "<h3>".i18n_var( "Age Categories" )."</h3>\n";
foreach( $applicationIndex->getAgeCategories() as $age ) {
  $applicationIndex->printCategoryBox( $age );
}

echo "<br style=\"clear:left;\" />\n";
echo "<h3>".i18n_var( "Subject Categories" )."</h3>\n";

foreach( $applicationIndex->getSubjectCategories() as $subject) {
  $applicationIndex->printCategoryBox( $subject );
}
include "footer.inc";
?>
