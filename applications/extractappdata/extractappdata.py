#!/usr/bin/python
# -*- coding: utf-8 -*-
#############################################################################
# Copyright (C) 2009 Daniel Laidig <d.laidig@gmx.de>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; see the file COPYING.LIB.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#############################################################################

"""Script to extract various application data for www.kde.org

Usage:
./extractappdata.py [OPTION]... [APP]...

If you leave out the list of applications at the end, this means "all applications".

Options:
--no-download
    Don't download any files from svn/git, just use what's already been downloaded.

--only-download
    Just downloads the files needed for the given actions, don't parse anything.

--download-icons
    Downloads application icons from svn/git.

--download-category-icons
    Download category icons from svn.

--extract-authors
    Extracts the author information from KAboutData.

--extract-sc-versions [KDE SC versions] (NOT IMPLEMENTED YET)
    Extracts a version history for all applications part of the Software Compilation (SC).
    You can give a list of SC releases to limit the number of downloaded files if you only
    want to extract information for specific releases. Otherwise just use the argument "all".
    Note: For multiple versions use spaces to separate versions in the list and enclose
    the list with quotes, e.g.:
        --extract-sc-versions "4.3.0 4.3.1"

--extract-desktop-files
    Extracts generic name and category from the desktop files.

--validate (NOT IMPLEMENTED YET)
    Validates all yaml files.

--build-index
    Build an index file containing all applications used for building the navigation menu.
    This option can't be combined with other options.

--build-edu-index
    Build (1) an index file containing all applications in the category Education, 
    (2) an index file categorized by age, and (3) an index file categorized by subject
    used for building the navigation menu.
    This option can't be combined with other options.

--resize-screenshots
    Creates resized versions of the application screenshots.

--help, -h
    Display this message and exit.
"""

from appdata import AppData
from downloader import Downloader
from parser import Parser

import sys, os, getopt

def usage():
    print __doc__

def getApplicationList():
    files = os.listdir(AppData.appDataDir)
    apps = []
    for f in files:
        if f.endswith(".json") and not f.endswith("_generated.json"):
            apps.append(f[:-5])
    return apps

def main():
    os.chdir(os.path.dirname(__file__))
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["no-download", "only-download", "download-icons", "download-category-icons", "extract-authors", "extract-sc-versions=", "extract-desktop-files", "validate", "build-index", "build-edu-index", "resize-screenshots", "help"])
    except getopt.GetoptError, err:
        print str(err)
        sys.exit(1)

    if ("--help", "") in opts or ("-h", "") in opts:
        usage()
        sys.exit(1)

    download = True
    parse = True
    if ("--only-download", "") in opts and ("--no-download", "") in opts:
        print "error: --only-download and --no-download can't be combined."
        sys.exit(1)
    if ("--only-download", "") in opts:
        parse = False
    if ("--no-download", "") in opts:
        download = False

    apps = args
    applicationList = getApplicationList()
    if len(apps) == 0: # use all applications if no specific list has been given
        apps = applicationList

    if len(set(apps) - set(applicationList)) != 0:
        print "Error: The following applications were not found:", ", ".join(set(apps) - set(applicationList))
        sys.exit(1)

    # Create AppData objects
    apps = map(lambda app: AppData(app), apps)

    downloader = Downloader()
    downloader.setApplicationList(apps)
    parser = Parser()
    parser.setApplicationList(apps)

    if ("--download-icons", "") in opts:
        if download:
            downloader.setDownloadIcons(True)
        else:
            print "Warning: --download-icons doesn't do anything with --no-download."

    if ("--download-category-icons", "") in opts:
        downloader.downloadCategoryIcons()

    if ("--extract-authors", "") in opts:
        downloader.setDownloadCodeFiles(download)
        parser.setParseCodeFiles(parse)

    scVersions = None
    for opt, arg in opts:
        if opt == "--extract-sc-versions":
            scVersions = arg
    if scVersions != None:
        if scVersions == "all":
            import json
            scReleases = open("kde-sc-releases.json", "r")
            scVersions = json.load(scReleases).keys()
            scReleases.close()
        else:
            scVersions = scVersions.split(" ")
        downloader.setDownloadScVersions(scVersions)
        parser.setParseScVersions(scVersions)

    if ("--extract-desktop-files", "") in opts:
        downloader.setDownloadDesktopFiles(download)
        parser.setParseDesktopFiles(parse)

    if ("--build-index", "") in opts:
        if len(opts) != 1:
            print "Error: --build-index can't be combined with other options."
            sys.exit(1)
        import json
        print "Building index file"
        index = {}
        for app in apps:
            try:
                category = app.generatedInfo["category"]
                if not category in index:
                    index[category] = []
                index[category].append([app.name, app.info["name"], app.generatedInfo["generic name"]])
            except:
                print "Error: skipping application", app.name, "(probably, the category and/or generic name was not found)"
        for category in index:
            index[category].sort(lambda x,y: cmp(x[1].lower(), y[1].lower()))
        f = open("../index.json", "w")
        json.dump(index, f, sort_keys=True, indent=4, separators=(',', ':'))
        f.close()

    if ("--build-edu-index", "") in opts:
        if len(opts) != 1:
            print "Error: --build-edu-index can't be combined with other options."
            sys.exit(1)
        import json
        edu_apps = []
        for app in apps:
	    category = app.generatedInfo["category"]
	    if category == "Education":
	        edu_apps.append(app)

        print "Building index file"
        index = {}
        for app in edu_apps:
            try:
                category = app.generatedInfo["category"]
                if not category in index:
                    index[category] = []
                index[category].append([app.name, app.info["name"], app.generatedInfo["generic name"]])
            except:
                print "Error: skipping application", app.name, "(probably, the category and/or generic name was not found)"
        for category in index:
            index[category].sort(lambda x,y: cmp(x[1].lower(), y[1].lower()))
        f = open("../index.json", "w")
        json.dump(index, f, sort_keys=True, indent=4, separators=(',', ':'))
        f.close()

        print "Building index file by age"
        index = {}
        for app in edu_apps:
            try:
                age = app.info["age"]
                if not age in index:
                    index[age] = []
                index[age].append([app.name, app.info["name"], app.generatedInfo["generic name"]])
            except:
                print "Error: skipping application", app.name, "(probably, the age description and/or generic name was not found)"
        for age in index:
            index[age].sort(lambda x,y: cmp(x[1].lower(), y[1].lower()))
        f = open("../age.json", "w")
        json.dump(index, f, sort_keys=True, indent=4, separators=(',', ':'))
        f.close()

        print "Building index file by subject"
        index = {}
        for app in edu_apps:
            try:
                subject = app.info["subject"]
                if not subject in index:
                    index[subject] = []
                index[subject].append([app.name, app.info["name"], app.generatedInfo["generic name"]])
            except:
                print "Error: skipping application", app.name, "(probably, the subject and/or generic name was not found"
        for subject in index:
            index[subject].sort(lambda x,y: cmp(x[1].lower(), y[1].lower()))
        f = open("../subject.json", "w")
        json.dump(index, f, sort_keys=True, indent=4, separators=(',', ':'))
        f.close()

    if ("--resize-screenshots", "") in opts:
        if len(opts) != 1:
            print "Error: --resize-screenshots can't be combined with other options."
            sys.exit(1)
        for app in apps:
            source = "../../images/screenshots/"+app.name+".png"
            dest = "../../images/screenshots/resized/"+app.name+".png"
            if not os.path.isfile(source):
                print "Screenshot for", app.name,"not found"
                continue
            ret = os.system("convert +set date:modify +set date:create -resize '540x400>' '{source}' '{dest}'".format(source=source, dest=dest))
            if ret == 0:
                print "Screenshot for", app.name, "converted"
            else:
                print "ERROR converting screenshot for", app.name

    if ("--validate", "") in opts:
        print "Warning: Validation is not yet implemented."

    if download and not downloader.isNoOp():
        downloader.run()
    if parse and not parser.isNoOp():
        parser.run()

if __name__ == "__main__":
    main()
