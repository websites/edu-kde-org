# -*- coding: utf-8 -*-
#############################################################################
# Copyright (C) 2009 Daniel Laidig <d.laidig@gmx.de>
# Copyright (C) 2009 Christian Seiler <webmaster@christian-seiler.de>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; see the file COPYING.LIB.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#############################################################################
# This script is a quick&dirty hack to extract author/credit information from C++ source files.

import re, sys

categoryMap = {
    "Game": "Games",
    "System": "System",
    "Graphics": "Graphics",
    "Education": "Education",
    "Utility": "Utilities",
    "Settings": "Settings",
    "AudioVideo": "Multimedia",
    "Network": "Internet",
    "Office": "Office",
    "Development": "Development"
}

class CodeParser:
    strconcat = re.compile('"(?:\\\\"|[^"])*"\s*', re.DOTALL)

    def __init__(self):
        pass

    def extractVersionNumber(self, f):
        pass

    def extractGenericName(self, f):
        regexp = re.compile(r'^GenericName=(.+)$')
        regexp2 = re.compile(r'^Comment=(.+)$')
        fallback = None
        for line in f:
            line = line[:-1]
            m = regexp.match(line)
            m2 = regexp2.match(line)
            if m2:
                fallback = m2.group(1)
            if not m:
                continue
            return m.group(1)
        return fallback

    def extractCategory(self, f):
        regexp = re.compile(r'^Categories=(.+)$')
        categories = None
        for line in f:
            line = line[:-1]
            m = regexp.match(line)
            if not m:
                continue
            categories = m.group(1).split(";")
            break
        if not categories:
            return None
        for category in categories:
            if category in categoryMap:
                return categoryMap[category]
        print "    Error: No known category in", categories

    # Contact C++ strings
    def concatstr(self, param):
        result = None
        p = self.strconcat.match(param)
        while p:
            if result == None:
                result = p.group(0).strip()
            else:
                result = result[:-1] + p.group(0).strip()[1:]
            param = param[p.end():]
            p = self.strconcat.match(param)
        return result

    def findVariable(self, name, string):
        vdecl_start1 = '\\bchar\s*\*\s*'
        vdecl_end1 = '\s*=\s*((?:"(?:[^"]|\\\\")*"\s*)*)'
        vdecl_start2 = '\\bchar\s*'
        vdecl_end2 = '\s*\[\]\s*=\s*((?:"(?:[^"]|\\\\")*"\s*)*)'

        re1 = re.compile(vdecl_start1 + re.escape(name) + vdecl_end1)
        re2 = re.compile(vdecl_start2 + re.escape(name) + vdecl_end2)

        m = re1.search(string)
        if (m):
            return self.        concatstr(m.group(1))
        m = re2.search(string)
        if (m):
            return self.concatstr(m.group(1))

        return None

    def getParameters(self, string):
        # regexp mojo for braces, two levels of nesting
        braces = re.compile('\(((?:"(?:[^"]|\\\\")*"|[^()"]+|\((?:"(?:[^"]|\\\\")*"|[^()"]+)*\))*)\)', re.DOTALL)

        # regexp mojo for single parameter
        single_param = re.compile('((?:"(?:[^"]|\\\\")*"|[^()",]+|\((?:"(?:[^",]|\\\\")*"|[^()",]+)*\))+)(,|$)', re.DOTALL)

        string = string.replace('(KDE_VERSION_MINOR)', '') #KGet uses 3 levels of nesting, remove the problematic parts for now
        string = string.replace('(KDE_VERSION_RELEASE)', '')

        params = braces.match(string)

        if params == None:
            raise Exception("Not-parsable C++ syntax!")

        result = []

        param = params.group(1)
        p = single_param.match(param)
        while p:
            result.append(p.group(1).strip())
            param = param[p.end():]
            p = single_param.match(param)

        return result

    def extractAuthors(self, f):
        authors = []
        credits = []

        contents = f.read()

        comment1 = re.compile('^\s*//.*\n', re.IGNORECASE)
        comment2 = re.compile('^\s*/\*.*?\*/', re.DOTALL | re.IGNORECASE)
        preproc = re.compile('/\s*#.*\n', re.IGNORECASE)
        i18n = re.compile('(?:I18N_NOOP|ki18n)\s*\(\s*((?:"(?:[^"]|\\\\")*"\s*)+)\s*\)', re.DOTALL)
        i18n3 = re.compile('(?:I18N_NOOP|ki18n)\s*\(\(\s*((?:"(?:[^"]|\\\\")*"\s*)+)\s*\)\)', re.DOTALL) # hack for ki18n(("KAlgebra"))
        # for references
        i18n2 = re.compile('ki18n\s*\(\s*([A-Za-z][A-Za-z0-9_]*)\s*\)', re.DOTALL)

        i18nc = re.compile('(?:ki18nc)\s*\(\s*"(?:[^"]|\\\\")*"\s*,\s*((?:"(?:[^"]|\\\\")*"\s*)+)\s*\)', re.DOTALL)
        i18nc2 = re.compile('ki18nc\s*\(s*"(?:[^"]|\\\\")*"\s*,\s*([A-Za-z][A-Za-z0-9_]*)\s*\)', re.DOTALL)

        odecl = re.compile('KAboutData\s+([A-Za-z][A-Za-z0-9_]*)\s*\(')

        contents = contents.replace('KLocalizedString()', '""')
        contents = comment1.sub('', contents)
        contents = comment2.sub('', contents)
        contents = preproc.sub('', contents)
        contents = i18n.sub(lambda match: self.concatstr(match.group(1)), contents)
        contents = i18n3.sub(lambda match: self.concatstr(match.group(1)), contents)
        contents = i18n2.sub(lambda match: match.group(1), contents)
        contents = i18nc.sub(lambda match: self.concatstr(match.group(1)), contents)
        contents = i18nc2.sub(lambda match: match.group(1), contents)

        match = odecl.search(contents)
        if match == None:
            print "    Error: Could not find KAboutData declaration in source."
            return

        variable = match.group(1)

        params = self.getParameters(contents[match.end()-1:])

        if len(params) < 4:
            print "    Error: Constructor of KAboutData called with less than 4 parameters."
            return

        appname = params[0]
        progname = params[2]
        version = params[3]
        license = None
        if len(params) >= 6:
            license = params[5]
            if license.startswith("KAboutData::License_"):
                license = license[20:]
            else:
                print "    Error: wrong license format:", license
                license = None
        else:
            print "    Warning: Constructor of KAboutData called without license"

        if len(params) > 4:
            description = params[4]
        else:
            description = None

        # Reference to variable
        if appname[0] != '"':
            appname = self.findVariable(appname, contents)
        if progname[0] != '"':
            progname = self.findVariable(progname, contents)
        if version[0] != '"':
            version = self.findVariable(version, contents)
        if description != None and description[0] != '"':
            description = self.findVariable(description, contents)

        #if appname == None:
            #print >> sys.stderr, "Could not determine app name."
        #else:
            #appname = appname[1:-1].decode('string_escape')
            #print "App Name: %s" % appname

        #if progname == None:
            #print >> sys.stderr, "Could not determine program name."
        #else:
            #progname = progname[1:-1].decode('string_escape')
            #print "Program name: %s" % progname

        #if description == None:
            #print >> sys.stderr, "Could not determine program description."
        #else:
            #description = description[1:-1].decode('string_escape')
            #print "Program description: %s" % description

        if version == None:
            print >> sys.stderr, "    Could not determine program version."
        else:
            version = version[1:-1].decode('string_escape')
            #print "Program version: %s" % version

        authordecl = re.compile(re.escape(variable) + '\.addAuthor\s*\(')
        creditdecl = re.compile(re.escape(variable) + '\.addCredit\s*\(')
        licensedecl = re.compile(re.escape(variable) + '\.setLicense\s*\(')

        ptr = contents
        m = authordecl.search(ptr)
        while m:
            params = self.getParameters(ptr[m.end()-1:])

            name = params[0][1:-1].decode('string_escape')
            if len(params) > 1:
                task = params[1][1:-1].decode('string_escape')
            else:
                task = None
            if len(params) > 2:
                email = params[2][1:-1].decode('string_escape')
            else:
                email = None
            if len(params) > 3:
                web = params[3][1:-1].decode('string_escape')
            else:
                web = None

            #print "Found author: %s" % repr((name,task,email,web))
            authors.append([name,task,email,web])

            ptr = ptr[m.end():]
            m = authordecl.search(ptr)

        ptr = contents
        m = creditdecl.search(ptr)
        while m:
            params = self.getParameters(ptr[m.end()-1:])

            name = params[0][1:-1].decode('string_escape')
            if len(params) > 1:
                task = params[1][1:-1].decode('string_escape')
            else:
                task = None
            if len(params) > 2:
                email = params[2][1:-1].decode('string_escape')
            else:
                email = None
            if len(params) > 3:
                web = params[3][1:-1].decode('string_escape')
            else:
                web = None

            #print "Found credit: %s" % repr((name,task,email,web))
            credits.append([name,task,email,web])

            ptr = ptr[m.end():]
            m = creditdecl.search(ptr)

        ptr = contents
        m = licensedecl.search(ptr)
        while m:
            params = self.getParameters(ptr[m.end()-1:])

            license = params[0]
            if license.startswith("KAboutData::License_"):
                license = license[20:]
                print "    Found license set with \"setLicense()\""
            else:
                print "    Error: wrong license format:", license
                license = None

            ptr = ptr[m.end():]
            m = licensedecl.search(ptr)

        return (authors, credits, version, license)
