#!/usr/bin/python
# -*- coding: utf-8 -*-
#############################################################################
# Copyright (C) 2009 Daniel Laidig <d.laidig@gmx.de>
#
# This script is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This script is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; see the file COPYING.LIB.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#############################################################################

from scmdownloader import ScmDownloader
from codeparser import CodeParser

import json, os

class AppData:
    appDataDir = "../apps"
    tempDir = "appdata-temp"

    def __init__(self, name):
        self.name = name
        f = open(self.appDataDir + "/" + name + ".json", "r")
        try:
            self.info = json.load(f)
        except:
            print "failed to load data file for application", name
            exit(1)
        f.close()

        self.generatedInfoFileName = self.appDataDir + "/" + name + "_generated.json"
        if os.path.isfile(self.generatedInfoFileName):
            f = open(self.generatedInfoFileName, "r")
            self.generatedInfo = json.load(f)
            f.close()
        else:
            self.generatedInfo = {}

    def __del__(self):
        f = open(self.generatedInfoFileName, "w")
        json.dump(self.generatedInfo, f, sort_keys=True, indent=4, separators=(',', ':'))

    def isValid(self):
        entries = set(["name", "repository", "icon"])
        keys = set(self.info.keys())

        valid = (keys <= entries)
        if not valid:
            print "AppData for", self.name, "does contain the following invalid entries:", ", ".join(keys - entries)
        return valid

    def codeFileName(self):
        return self.tempDir+"/"+self.name+".cpp"

    def desktopFileName(self):
        return self.tempDir+"/"+self.name+".desktop"
