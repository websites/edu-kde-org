<?php
$translation_file = "edu-kde-org";
include_once( "functions.inc" );
include('../site_includes/applications.inc');

pageCategory( $cat, $pretty_cat, $is_subcategory );

function function_to_get_page_title()
{
	pageCategory( $cat, $pretty_cat, $is_subcategory );
	if( !$is_subcategory ) {
		if( $cat == "all" ) {
			$page_title = i18n_var( "%1 Applications", $pretty_cat );
		} else {
			$page_title = i18n_var( "%1 Categories", $pretty_cat );
		}
	} else {
		$page_title = i18n_var( "%1 Applications", $pretty_cat );
	}
	return $page_title;
}

if( !$is_subcategory ) {
	if( $cat == "all" ) {
		include "header.inc";

		foreach( $applicationIndex->getAllApplications() as $app ) {
			$applicationIndex->printapplicationBox( $app );
		}
	}
	else {
		include "header.inc";

		foreach( $applicationIndex->getSubcategories( $cat ) as $sub ) {
			$applicationIndex->printCategoryBox( $sub );
		}
	}
}
else {
	include "header.inc";

	foreach( $applicationIndex->getApplications( $pretty_cat ) as $app ) {
		$applicationIndex->printApplicationBox( $app, $cat );
	}
}

include "footer.inc";
?>
