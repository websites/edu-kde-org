<?php
  $site_root = "../";
  $page_title = "Pairs";
  
  include( "header.inc" );
  
  $appinfo = new AppInfo( "Pairs" );
  $appinfo->setIcon( "../images/icons/pairs_32.png", "32", "32" );
  $appinfo->setVersion( "1.0" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2012", "Aleix Pol Gonzalez" );
  $appinfo->addAuthor( "Aleix Pol Gonzalez", "aleixpol@kde.org" );
  $appinfo->addAuthor( "Marco Calignano", "marco.calignano@gmail.com" );
  $appinfo->show();
?>

<br />

<center>
<?php
  $gallery = new EduGallery("Pairs - Screenshot");
  $gallery->addImage("pics/pairs_sm.png", "pics/pairs-greenpeople.png", 330, 309,  "[Screenshot]", "", "Pairs");
  $gallery->show();
  ?>
 </center>

<p>Pairs is a game thought to improve the memory capabilities while playing. It has different modalities, from the more common where you pair the same repeated image to pairing images to sounds.</p>

<br />
<hr width="30%" align="center" />
<p>Page author: Aleix Pol Gonzalez<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include("footer.inc");
?>

