<?php
  $site_root = "../";
  $page_title = 'KStars - Screenshots';
  

  include ( "header.inc" );
  ?>

<div id="quicklinks">
[
  <a href="#new_kstars_0.9.1">New KStars 0.9.1 Screenshots</a> |
  <a href="#kstars_0.9">KStars 0.9 Screenshots</a> |
  <a href="#older">Older screenshots</a>
]
</div>

<h2><a name="new_kstars_0.9.1">New KStars 0.9.1 Screenshots</a></h2>

<table bgcolor="AAAAFF" align="center" cellpadding="2" width="700">
<tr>
<td>
<center><b>Scorpius Rising</b></center>
New: The Infopanel has been replaced with Text Boxes <br />drawn directly on the map.
</td>
<td>
<center><b>Scorpius Rising, II</b></center>
New: The Text Boxes can be "unshaded" to reveal more data.
</td>
</tr>
<tr>
<td align="center">
<a href="screens0.9/screen0a.png"><img border="0" src="screens0.9/screen0as.png" width="342" height="289"/></a>
</td>
<td align="center">
<a href="screens0.9/screen0b.png"><img border="0" src="screens0.9/screen0bs.png" width="342" height="289"/></a>
</td>
</tr>
<tr>
<td>
<center><b>Trifid Nebula Details</b></center>
New: The new Object Details Window, showing data for the Trifid Nebula.
</td>
<td>
<center><b>Orion Sets</b></center>
</td>
</tr>
<tr>
<td align="center">
<a href="screens0.9/screen0c.png"><img border="0" src="screens0.9/screen0cs.png" width="342" height="289"/></a>
</td>
<td align="center">
<a href="screens0.9/screen0.png"><img border="0" src="screens0.9/screen0s.png" width="300" height="277"/></a>
</td>
</tr>
</table>

<h2><a name="kstars_0.9">KStars 0.9 Screenshots:</a></h2>

<table bgcolor="AAAAFF" align="center" cellpadding="2" width="700">
<tr>
<td>
<center><b>The Great Galaxy in Andromeda</b></center>
New: objects have correct angular size and position angle.
</td>
<td>
<center><b>The Markarian Chain of Galaxies in Virgo</b></center>
New: more examples showing angular sizes and position angles.
</td>
</tr>
<tr>
<td align="center">
<a href="screens0.9/screen1.png"><img border="0" src="screens0.9/screen1s.png" width="342" height="274"/></a>
</td>
<td align="center">
<a href="screens0.9/screen2.png"><img border="0" src="screens0.9/screen2s.png" width="342" height="274"/></a>
</td>
</tr>
<tr>
<td>
<center><b>Conjunction of Planets, 2002</b></center>
New: Planet name labels; large, negative time step
</td>
<td>
<center><b>Charting the Northern Winter Sky</b></center>
New: Infopanel and Toolbars hidden.
</td>
</tr>
<tr>
<td align="center">
<a href="screens0.9/screen3.png"><img border="0" src="screens0.9/screen3s.png" width="342" height="274"/></a>
</td>
<td align="center">
<a href="screens0.9/screen4.png"><img border="0" src="screens0.9/screen4s.png" width="342" height="274"/></a>
</td>
</tr>
<tr>
<td>
<center><b>Veil Nebula with Menu</b></center>
New: View Toolbar anchored on left side of window; transit time in popup menu
</td>
<td>
<center><b>The Magellanic Clouds from Perth, Australia</b></center>
</td>
</tr>
<tr>
<td align="center">
<a href="screens0.9/screen5.png"><img border="0" src="screens0.9/screen5s.png" width="342" height="275"/></a>
</td>
<td align="center">
<a href="screens0.9/screen6.png"><img border="0" src="screens0.9/screen6s.png" width="342" height="274"/></a>
</td>
</tr>
<tr>
<td>
<center><b>Saturn</b></center>
New: Planets are also drawn with realistic angular sizes and position angles
</td>
<td>
<center><b>KStars Calculator</b></center>
New: the entire calculator is new; here you can access many astronomical algorithms.
</td>
</tr>
<tr>
<td align="center">
<a href="screens0.9/screen7.png"><img border="0" src="screens0.9/screen7s.png" width="342" height="274"/></a>
</td>
<td align="center">
<a href="screens0.9/screen8.png"><img border="0" src="screens0.9/screen8s.png" width="323" height="237"/></a>
</td>
</tr>
<tr>
<td>
<center><b>KStars, without the stars!</b></center>
</td>
<td>
<center><b>There's Polaris, but it's nowhere near the coordinate pole?!</b></center>
(Hint: check the date, and think about precession :)
</td>
</tr>
<tr>
<td align="center">
<a href="screens0.9/screen9.png"><img border="0" src="screens0.9/screen9s.png" width="342" height="274"/></a>
</td>
<td align="center">
<a href="screens0.9/screen10.png"><img border="0" src="screens0.9/screen10s.png" width="342" height="274"/></a>
</td>
</tr>
</table>

<h2><a name="older">Older screenshots:</a></h2>
<p>
<b>KStars 0.8:</b><br />
<a href="screens0.8/kstars1.png">Orion and Jupiter Rising</a><br />
<a href="screens0.8/kstars2.png">The Northern Milky Way</a><br />
<a href="screens0.8/kstars3.png">The new color configuration tab</a><br />
<a href="screens0.8/kstars4.png">Planetary conjunction of 25 Apr 2000</a><br />
<a href="screens0.8/kstars5.png">Showing bright stars only</a><br />
<a href="screens0.8/kstars6.png">The same view with all options enabled</a><br />
</p>
<p>
<b>KStars 0.7:</b><br />
<a href="screens0.7/kstars1.png">Observing Mars in Sagittarius</a><br />
<a href="screens0.7/kstars2.png">Observing The Moon and Saturn in Taurus</a><br />
<a href="screens0.7/kstars3.png">Zoomed in on the Moon</a><br />
<a href="screens0.7/kstars4.png">Identifying thr Trifid Nebula</a><br />
<a href="screens0.7/kstars5.png">Change Location Dialog</a><br />
<a href="screens0.7/kstars6.png">Find Object Dialog</a><br />
<a href="screens0.7/kstars7.png">KStars in French</a><br />
<a href="screens0.7/kstars8.png">The heart of the Virgo Cluster of Galaxies</a>
</p>
<p>
<b>KStars 0.5:</b><br />
<a href="screens0.5/kstars1.png">Orion and Jupiter Rising</a><br />
<a href="screens0.5/kstars3.png">Saturn in Taurus</a><br />
<a href="screens0.5/kstars_hst.png">Viewing an HST image of NGC 4013</a><br />
</p>
<p>
For a historical comparison of KStars screenshots, see our
<a href="history/index.html">Evolution of KStars</a> feature.
</p>

<hr />
<p>
KStars: copyright 2001 by Jason Harris<br />
<a href="mailto:kstars@30doradus.org"><i>kstars@30doradus.org</i></a>
</p>

<?php
  include "footer.inc";
?>
