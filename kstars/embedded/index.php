<?php
  $site_root = "../../";
  $page_title = 'KStars/Embedded:  The Universe, in your hands!';
  
  include ( "header.inc" );
?>

<p>
KStars has been ported to the
<a href="http://www.trolltech.com/products/qtopia/">Qtopia</a>
palmtop computing environment!  The port is based on the Kstars-0.8.5
release, and it was done entirely by S&eacute;bastien Prud'homme; we just
received his tarball out of the blue.  Thanks, S&eacute;bastien!
</p>
<p>
Unfortunately, S&eacute;bastien has informed us that the port isn't really usable
on a PDA at this time.  He developed it using a Qtopia emulator on a
desktop computer.  The problem appears to be that the memory and CPU
requirements are much too high.  I have some suggestions for how to
make KStars more PDA-friendly:
</p>

<ul>
<li> remove the messier images, and maybe planet images too.</li>
<li> remove most of the stars catalog (you could cut it down to
6000 stars by stopping at 6th magnitude, the naked-eye limit)</li>
<li> remove the IC catalog, and maybe NGC too (if needed)</li>
<li> remove the milky way, or at least reduce its number of points.</li>
<li> remove the coordinate grid, or reduce its number of points.</li>
<li> remove the lower zoom settings, so that thousands of objects cannot be
displayed at one time</li>
<li> use simplified planet position calculations?</li>
</ul>
<p>
I'm too busy developing regular KStars at the moment to give this a look;
if you'd like to give it a shot, you can <a href="http://kstars.sourceforge.net/kstars-embedded-0.8.5.tar.gz">download KStars/E here</a>.
</p>
<p>
Here are some screenshots.  It looks really cool, I hope it will be
usable soon!
</p>
<p>
<img src="screenshot1.png" alt="screen1" /><img src="/pics/clear-dot.png" width="15" height="15" alt=" " border="0" />
<img src="screenshot2.png" alt="screen2" /><img src="/pics/clear-dot.png" width="15" height="15" alt=" " border="0" />
</p><p>
<img src="screenshot3.png" alt="screen3" /><img src="/pics/clear-dot.png" width="15" height="15" alt=" " border="0" />
<img src="screenshot4.png" alt="screen4" /><img src="/pics/clear-dot.png" width="15" height="15" alt=" " border="0" />
</p><p>
<img src="screenshot5.png" alt="screen5" /><img src="/pics/clear-dot.png" width="15" height="15" alt=" " border="0" />
</p>
<hr />
<p>
Jason Harris
<a href="mailto:kstars@30doradus.org">kstars@30doradus.org</a>
</p>

<?php
  include "footer.inc";
?>
