<?php
  $site_root = "../";
  $page_title = 'Installing KStars';
  
  include ( "header.inc" );
?>

<h3>Choose your OS</h3>

<p>
Choose your operating system for specific instructions. If your operating system doesn't provide binaries (executable programs), you
may need to build KStars from source code.
</p>
<br />
<center>
<div id="quicklinks">
[
   <a href="#windows">Microsoft Windows</a>
   <a href="#gnulinux">GNU/Linux</a>
   <a href="#macos">Macintosh</a>
   <a href="#fromsources">Building from sources</a>
]
</div>
</center>
<br />

<h3><a name="windows">Microsoft (R) Windows (TM)</a></h3>
<p>Windows 10+ 64bit installer <a href="https://www.indilib.org/jdownloads/kstars/kstars-3.6.3.exe">KStars 3.6.3</a></p>
<p>Use <a href="httpss://sourceforge.net/projects/quickhash/">Quick Hash GUI</a> to verify the integrity of the executable file. The following are MD5 and SHA256 hashes:</p>
<ul>
<li><a href="https://www.indilib.org/jdownloads/kstars/kstars-3.6.3.exe.sha256">SHA256</a></li>
</ul>
<h3><a name="macos">MacOS</a></h3>
<p><a href="https://www.indilib.org/jdownloads/kstars/kstars-3.6.3.dmg">KStars 3.6.3 DMG Installer</a> is available for MacOS 10.15+. After mounting the DMG, please follow the instructions within the DMG.</p>
<ul>
<li><a href="https://www.indilib.org/jdownloads/kstars/kstars-3.6.3.dmg.sha256">SHA256</a></li>
</ul>
<h3><a name="gnulinux">GNU/Linux</a></h3>
<p>On most GNU/Linux distributions, KStars can be installed from your desktop environment software center (eg: Discover). If not found, you can install the latest KStars on Ubuntu/Mint by typing the following commands in the console:</p>
<pre>
sudo apt-add-repository ppa:mutlaqja/ppa
sudo apt-get update
sudo apt-get install indi-full kstars-bleeding
</pre>
</p>

<h3><a name="fromsources">Building from source</a></h3>
<p>See <a href="httpss://techbase.kde.org/Projects/Edu/KStars/Building_KStars_KF5">Building KStars from Source</a> for details.</p>

<?php
  include "footer.inc";
?>
