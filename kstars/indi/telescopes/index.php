<?php
  $site_root = "../../";
  $page_title = 'Telescopes supported under KStars';

  include ( "header.inc" );
?>

<a href="indi-welcome.png">
   <img alt="[Screenshot]" src="indi-welcome.png" width="600" />
</a>

<br />
<div id="quicklinks">
[
  <a href="#Overview">Overview</a> |
  <a href="#Telescopes">Telescopes</a> |
  <a href="#FAQ">FAQ</a> |
]
</div>
<br />

<h3><a name="Overview">Overview</a></h3> <p> <img alt="Device Manager" src="device_manager.png" style="float:right"><b>KStars</b> supports
a large number of astronomical instruments provided by the <a href="http://www.indilib.org">INDI Library</a>. Using KStars Telescope Wizard, you can let KStars automatically scan ports for connected telescopes. Alternatively, you can start the driver for your telescope model in KStars Device Manager. Once a telescope is connected, you can command it either via INDI Control Panel, or directly form KStars SKy Map. Just right click on any object, and you will find your telescope listed in the popup menu. Using the context menu, you can issue commands such as <i>Slew</i>, <i>Track</i>, <i>Sync</i>, and <i>Park</i> in addition to motion commands. KStars assumes that you telescope is already powered, aligned, and ready for use. At your option, you can utilize Ekos <i>Align</i> module to help you in your polar alignment using the drift method. You can either control a telescope connected directly to your PC, or remotely. To connect to a remote telescope, add an INDI server host in the client tab of the device manager along with a port number. INDI server may run on many embedded devices, including Rasperry Pi.
 </p> <hr />

<h3><a name="Telescopes">Telescopes</a></h3><p>
The following is a list of telescopes supported under KStars:</p>
<table style="border: 1px solid black;" width="100%">
	<tr>
		<th>Manufaturer</th>
		<th>Model</th>
		<th>Image</th>
		<th>Driver</th>
		<th>Support Status</th>
	</tr>

	<tr style="border: 1px solid black;">
		<td rowspan="2" style='vertical-align:middle;border: 1px solid black;'>Meade</td>
		<td style='vertical-align:middle;border: 1px solid black;'>LX200 Classic</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="lx200_classic.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_lx200_classic</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>LX200 Autostar, all models (LX200, LX800, ETX..etc)</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="lx200_autostar.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_lx200_autostar, indi_lx200_gps</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Celestron</td>
		<td style='vertical-align:middle;border: 1px solid black;'>Nexstar, all models.</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="nexstar.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_celestron_gps</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Orion</td>
		<td style='vertical-align:middle;border: 1px solid black;'>Atlas/Siruis</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="orion.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_synscan</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Takahashi</td>
		<td style='vertical-align:middle;border: 1px solid black;'>Temma</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="temma.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_temma</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Astro-Physics</td>
		<td style='vertical-align:middle;border: 1px solid black;'>GTO V2 Mount</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="astrophysics.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_lx200ap</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>iOptoron</td>
		<td style='vertical-align:middle;border: 1px solid black;'>iEQ45</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ieq45.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_ieq45</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>SkyEng</td>
		<td style='vertical-align:middle;border: 1px solid black;'>SkyCommander</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="skycommander.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_skycommander</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>

	<tr style="border: 1px solid black;">
		<td style='vertical-align:middle;border: 1px solid black;'>Custom</td>
		<td style='vertical-align:middle;border: 1px solid black;'>Argo Navis, Losmandy Gemini, Mel Bartels, EQ-6 MCU, HEQ-5/6 MCU</td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="heq6.png"></td>
		<td style='vertical-align:middle;border: 1px solid black;'><i>indi_lx200_basic</i></td>
		<td style='vertical-align:middle;border: 1px solid black;'><img src="ok.png"></td>
	</tr>




</table>

<hr/>


<h3><a name="FAQ"></a></h3>
<?php
 $faq = new FAQ();
  $faq->addQuestion("When I try to Connect, KStars reports that the telescope is not connected to the serial/USB port. What can I do?",
    "This message is triggered when KStars cannot communicate with the telescope. Here are few things you can do:

    <ul><li>Check that you have both reading and writing permission for the port you are trying to connect to.</li>

    <li>Check the connection cable, make sure it is in good condition and test it with other applications.</li>

    <li>Check your telescope power, make sure the power is on and that the telescope is getting enough power.</li>

    <li>Set the correct port in the INDI Control Panel under the Devices menu. The default device is /dev/ttyS0</li>

    <li>Restart KStars and retry again.</li></ul>");

$faq->addQuestion("Can I use KStars to do guiding?", "If your telescope support Pulse commands, then KStars Ekos guiding module can autoguide your telescope using a CCD or a web cam.");

$faq->addQuestion("KStars reports that the telescope is online and ready, but I cannot find the telescope's crosshair, where is it?",
    "KStars retrieves the telescopes RA and DEC coordinates upon connection. If your alignment was performed correctly, then you should see the crosshair around your target in the Sky Map. However, the RA and DEC coordinates provided by the telescope may be incorrect (even below the horizon) and you need to Sync your telescope to your current target. You can use the right-click menu to center and track the telescope crosshair in the sky map.");


$faq->addQuestion("The telescope is moving erratically or not moving at all. What can I do?",
    "This behavior is mostly due to incorrect settings, please verify the following check list:

    <ol><li>Is the telescope aligned?</li>

    <li>Is the telescope alignment mode correct? Use INDI Control Panel to check and change these settings (Alt/Az,Polar, Land).</li>

    <li>Are the telescope's time and date settings correct?</li>

    <li>Are the telescope's longitude and latitude settings correct?</li>

    <li>Is the telescope's UTC offset correct?</li>

    <li>Are the telescope's RA and DEC axis locked firmly?</li>

    <li>Is the telescope's N/S switch (when applicable) setup correctly for your hemisphere?</li>

    <li>Is the cable between the telescope and computer in good condition?</li></ol>");



 
  $faq->show();
?>

<hr/>

<br />
<hr width="50%" align="center" />
<p>Webmasters: Jasem Mutlaq<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
