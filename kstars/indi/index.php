<?php
  $site_root = "../../";
  $page_title = 'Devices supported under KStars';
  
  include ( "header.inc" );
?>

<a href="indi-welcome.png">
   <img alt="[Screenshot]" src="indi-welcome.png" width="600" />
</a>

<br />
<div id="quicklinks">
[
  <a href="#Overview">Overview</a> |
  <a href="#news">News</a> |
  <a href="ekos/">Ekos</a> |
  <a href="telescopes/">Telescopes</a> |
  <a href="ccds/">CCDs</a> |
  <a href="focusers/">Focusers</a> |
  <a href="filters/">Filters</a> |
  <a href="others/">Others</a> |
  <a href="#credits">Credits</a> |
]
</div>
<br />

<h3><a name="Overview">Overview</a></h3> <p> <b>KStars</b> supports
a large number of astronomical instruments provided by the <a href="http://www.indilib.org">INDI Library</a>.
This includes <a href="telescopes/">telescopes</a>, <a href="ccds/">CCDs</a>, <a href="focusers/">focusers</a>, 
<a href="filters/">filters</a>, domes, spectrometers, and auxliary devices.
Using KStars native INDI support means you have complete control over your instruments under Linux.
In addition to astronomical devices support, KStars provides <a href="ekos/">Ekos</a>, a complete tool for astrophotography. With Ekos, you can
align and guide your telescope, focus your CCD, and capture images using an easy intuitive interface. Furthermore, KStars FITSViewer tool
provides a simple viewer for FITS images that is capable of detecting stars and applying various filters to aid you in your astrophtography decisions.
Those features make KStars/INDI a very attractive choice for amateur and professional astronomers.
 </p> <hr />

<?php
  kde_general_news("./news.rdf", 10, true);
?>

<hr />

<h3><a name="Credits">Credits</a></h3>
<p>
<strong>Current Maintainer:</strong> Jasem Mutlaq <mutlaqja at ikarustech dot com> <br />
</p>


<br />
<hr width="50%" align="center" />
<p>Webmaster: Jasem Mutlaq<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>
