<?php

  $site_root = "../";
   $page_title = 'KStars:  New Features in KDE 3.3';
   
  include ( "header.inc" );
?>

<p>
Let's take a brief tour of some of the features we've 
added to KStars for the KDE 3.3 release.  This was a 
relatively short release cycle, so there are not as 
many new features as we had in 
<a href="kstars-3.2-features.php">the 3.2 release</a>.  
Nevertheless, there have still been several 
interesting developments, so come on in and check it 
out.. 
</p>

<h2>Image Acquisition</h2>
<table>
<tr>
<td>
Through the development of the 
<a href="http://indi.sourceforge.net/">INDI</a> 
protocol, Jasem
Mutlaq has already provided KStars users with the 
ability to <a href="indi.php">control their 
telescopes</a>.  With this release, 
he adds image acquisition capabilities, using either 
simple webcams and other Video For Linux Devices, or 
more serious CCD cameras, such as those produced by 
Finger Lakes Instruments.  In addition to simply 
controlling the camera hardware, Jasem has also developed
a FITS Viewer tool, which allows for convenient display 
and editing of the acquired images.  KStars is rapidly 
becoming an attractive option for serious amateur 
astronomers in search of software to support their 
observations.
</td>
<td>
<a href="feature/FITSViewer.png"><img border="0" alt="FITS image viewer" src="feature/FITSViewer_thumb.png" width="300" height="300"/></a>
</td>
</tr>
</table>

<h2>Back To The Future</h2>
<p>
<a href="feature/SetTime.png"><img border="0" align="right" alt="setting a very remote date" src="feature/SetTime_thumb.png" width="331" height="127"/></a>
Thanks to code contributed by Michel Guitel, you can now use 
very remote dates in KStars.  In standard KDE/Qt applications, 
there is a hard-coded limit on the range of allowable dates: 
only years between 1752 and 8000 are possible.  Michel  
developed a complete replacement for the Qt date/time classes 
that does not have this limitation, and we added clones of the 
GUI date/time widgets (such as the date picker) which support 
the new date/time classes.  The upshot is, you can now set 
the date to pretty much anything you like (there's actually 
still an arbitrary restriction to years between -50000 and 
+50000).  However, be warned that the accuracy of the 
positions of solar system bodies will become progressively 
worse as more remote dates are examined.  If you want to 
maintain accuracy on the level of arcseconds, you should not 
stray too many centuries from the present epoch!
</p>

<h2>Sky Display Improvements</h2>
<p>
<a href="feature/CBounds.png"><img border="0" align="right" alt="constellation boundaries" src="feature/CBounds_thumb.png" width="320" height="240"/></a>
The sky display now includes constellation boundaries, 
the imaginary lines defined by the 
<a href="http://www.iau.org/">International Astronomical 
Union</a> which divide the sky into the 
<a href="http://www.astro.wisc.edu/~dolan/constellations/constellation_list.html">88 constellations</a>.  In addition, three important celestial 
guide-lines 
(<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/ai-horizon.html">the 
horizon</a>, 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/ai-cequator.html">the 
celestial equator</a>, and 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/ai-ecliptic.html">the 
ecliptic</a>) are now labeled on the map.
</p>
<p>
Here's a new feature that's pretty slick: if you hover the 
mouse pointer for a few moments in the map, a name label will 
automatically appear next to the object nearest the pointer.
This label is temporary; as soon as the pointer is moved, it
will fade away and disappear.
</p>

<h2>New Tools and Tool Improvements</h2>
<p>
<a href="feature/AngularRuler.png"><img border="0" align="right" alt="angular distance ruler" src="feature/AngularRuler_thumb.png" width="320" height="240"/></a>
Pablo de Vicente has provided us with a very useful new 
&quot;Angular Ruler&quot; tool.  Using the popup menu or the &quot;[&quot; key, 
you define the &quot;starting point&quot; for the measurement.  Then 
move the mouse to the &quot;ending point&quot;, and use the popup menu 
or the &quot;]&quot; key to compute the angular distance between the 
two points.  The computed distance is displayed in the 
statusbar.  While defining the pair of points, a dotted line 
is drawn from the starting point to the mouse pointer, so 
you can easily tell that you are in the Angular Ruler mode.
</p>
<p>
Pablo has also been busy developing his AstroCalculator
tool.  He has added two more modules: one to compute the 
dates of the equinoxes and solstices, and another to compute 
planet coordinates.  In addition, he has completed adding 
&quot;batch modes&quot;" for the calculator modules.  These allow you 
to perform the astrocalculator functions on large sets of 
data in input files, and to retrieve the results in output 
files.
</p>
<p>
KStars now includes a startup wizard with which you can set 
your geographic location right away, and even download extra
data files which are not included in the standard KStars 
distribution.  The download feature uses the new KNewStuff 
mechanism to provide an intuitive GUI and a robust 
installation process.  
</p>
<p>
The Object Details window has gotten some attention for this 
release.  The information displayed for an object now includes 
the object's angular size and its distance from Earth.  Also, 
the object's &quot;airmass&quot; (a measure of how much of the Earth's 
atmosphere light from the object must pass through) is displayed,
along with the object's hour angle (a measure of the angle between
the object and your local meridian).  For the Moon, we also 
display its illumination fraction and a brief description of its 
current phase (e.g., &quot;firs quarter&quot;, &quot;waxing gibbous&quot;, etc.).
</p>
<p>
In the Image Viewer, we added a statusbar that displays a brief
description of the image, as well as an author credit.  If there 
are usage restrictions on the image (e.g., for non-commercial use 
only), they will be displayed here as well.
</p>

<h2>UI Improvements</h2>
<p>
<a href="feature/NightVision.png"><img border="0" align="right" alt="improved Night Vision" src="feature/NightVision_thumb.png" width="320" height="240"/></a>
KStars has always offered a &quot;Night Vision&quot; color scheme consisting
only of red tones, which do not cause your dark-adapted pupils to 
contract.  However, the color scheme did not affect the colors of 
the regular UI widgets (such as the menu, toolbars, and the 
backgrounds of tool windows).  We have now added a feature that will 
apply a dark-red color scheme to the KStars widgets when the Night 
Vision color scheme is selected.  Don't worry, your original 
widget colors will be restored when you choose a different color 
scheme.
</p>
<p>
However, even this addition does not provide a complete dark-vision 
solution, because only the KStars windows are affected, not the rest 
of the KDE desktop.  It would be very rude for KStars to hijack the 
colors of other application windows; instead, we now provide a 
Fullscreen mode, which will cover your entire screen 
with the KStars window.  By combining the Night Vision color scheme
with Fullscreen mode, you get a reasonably complete dark-vision 
friendly environment.
</p>
<p>
It's always been the case that you can see the current RA/Dec
coordinates of the mouse pointer in the statusbar.  With the 3.3 
release, the statusbar now shows the current Az/Alt coordinates
as well.  Actually, you can even configure which are shown, in the 
Settings|Statusbar menu.
</p>

<h2>DCOP and Command-Line Functions</h2>
<p>
There are four new DCOP functions available in this release:
</p>
<dl>
<dt>setColor( QString name, QString value )</dt>
<dd>Sets the color named <i>name</i> to value <i>value</i></dd>
<dt>loadColorScheme( QString schemeName )</dt>
<dd>Load the color scheme named <i>schemeName</i></dd>
<dt>exportImage( QString filename )</dt>
<dd>Saves the current sky map image to file <i>filename</i></dd>
<dt>printImage( bool usePrintDialog, bool useChartColors )</dt>
<dd>Print the current sky map image.  If <i>usePrintDialog</i> is true,
the print dialog will be shown first.  If <i>useChartColors</i> is
true, the &quot;Star Chart&quot; color scheme will be used for printing, to 
conserve ink.</dd>
</dl>

<p>
Finally, for command-line &quot;image dump&quot; mode, there is now an 
additional argument available for specifying the date and time 
for the image.
</p>

<hr />
<p>
<a href="http://edu.kde.org/kstars">Back to the KStars Homepage</a>
</p>

<?php
include "footer.inc";
?>
