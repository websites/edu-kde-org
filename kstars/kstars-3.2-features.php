<?php
  $site_root = "../";
  $page_title = 'KStars:  New Features in KDE 3.2';
  
  include ( "header.inc" );
?>

<p>
This document highlights some of the changes made to KStars since the
KDE 3.1 release.  We've made large strides toward making KStars a
genuinely useful tool for serious amateur astronomers, while still
retaining our simple and intuitive interface that makes KStars easy
to use even for those just getting to know the night sky.
</p>
<small>
Note: we had one unofficial &quot;stable CVS&quot; release since KDE 3.1, so
some of the features listed here won't be new if you have installed that
version.  There's still quite a bit that has changed since then, however,
so you should still take a look!
</small>

<h2>Stars:</h2>
<table border="0" width="100%">
<tr>
<td colspan="2" valign="top">
The number of stars has increased by 300%, to 126,000.
We now use the 
<a href="http://cdsweb.u-strasbg.fr/viz-bin/Cat?I/239">Hipparcos/Tycho 
Star Catalog</a>, and have increased the
number of named stars to over 1500.  At right, you can see the
constellation Taurus as displayed in KStars from KDE 3.1, and below
is the same section of sky from 3.2, with all stars being shown.
(This is not the default view; by default the fainter stars are only
visible when you zoom in.)
<br />
<img alt="spacer" src="feature/trans.png" width="9" height="9"/>
</td>
<td rowspan="2" colspan="2" align="right" valign="top">
<a href="feature/NormalView.png"><img alt="Normal View" style="border: 0" src="feature/NormalView_thumb.png" width="256" height="128"/></a><br />
<a href="feature/FullOfStars.png"><img alt="Full of Stars!" style="border: 0" src="feature/FullOfStars_thumb.png" width="256" height="128"/></a><br />
</td>
</tr>
<tr>
<td valign="bottom" rowspan="2">
<a href="feature/GreekLetters.png"><img alt="Genetive Star names" style="border: 0" src="feature/GreekLetters_thumb.png" width="128" height="128"/></a>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan="2">
In addition, the genetive names of stars are now rendered with actual Greek
Letters instead of their English spelling.
</td>
<td><img alt="spacer" style="border: 0" src="feature/trans.png" width="9" height="9"/></td>
</tr>
</table>

<h2>Solar System:</h2>

<table>
<tr>
<td valign="top">
What's this mess?  KStars is displaying name labels for some of the
2000+ asteroids and comets now included in the simulation.  The name
labels are optional, and you can control how many of these bodies
get name labels by default.
</td>
<td align="right" valign="top">
<a href="feature/MinorPlanets.png"><img alt="Comets and Asteroids" style="border: 0" src="feature/MinorPlanets_thumb.png" width="192" height="128"/></a><br />
</td>
</tr>
</table>

<table>
<tr>
<td align="right" valign="top">
<a href="feature/JupiterMoons.png"><img alt="Jupiter's Moons" style="border: 0" src="feature/JupiterMoons_thumb.png" width="288" height="144"/></a><br />
</td>
<td valign="top">
<a href="http://www.seds.org/nineplanets/nineplanets/jupiter.html">Jupiter</a>'s 
four largest moons 
(<a href="http://www.seds.org/nineplanets/nineplanets/io.html">Io</a>, 
<a href="http://www.seds.org/nineplanets/nineplanets/europa.html">Europa</a>, 
<a href="http://www.seds.org/nineplanets/nineplanets/ganymede.html">Ganymede</a> 
and 
<a href="http://www.seds.org/nineplanets/nineplanets/callisto.html">Callisto</a>) 
are now
included in the simulation, though you have to zoom in on Jupiter to
see them.  Track on Jupiter with an accelerated timestep to watch the
moons orbit around Jupiter.  When 
<a href="http://quest.arc.nasa.gov/galileo/About/galileobio.html">Galileo 
did that 400 years ago</a>, it proved once and for all that the Earth was 
not the center of the Universe!
</td>
</tr>
</table>

<table>
<tr>
<td valign="top">
You may have noticed a line behind Jupiter in the previous screen.
All solar system bodies can now optionally have a &quot;Trail&quot; attached to 
them, tracing their path across the sky.  This lets you examine 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/ai-retrograde.html">retrograde 
loops</a>, as seen for Mars in the Fall of 2003 at right.  These Trails 
are most useful when tracking on a body with a large timestep.  By 
default, solar system bodies automatically get a Trail when they are 
centered in the display.
</td>
<td align="right" valign="top">
<a href="feature/PlanetTrails.png"><img alt="Planet Trails" style="border: 0" src="feature/PlanetTrails_thumb.png" width="240" height="144"/></a><br />
</td>
</tr>
</table>

<h2>Telescope Control:</h2>
<table>
<tr>
<td valign="top">
<a href="feature/INDITrack.png"><img alt="Telescope Control example" style="border: 0" src="feature/INDITrack_thumb.png" width="288" height="192"/></a><br />
</td>
<td valign="top">
One of the most exciting new features is the 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/indi.html">fully-integrated 
telescope control</a> through the <a href="http://indi.sourceforge.net/">INDI 
protocol</a>.  A large number of devices is already supported, including 
most popular computerized telescope mounts.  The present position of your 
telescope is indicated in the map with a FOV symbol.  Simple slew/track 
commands are available through the right-click popup menu, and more 
detailed control is possible with the Telescope Control Panel.  It is 
even possible to 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/indi-remote-control.html">control 
devices remotely</a>, through the internet.  
</td>
</tr>
</table>

<h2>UI Enhancements:</h2>
<table>
<tr>
<td valign="top">
There is now a 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/customize.html">customizable 
"Field-of-View" indicator</a>, which can be used
to show where the display is centered.  You can select from four preset
FOV symbols, or create your own, specifying the symbol's shape, color,
and angular size.  It can even calculate the angular size based on the
parameters of a telescope system.
</td>
<td align="right" valign="top">
<a href="feature/FOVSymbol.png"><img alt="Field-of-View Symbols" style="border: 0" src="feature/FOVSymbol_thumb.png" width="128" height="128"/></a><br />
</td>
</tr>
</table>

<table>
<tr>
<td valign="top">
You can now click-and-drag a rectangle in the sky map which specifies a
field-of-view you'd like to zoom in on; just hold down the Ctrl key.  In
addition, there is now a "Default Zoom" menu/toolbar action, and a tool
for zooming to a specific angular size for the display.
</td>
<td align="right" valign="top">
<a href="feature/ZoomBox.png"><img alt="Zoom box" style="border: 0" src="feature/ZoomBox_thumb.png" width="128" height="128"/></a><br />
</td>
</tr>
</table>

<h2>Tools:</h2>

<p>
KStars now includes a number of Tools that enhance its usefulness 
beyond simple sky-mapping.  In addition, the 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/tool-calculator.html">Astrocalculator</a> 
(which was the only Tool available in 3.1) now includes a number of 
enhancements, including file-based "batch modes" for many of its 
modules.
</p>

<table>
<tr>
<td valign="top">
<a href="feature/AltVsTime.png"><img alt="Altitude v. Time" style="border: 0" src="feature/AltVsTime_thumb.png" width="128" height="128"/></a><br />
</td>
<td valign="top">
Do you need to know if your favorite object will be observable tonight?
The 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/tool-altvstime.html">Altitude 
vs. Time Tool</a> plots the positions of a list of objects
above or below the Horizon, as a function of time.  You specify which 
objects should be plotted.  By default, the curves for the current location
and date in the main window are shown, but these can be changed in the 
&quot;Date &amp; Location&quot; Tab.
</td>
</tr>
</table>

<table>
<tr>
<td valign="top">
<a href="feature/WUT.png"><img alt="What's Up Tonight?" style="border: 0" src="feature/WUT_thumb.png" width="128" height="128"/></a><br />
</td>
<td valign="top">
The Altitude Vs. Time Tool is powerful, but is best for examining a
known list of objects.  Sometimes you may just want to know what is going 
to be above your Horizon on  a specific Date.  The 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/tool-whatsup.html">&quot;What's 
Up Tonight?&quot; Tool</a> provides a summary of such objects, organized by 
object type.  It also presents a short Almanac for the evening, with 
rise and set times for the Sun and Moon.  
</td>
</tr>
</table>

<table>
<tr>
<td valign="top">
Since Jupiter's Moons are now included in the display, we figured
we should offer this Tool as well.  The 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/tool-jmoons.html">Jupiter 
Moons Tool</a> plots the positions of Jupiter's four largest moons, 
relative to Jupiter, as a function of time.  Each moon's path resembles 
a sinusoidal curve, since it is in orbit around Jupiter.  You can use 
the +/- keys to stretch the scale of the time axis, and use the [/] keys 
to translate the range of dates shown.
</td>
<td valign="top">
<a href="feature/JMoonTool.png"><img alt="Jupiter Moons Tool" style="border: 0" src="feature/JMoonTool_thumb.png" width="128" height="128"/></a><br />
</td>
</tr>
</table>

<table>
<tr>
<td valign="top">
We also provide a simple 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/tool-solarsys.html">Solar 
System Viewer</a>, which provides a top-down 
view of the Solar System for the current date on the KStars clock.  The 
view can be zoomed in and out with the +/- keys. 
</td>
<td valign="top">
<a href="feature/SolarSystem.png"><img alt="Solar System Viewer" style="border: 0" src="feature/SolarSystem_thumb.png" width="192" height="192"/></a><br />
</td>
</tr>
</table>

<table>
<tr>
<td valign="top">
<a href="feature/LightCurve.png"><img alt="AAVSO Light Curves" style="border: 0" src="feature/LightCurve_thumb.png" width="192" height="192"/></a><br />
</td>
<td valign="top">
Astronomy is a science in which amateurs can make important contributions. 
A well-known example of this is variable-star monitoring.  The American
Association of Variable Star Observers 
(<a href="http://www.aavso.org">AAVSO</a>) makes systematic 
observations of capricious stars nightly, and publishes the data
on their website.  We have arranged direct access to this database through 
KStars, so you can 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/tool-aavso.html">download 
the very latest lightcurve plots</a> for any of the 
hundreds of variable stars that is monitored by the AAVSO.
</td>
</tr>
</table>

<table>
<tr>
<td valign="top">
<a href="feature/ScriptBuilder.png"><img alt="Script Builder Tool" style="border: 0" src="feature/ScriptBuilder_thumb.png" width="128" height="128"/></a><br />
</td>
<td valign="top">
KStars can be scripted to perform complex behaviors without user 
interaction, through the KDE DCOP mechanism.  This feature existed in the
3.1 version, but while the scripting language is pretty simple, it is still 
a bit like programming, and required manual editing of text files.  KStars 
now includes the 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/tool-scriptbuilder.html">Script 
Builder Tool</a>, which provides a simple point-and-click interface with 
which you can create, modify and test working KStars scripts.  Give it a 
try, it's fun!
</td>
</tr>
</table>


<h2>Other Enhancements:</h2>

<ul>
<li>Startup time reduced by about 50%.  Sky-rendering time cut in half.</li>
<li>Command-line image-dump mode.  This can be used to 
<a href="http://www.30doradus.org/desktop.png">dynamically generate 
a background image for your desktop</a> showing your sky in real-time.</li>
<li>Expanded 
<a href="http://docs.kde.org/en/HEAD/kdeedu/kstars/ai-tools.html#tool-details">Object 
Details window</a>, with access to professional-grade online
databases</li>
<li>High-quality printing.  The printed image is no longer a bitmap, it is
a vector-based PostScript Document.  No more jaggies!</li>
<li>New Menu items: Export Image, Run Script</li>
<li>Expanded list of objects with external Image links in their popup 
menu</li>
<li>Many, many usability enhancements</li>
</ul>

<p>
We hope you enjoy KStars!
</p>
<p>
The KStars Team:<br />
<small>
&nbsp;&nbsp;&nbsp;Jason Harris<br />
&nbsp;&nbsp;&nbsp;Heiko Evermann<br />
&nbsp;&nbsp;&nbsp;Thomas Kabelmann<br />
&nbsp;&nbsp;&nbsp;Pablo de Vicente<br />
&nbsp;&nbsp;&nbsp;Jasem Mutlaq<br />
&nbsp;&nbsp;&nbsp;Mark Hollomon<br />
&nbsp;&nbsp;&nbsp;Carsten Niehaus<br />
</small>
</p>

<hr />
<p>
<a href="http://edu.kde.org/kstars">Back to the KStars Homepage</a>
</p>

<?php
  include "footer.inc";
?>
