Script:  Lunar Phases
Author:  Jason Harris
Date:    17 Sept. 2004

Description:
Tracks the Moon through one full cycle of its phases, beginning with 
the New Moon phase on 14 Sept 2004.  Note at the beginning that the 
New Moon appears very close to the Sun in the sky, as it must (since 
the phase is due to the illumination of the Moon by the Sun).  As
time passes, the Moon moves eastward, away from the Sun, gradually
revealing its illuminated half.  Eventually, when it is opposite the 
Sun in the sky, it is fully illuminated, and then continues its 
Eastward track back towad the Sun, and back toward the New phase.

