Script:  Seasons at Noon
Author:  Jason Harris
Date:    17 Sept. 2004

Description:
Over the course of a year, the Sun appears to travel across the sky 
along a line called the Ecliptic.  As it does this, its maximum 
altitude angle in the sky (which occurs at Noon) varies, producing 
the seasons: the Sun is high in the sky in Summer, and low in the sky 
in Winter.

This script illustrates the annual migration of the Sun.  The display 
is pointed South, and the clock is set to local Noon.  Each timestep
spans exactly three days, so every frame will be at Noon, three days 
after the previous frame.  We start at the 17th of September, which 
is near the Autumnal equinox (the equinoxes occur when the Sun passes
the Celestial Equator, which is drawn on the sky as a white curve by 
default).

Note that when the Sun reaches its lowest noontime position, it will 
be the beginning of Winter (lete December for northern hemisphere 
locations), and it reaches its highest noontime position at the
beginning of Summer (late June for northern hemisphere locations). 
These minimum and maximum positions are known as the Winter and Summer 
Solstices.
