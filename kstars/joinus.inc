<h3><a name="join">Join Us!</a></h3>

<p>
Interested in KStars? Found KStars useful? Come by our <a href='https://mail.kde.org/mailman/listinfo/kstars-devel'>mailing list</a> and share your suggestions, and tell us what you liked / what you did not like. Any feedback and help will be appreciated.
</p>
<p>If you know how to program, you're welcome to join our developer team! Get started by building <a href='https://invent.kde.org/education/kstars/-/blob/master/README.md'>KStars from source<a/>. Most developer discussion happens on our <a href='https://mail.kde.org/mailman/listinfo/kstars-devel'>mailing
list</a> or <a href="https://invent.kde.org/education/kstars/-/issues">issue tracker</a>, so if you need help, that's a good place to ask. KStars is
written in C++ using <a href="https://qt-project.org/">Qt</a> and <a href="http://www.kde.org">KDE</a> libraries. 
</p>
<p>
If you are an astronomy enthusiast, there is our <a href="astroinfo.html">AstroInfo</a> project. We could also use help writing documentation and translating. Even bug reports and feature requests are a big help, so keep them coming!
</p>
<p>

<p>
Some pointers to get started:
<ul>
<li>Join our <a href='https://mail.kde.org/mailman/listinfo/kstars-devel'>mailing list</a></li>
<li>Hang out on our <a href="https://webchat.kde.org/#/room/#kstars:kde.org">KStars Web Chat</a> channel.</li>
<li>Details on building KStars git master are <a href='https://invent.kde.org/education/kstars/-/blob/master/README.md'>here</a></li>
<li>Some junior and not-so-junior jobs <a href='http://techbase.kde.org/Projects/Edu/KStars/JuniorJobs'>here!</a></li>
</ul>
</p>
