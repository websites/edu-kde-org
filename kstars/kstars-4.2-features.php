<?php

  $site_root = "../";
   $page_title = 'KStars:  New Features in KDE 4.2';
   
  include ( "header.inc" );
?>

<p>
The KStars team joins in the celebration of the International Year of
Astronomy with the release of version 1.5.0 along with KDE 4.2, seeing
a whole host of new features.

We hope that KStars will help achieve the mission of IYA 2009 - to
take astronomy to the masses, at all levels. Hope you enjoy the
features!
</p>

<h2>100 million stars - Keep zooming in!</h2>
<table>
<tr>
<td valign="top">
<p>
Are you one of those amateurs with a humongous telescope or do you own
an observatory that you'd want to use KStars with? KStars can now load
100 million stars without consuming any more memory than before!
</p>
<p>
We now provide the brightest 100 million stars from the USNO NOMAD
Catalog, going down to mag 16 as an optional download. To download
this, select 'Download New Data' from the File Menu and choose to
install 'USNO NOMAD Catalog (Part)'. You could adjust the star density
by using a simple slider in the Configuration dialog. Happy observing!
</p>
</td>
<td valign="top">
<a href="feature/USNO-NOMAD-1e8.png"><img border="0" alt="KStars with the 100 million star catalog" src="feature/USNO-NOMAD-1e8_thumb.png" width="301"/></a>
</td>
</tr>
</table>
<br />

<h2>LMC and SMC</h2>
<table>
<tr>
<td valign="top">
<p>
KStars now displays Milky Way-like patches for the Large and Small Magellanic
Clouds. This makes that confusing bundle of DSOs that one would see
scattered on the LMC more meaningful.
</p>
</td>
<td valign="top">
<a href="feature/LMC-SMC.png"><img border="0" alt="Milky Way-like patches for LMC and SMC" src="feature/LMC-SMC_thumb.png" width="301" /></a>
</td>
</tr>
</table>
<br />

<h2>More realistic stars!</h2>
<table>
<tr>
<td valign="top">
<p>
Stars drawn by KStars now look more realistic, and colors are softer than before as they are in reality.
</p>
</td>
</tr>
</table>
<br />

<h2>Support for non-western Sky Cultures</h2>
<table>
<tr>
<td valign="top">
<p>
Support for non-western Sky cultures is now available in KStars. To change the sky culture, select Configure KStars from the Settings Menu and choose from the drop down list on the guides tab of the dialog that opens. Many thanks to the Stellarium folks for the data!
</p>
</td>
<td valign="top">
<a href="feature/non-western-sky-culture.png"><img border="0" alt="Non western Sky Culture support" src="feature/non-western-sky-culture_thumb.png" width="301" /></a>
</td>
</tr>
</table>
<br />

<h2>Compute conjunctions of a planet with any object!</h2>
<table>
<tr>
<td valign="top">
<p>
In KDE 4.1, the conjunctions tool forced that both objects between
which conjunctions were being computed be Planets. In KDE 4.2, while
one of them still needs to be a planet, the other object could be any
object in KStars.
</p>
<p>
This extends the purpose of the conjunctions tool. You could now track events like comet rendezvous with a particular object, or photogenic events like Mars passing through the Beehive Cluster.
</p>
</td>
<td valign="top">
<a href="feature/deep-sky-conjunctions.png"><img border="0" alt="Predict conjunctions of planets with deep-sky objects" src="feature/deep-sky-conjunctions_thumb.png" width="301" /></a>
</td>
</tr>
</table>
<br />

<h2>Sky Calendar</h2>
<table>
<tr>
<td valign="top">
<p>
This is a tool that plots the time of various interesting events (eg: Venus rising) against the days of the year.
</p>
<p>
Unfortunately, it was too late before we could add labels to the curves, so this feature is incomplete. It should be complete in the next release. Till then, it could be used with a legend for the colors.
</p>
</td>
<td valign="top">
<a href="feature/sky-calendar-incomplete.png"><img border="0" alt="The (unfinished) Sky Calendar tool" src="feature/sky-calendar-incomplete_thumb.png" width="301" /></a>
</td>
</tr>
</table>
<br />

<h2>Other Improvements</h2>
<table>
<tr>
<td valign="top">
<p>
A whole bunch of localization issues have been fixed for KDE 4.2 release. Constellations names are now localized in the popup menu and observing list wizard, for instance. Info and Image link titles in the details dialog are localized as well. 
</p>
</td>
<td valign="top">
<a href="feature/localization-fixes-4.2.png"><img border="0" alt="The (unfinished) Sky Calendar tool" src="feature/localization-fixes-4.2_thumb.png" /></a>
</td>
</tr>
</table>
<br />

<table>
<tr>
<td valign="top">
<p>
INDI has been separated from KStars and KStars can be optionally compiled against libindi >= 0.6 in order to add support for astronomical instruments.
</p>
</td>
</tr>
</table>
<br />

<table>
<tr>
<td>
<p>
KStars now shows the catalog numbers for stars from the Henry Draper catalog. Right clicking on a star that belongs to the HD catalog will display a popup menu that shows the HD catalog number.
</p>
<p>
With the optional Henry Draper index addon, KStars can also search by HD number. To do this, install the addon by selecting File -> Download New Data -> Henry Draper Index. Once the addon is installed, you could search for a HD star by firing up the Find Dialog (Ctrl+F) and typing "HD" (no quotes) followed by the catalog number (Eg: HD123455). Nothing will appear in the list below, but on hitting OK, KStars will slew to the HD star.
</p>
</td>
<td valign="top">
<a href="feature/hd-catalog.png"><img border="0" alt="The (unfinished) Sky Calendar tool" src="feature/hd-catalog_thumb.png" /></a>
</td>
</tr>
</table>
<br />

<hr />
<a href="http://edu.kde.org/kstars">Back to the KStars Homepage</a>


<?php
include "footer.inc";
?>
