<?php
  $site_root = "../..";
  $page_title = 'KStars SVN Snapshots';
  
  include ( "header.inc" );
?>

<p>
We make periodic snapshot releases of KStars, in case you 
do not want to get the SVN sources 
<a href="svn.php">in the usual way</a>.
</p>

<p>
Please note: These are unstable releases, fresh from SVN.
We have not done our usual careful vetting against bugs
for these releases.  That said, we usually keep SVN in good 
order, and by running recent SVN code, you can help us find 
and resolve issues.  So have fun!
</p>

<p>
You can verify the integrity of the file with the associated 
GPG signature, or MD5 sum.  Just use 
<code>gpg --verify &lt;filename&gt;.sig &lt;filename&gt;</code> 
for the GPG signature.  For the MD5 sum, type 
<code>md5sum &lt;filename&gt;</code>, and compare the result
to the value in the <code>&lt;filename&gt;.md5</code> file.
</p>

<center>
<p>
--=&gt; <a href="http://www.30doradus.org/kstars/">SVN snapshots</a> &lt;=--
</p>
</center>

<hr />
<p>Jason Harris<br />
<a href="mailto:kstars AT 30doradus DOT org">kstars At 30doradus DOT org</a>
</p>

<?php
  include "footer.inc";
?>

