<?php
  $site_root = "../";
  $page_title = 'Google Summer of Code';
  
  include ( "header.inc" );
?>

<h3>Welcome prospective students!</h3>
<p>KStars welcomes all prospective students planning on participating in <a href="https://developers.google.com/open-source/gsoc/">GSoC 2022</a>.</p>

<p>By participating in Open Source projects such as KStars, you shall gain a considerable experience not only in terms of real world development practices in the software industry, but also improve your skills in planning, management, and communication. There are usually three/four slots available for KStars, so the competition to win is stiff! After a student is selected, he/she shall implement the selected project over the course of roughly three months with mid-term evaluations submitted to Google on the progress of the project's objectives.</p>

<p>You are expected to fully dedicate the GSoC 2022 summer period to the development of KStars. Specifically, the following is <b>NOT</b> accepted during the program duration:</p>
<ul>
<li>Full/Part time job.</li>
<li>University summer course.</li>
<li>Internship.</li>
<li>Travel/Vacation.</li>
</ul>
<p>GSoC 2022 should be treated as a <i>full time</i> job by the prospective student for the duration of the program. If you have any college examinations during the GSoC 2022 period they must be clearly indicated in the proposal.</p>

<p>Before any student is selected for any KStars GSoC project, they must <b>demonstrate</b> their ability to work in KStars/KDE/Qt environment as discussed in the next section.</p>

<h3>Where do I start?</h3>

<p>To develop for KStars, you are expected to be well versed in the following areas:</p>
<ol>
<li>C/C++</li>
<li>Algorithms, data structures, and design patterns.</li>
<li>Qt/QML</li>
<li>KDE</li>
</ol>

<p>Since you selected KStars for your project, it is safe to assume you are interested in astronomy; Therefore, a background in Astronomy also helps in the development process. To prepare yourself for GSoC, perform the following steps:
<ol>
<li>Read <a href="https://community.kde.org/GSoC/2022/Ideas#KStars">KStars GSoC ideas</a>: The KStars team compiled a few ideas for GSoC students. You need to carefully read all ideas and select which one best suit your interests and abilities. If none match your interest, you can propose a new idea pending the approval of KStars mentor.</li>
<li>Prepare your computer environment for KStars development:</li>
<ul>
<li>Download and install a modern Linux distribution: While you can build KStars on any Linux distribution, it is recommended to install a modern KDE-based distribution such as <a href="http://www.kubuntu.org/getkubuntu">Kubuntu</a> 20.04 or later.</li>
<li>Download <a href="http://www.qt.io/download-open-source/">Qt Creator</a>: Ensure to install all examples and documentation. <a href="http://doc.qt.io/qt-5/qtexamplesandtutorials.html">Several examples</a> are available online as well. Start with simple <i>Hello World</i> application to get familiar with Qt, and proceed with building GUI applications based on QWidget and QML.</li>
<li><a href="https://invent.kde.org/education/kstars#development">Build GIT version of KStars.</a> You should be able to open KStars project in Qt Creator. KDE applications using CMake build system. Go to File --> Open Project and select CMakelists.txt</li>
</ul>

<li>Familiarize yourself with KStars: The best way to learn about KStars is to dig the code and fix/improve things! Start with simple ideas or bug fixes:</li>
<ul>
<li>Study <a href="https://api.kde.org/extragear-api/edu-apidocs/kstars/html/index.html">KStars API</a>: KStars may be a complex piece of software but it's not magic, learn how it internally works.</li>
<li>Review the <a href="https://docs.kde.org/stable/en/kdeedu/kstars/">KStars handbook</a>: While the handbook is meant for end users, it's good to familiarize yourself with how end users utilize KStars.</li>
<li>Hunt <a href="https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&f0=OP&f1=OP&f2=product&f3=component&f4=alias&f5=short_desc&f6=status_whiteboard&f7=content&f8=CP&f9=CP&j1=OR&o2=substring&o3=substring&o4=substring&o5=substring&o6=substring&o7=matches&product=kstars&query_format=advanced&v2=kstars&v3=kstars&v4=kstars&v5=kstars&v6=kstars&v7=%22kstars%22">pending KStars bugs</a>: Try to fix pending KStars bugs items, send a patch to the maintainer and/or propose possible fixes!</li>
<li>Search <a href="https://invent.kde.org/education/kstars/-/issues?label_name%5B%5D=Junior+Job">pending KStars issues</a>: Check the list of issues in KStars. These can be bugs, or tasks, or new features to be implemented. Pick one issue and coordinate with the maintainer to work on it.</li>
<li>Check out INDI forum <a href="https://indilib.org/forum/wish-list.html">Wishlist</a> items. Some are applicable to KStars/Ekos and could serve as an entry point to KStars and Ekos development.</li>
</ul>
</ol>

<p>Students are <b>required</b> to either implement one of junior jobs and/or resolve bugs to prove eligibility to participate in KStars. You need to be actively involved in KStars before the GSoC official proposal submission date and evaluation.</p>

<h3>Communication</h3>

<p>During the course of the GSoC project, you are expected to keep your mentor and the KStars developers informed throughout the project cycle:</p>
<ol>
<li>First and foremost is KStars Developement <a href='https://mail.kde.org/mailman/listinfo/kstars-devel'>mailing list</a>: Use the mailing list for everything from inquiry, non-trivial code commits, and progress reports.</li>
<li>Developers sometimes hang out at <a href="https://webchat.kde.org/#/room/#kstars:kde.org">KStars web chat channel</a>. You can discuss a variety of topics with them subject to availability.</li>
</ol>
<p>While students are strongly encouraged to ask any development related questions via any communication channel above, you are <b>expected</b> to do your research before you ask. <i>Repetitively</i> asking trivial questions to mentors shows you have not performed your research well into the issue or lack basic problem-solving abilities. Therefore, research your issues then ask specific direct questions to the mailing list or mentors.</p>


<?php
  include "footer.inc";
?>
