<?php
  $site_root = "../";
  $page_title = "KVerbos";
  

  include( "header.inc" );

  $appinfo = new AppInfo( "KVerbos" );
  $appinfo->setIcon( "../images/icons/kverbos_32.png", "32", "32" );
  $appinfo->setVersion( "1.05.13" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2001", "the $title-Team" );
  $appinfo->addAuthor( "Arnold Kraschinksi", "arnold.kra67 AT gmx DOT de" );
  $appinfo->addContributor( "David Vignoni", "david80v AT tin DOT it", "svg icon" );
  $appinfo->show();
?>
<br />
<div id="quicklinks"> [
<a href="#kverbos2">KVerbos 2</a> |
<a href="#description">Description</a> |
<a href="#obtain">Obtain KVerbos 2</a> |
<a href="#oldkverbos">KVerbos 1</a> |
<a href="#descriptionold">Description</a> |
<a href="#obtainold">Obtain KVerbos</a> |
  <a href="#news">News</a>
  ]
</div>

<h2><a name="kverbos2">KVerbos 2 - Is coming into sight</a></h2>
<h3><a name="descriptio">Description</a></h3>
<p>With KVerbos you can practice the Spanish verb conjugation. The program comes with a
large set of Spanish verbs. You can select from a list of over 9 000 verbs the ones you want to 
train and you can select the tenses, too.</p>
<p>KVerbos isn't the result of language science. This means that there are verb forms
that aren't in the database but there a enough verbs so that even advanced learners will
find theirs.</p>
<p>The feedback is now integrated into the program. You will see how much exercises you have
solved and what were your results. You can select a picture and this will be presented to you
as a jigsaw which will be completed while you enter correct answers. A clock shows you how
long you are training or it is use as a timer and shows how much time is left.</p>
<p>Although there are things to do the program in this stage is alredy more interesting than
the old version.</p>

<h3><a name="obtain">How to obtain KVerbos 2</a></h3>
        <p>A webpage is available at <a href="http://www.mzgz.de/kverbos/ekverbos.htm">
        www.mzgz.de/kverbos/ekverbos.htm</a></p>

<p>At the moment KVerbos 2 is still in kdeplayground so you have to fetch the source code from there.
Or you go to the KVerbos web page an download the program.
</p>

<h2><a name="oldkverbos">KVerbos 1 - The old version</a></h2>
<h3><a name="descriptionold">Description</a></h3>

<p>Kverbos is an application specially designed to study Spanish verb forms.
</p>
<p>The program suggests a verb and a tense and the user enters the different
forms. The program corrects the user input and gives a feedback. <br />
The user can edit the list of the verbs that can be studied. The program can build
regular verb forms and the forms of the most important verb groups by itself. Irregular
verb forms have to entered by the
user.</p>

<h3><a name="obtainold">How to obtain KVerbos</a></h3>
        <p>A webpage is available at <a href="http://www.mzgz.de/kverbos/ekverbos.htm">
        www.mzgz.de/kverbos/ekverbos.htm</a></p>

<p>For the moment, please download the program from the webpage with the 2 files containing the verbs.<br />
To compile and install the tarball, please type the following in a console (it installs by default in
/usr/local/kde and in that case, the system cannot find the pictures so
<u><b>please don't forget the configure option</b></u>): <br /><br />
<font face="Courier New,Courier">
./configure --prefix=your_kde_dir<br />
make<br />
su -c "make install"<br />
</font>
</p>

<?php
  kde_general_news("./news.rdf", 10, true);

  include( "footer.inc" );
?>








