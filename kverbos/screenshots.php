<?php
  $page_title = "KVerbos - Screenshots";
  $site_root = "../";
  

  include( "header.inc" );

  $gallery = new EduGallery( "KVerbos Screenshots" );
  $gallery->addImage( "pics/startscreen_thumb.png", "pics/startscreen.png", 240, 179, "KVerbos 2 - The user identification", "", "KVerbos 2 - The user identification" );
  $gallery->addImage( "pics/themenu_thumb.png", "pics/themenu.png", 240, 179, "KVerbos 2 - Seclect what you want to do", "", "KVerbos 2 - Seclect what you want to do" );
  $gallery->startNewRow();
  $gallery->addImage( "pics/learning1_thumb.png", "pics/learning1.png", 243, 179, "KVerbos 2 - Enter the forms", "", "KVerbos 2 - Enter the forms" );
  $gallery->addImage( "pics/learning2_thumb.png", "pics/learning2.png", 240, 179, "KVerbos 2 - Look up a conjugation", "", "KVerbos 2 - KVerbos 2 - Look up a conjugation" );
  $gallery->startNewRow();
  $gallery->addImage( "pics/result_thumb.png", "pics/result.png", 240, 179, "KVerbos 2 - The result", "", "KVerbos 2 - The result" );
  $gallery->addImage( "pics/puzzlesolved_thumb.png", "pics/puzzlesolved.png", 243, 192, "KVerbos 2 - You did well.", "", "KVerbos 2 - You did well." );
  $gallery->startNewRow();
  $gallery->addImage( "pics/verbosoptionsselectgrammar_thumb.png", "pics/verbosoptionsselectgrammar.png", 243, 179, "KVerbos 2 - Select your verbs from over 9 000.", "", "KVerbos 2 - Select your verbs from over 9 000." );
  $gallery->startNewRow();

  $gallery->addImage( "pics/kverbos1_thumb.png", "pics/kverbos1.png", 243, 232, "KVerbos 1 - A verb and the tense", "", "KVerbos 1 - A verb and the tense" );
  $gallery->addImage( "pics/kverbos2_thumb.jpg", "pics/kverbos2.jpg", 243, 213, "KVerbos 1 - An irregular verb", "", "KVerbos 1 - An irregular verb" );
  $gallery->startNewRow();
  $gallery->addImage( "pics/kverbos3_thumb.jpg", "pics/kverbos3.jpg", 243, 207, "KVerbos 1 - The verbs editor", "", "KVerbos 1 - The verbs editor" );
  $gallery->show();

  include("footer.inc");
?>

