<?php
  $site_root = "../";
  $page_title = 'Artikulate';

  include ( "header.inc" );
?>

<?php
  include("artikulate.inc");
  $appinfo->showIconAndCopyright();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a>
]
</div>

<center>
<a href="pics/artikulate_screenshot.png"><img border="0" width="600" height="446" src="pics/artikulate_screenshot.png"></a>
 </center>

<h3><a name="description">Description</a></h3>

<p>Artikulate is a pronunciation trainer that helps improving and perfecting a learner's pronunciation skills for a foreign language.
It provides courses with native speaker recordings for several training languages.
The learner downloads those courses, selects a category of phrases to train, then starts with recording her/his own voice when speaking the phrases and comparing the results to the native speaker's recordings by listening to both. By adjusting and repeating the own pronunciation, the learner can improve his/her skill.</p>

<br />
<br />

<hr width="30%" align="center" />
<p>
Author: Andreas Cord-Landwehr<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
