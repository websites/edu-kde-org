<?php
require_once __DIR__.'/../vendor/autoload.php';

// Collect blog posts for a KDE-Edu News Feed
$feedu = new SimplePie();

// add urls to rss-feeds from blogs here
$feedu->set_feed_url( array(
  "http://jpwhiting.blogspot.com/feeds/posts/default?alt=rss/-/kde-edu",
  "http://annma.blogspot.com/feeds/posts/default?alt=rss",
  "http://www.proli.net/category/kde-edu/feed",
  "http://en.wordpress.com/tag/kdedu/feed/"
  ) );
  
// we don't support cache
$feedu->enable_cache( false );

// Stripping html tags in rss feeds makes the feed valid.
// The tags will not be stripped but encoded
$feedu->strip_htmltags(array());
$feedu->strip_attributes(array());
$feedu->encode_instead_of_strip(false);
//$feedu->strip_htmltags(array('p', 'a', 'img', 'span', 'br', 'div', 'img', 'blockquote', 'em', 'code'));

// parse the feeds
$feedu->init();

// set suitable content header
$feedu->handle_content_type();
?>
