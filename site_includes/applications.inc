<?php
// As header.inc haven't set the magic variables yet we do this:
$doc_root = $_SERVER["DOCUMENT_ROOT"];

//
// This class contains all 3 json index files.
// It serves a lot of tiny function for easy use.
//
class applicationIndex {
  var $index = array();
  var $age_index = array();
  var $subject_index = array();
  var $wkoIconsUrl = "http://edu.kde.org/images/icons/";
  
  // The constructor parses the json files.  
  function applicationIndex() {
    global $doc_root;
    $this->index = json_decode(file_get_contents("$doc_root/applications/index.json"), true);
    $this->age_index = json_decode(file_get_contents("$doc_root/applications/age.json"), true);
    $this->subject_index = json_decode(file_get_contents("$doc_root/applications/subject.json"), true);
  }
  
  function getAllApplications() {
    $sortedArray = $this->index[ "All" ];
    sort($sortedArray);
    return $sortedArray;
  }
  
  // must not be sorted but preserve the order by age  
  function getAgeCategories() {
    return array_keys( $this->age_index );
  }
  
  function getSubjectCategories() {
    $subjects = array_keys( $this->subject_index );
    sort( $subjects );
    return $subjects;
  }
  
  function getSubcategories( $cat = false ) {
    if( !$cat || $cat == "all" ) $list = array( "age", "subject" );
    elseif( $cat == "age" ) $list = $this->getAgeCategories();
    elseif( $cat == "subject" ) $list = $this->getSubjectCategories();
    return $list;
  }
  
  function getParentCategory( $cat ) {
    if( in_array( $cat, $this->getAgeCategories() ) ) return "age";
    elseif( in_array( $cat, $this->getSubjectCategories() ) ) return "subject";
  }
  
  function getApplications( $cat = false ) {
    $list = null;
    if( !$cat || $cat == "all" ) $list =  $this->getAllApplications();
    elseif( in_array( $cat, $this->getAgeCategories() ) ) $list = $this->age_index[ $cat ];
    elseif( in_array( $cat, $this->getSubjectCategories() ) ) $list = $this->subject_index[ $cat ];

    $sorted = $list;
    sort($sorted);
    return $sorted;
  }

  //
  // Information about an application
  //
  function getFileName( $application ) {
    return $application[0];
  }

  function getName( $application ) {
    return $application[1];
  }
  
  function getDescription( $application ) {
    return i18n_var( $application[2] );
  }
  
  function getIcon( $application, $size = 48 ) {
    return $this->wkoIconsUrl . strtolower( $this->getFileName( $application ) ) . "_$size.png";
  }

  //
  // Information about a category
  //
  function getCategoryIcon( $cat, $size = 48 ) {
    return "../../images/icons/categories/education-" . strtolower( $cat ) . "_$size.png";
  }
  
  //
  // Following functions produce the html output
  //
  function printCategoryBox( $cat ) {
    echo "<p class=\"app-category\">\n";
    echo "\t<a href=\"/applications/".strtolower($cat)."/\" >\n";
    echo "\t\t<img src=\"".$this->getCategoryIcon( $cat )."\" alt=\"$cat\" title=\"$cat\"/>\n<span>";
    echo i18n_var( $cat )."\t</span></a>\n";
    echo "</p>\n";  
  }
  
  function printApplicationBox( $application, $cat = "all" ) {
    echo "<p class=\"app-category\">";
    echo "\t<a href=\"/applications/" . strtolower( $cat ) . "/" . $this->getFileName( $application ) . "/\">";
    echo "\t\t<img src=\"" . $this->getIcon( $application ) . "\" alt=\"" . $this->getName( $application ) . "\" title=\"" . $this->getName( $application ) . "\" /><span>" . $this->getName( $application ) . "</span></a><br />\n";
    echo "\t <span>" . $this->getDescription( $application ) . "</span>";
    echo "</p>\n";
  }
}

// A bit longish named instance of the class
$applicationIndex = new applicationIndex();


// Get information about where we are
function pageCategory( &$cat, &$pretty_cat, &$is_subcategory ) {
  $pretty_cats = array (
    "all" => i18n_var( "All" ),
    "age" => i18n_var( "Age" ),
    "subject" => i18n_var( "Subject" ),
    "preschool" => i18n_var( "Preschool" ),
    "school" => i18n_var( "School" ),
    "university" => i18n_var( "University" ),
    "language" => i18n_var( "Language" ),
    "mathematics" => i18n_var( "Mathematics" ),
    "miscellaneous" => i18n_var( "Miscellaneous" ),
    "science" => i18n_var( "Science" )
  );

  $cat = $_REQUEST[ "category" ];
  $pretty_cat = $pretty_cats[ $cat ];
  if( in_array( $cat, array( "all", "age", "subject" ) ) ) $is_subcategory = false;
  else $is_subcategory = true;
}

?>
