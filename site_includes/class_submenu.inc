<?php

include_once( "functions.inc" );

class subMenu {
  var $menu;

  function subMenu() {
    if( file_exists( "menu.json" ) )
      $this->menu = json_decode(file_get_contents("menu.json"), true);
    else 
      $menu = false;
  }
  
  function isCurrent( $page ) {
    return ( strpos( $_SERVER[ "PHP_SELF" ], $page ) !== false ); 
  }
  
  function isExternal( $page ) {
    return substr( $page, 4 ) == "http";
  }
  
  function showEntry( $title, $page ) {
    if ( $this->isCurrent( $page ) )
      echo "<strong>$title</strong><br />\n";
    else
      echo "<a href=\"$page\">$title</a><br />\n";
  }
  
  function show() {
    if ( $this->menu === false ) return;
    echo "<div id=\"sidebar\">\n";
    echo "<strong>".i18n_var( "Contents" )."</strong><br /><br />\n";
    foreach( $this->menu as $title => $page ) {
      $this->showEntry( $title, $page );
    }
    echo "</div><!-- sidebar -->";
  }
}

?>
