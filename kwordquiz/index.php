<?php
  $site_root = "../";
  $page_title = "KWordQuiz";
  
  include( "header.inc" );

  $appinfo = new AppInfo( "KWordQuiz" );
  $appinfo->setIcon( "../images/icons/kwordquiz_32.png", "32", "32" );
  $appinfo->setVersion( "0.8.9" );
  $appinfo->setLicense("gpl");
  $appinfo->setCopyright( "2003", "Peter Hedlund" );
  $appinfo->addAuthor( "Peter Hedlund", "peter AT peterandlinda DOT com" );
  $appinfo->show();
?>

<br />
<p>KWordQuiz is a general purpose flash card program. It can be used for vocabulary learning and many other subjects. </p>

<p>KWordQuiz is the KDE version of the Windows program WordQuiz. If you have just switched to KDE/Linux you can use all files created in WordQuiz with KWordQuiz. Additional information about KWordQuiz is available at <a href="http://peterandlinda.com/kwordquiz/">the author's own website</a>.</p>

<p>KWordQuiz uses the kvtml file format and you can download <a href="http://edu.kde.org/contrib/kvtml.php">contributed</a> files.</p>

<p>Thanks to <b>Linux Magazine</b>, the KWordQuiz article on
<a href="http://www.linux-magazine.com/issue/41">http://www.linux-magazine.com/issue/41 [KTools - KWordQuiz - Make vocabulary training fun]</a> is now available online. <br />
The article is available as a PDF and can be downloaded and printed for private use without charge.
</p>

<center>
<?php
  $gallery = new EduGallery("KWordQuiz - Screenshot");
  $gallery->addImage("pics/kwordquiz1_sm.png", "pics/kwordquiz1.png", 330, 301,  "[Screenshot]", "", "KWordQuiz");
  $gallery->show();
  ?>
 </center>
 
<?php
  kde_general_news("./news.rdf", 10, true);
 ?>
 

<?php
  include("footer.inc");
?>

