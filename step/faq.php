<?php
  $site_root = "../";
  $page_title = "Step FAQ";
  

  include( "header.inc" );
?>

<br />
<h3>Frequently Asked Questions</h3>
<p>This section is meant to answer frequently asked questions about Steps. If you still have any questions or think a question is missing
  here, please let me know.</p>

<?php
  $faq = new FAQ();

  $faq->addQuestion( "Why Step is 2d-only? Will 3d version be created?",
      "<p>Two dimension was chosen instead of three because it is quite easier to imagine, edit and visualize on a computer screen so the user can concentrate on physics itself (still not sure - then just compare the complexity of Inkscape and Blender). In physics many things in 2d are just the same as in 3d, many others differ only slightly.</p>
<p>A three-dimensional version of Step is possible but not planned for the near future because of the lack of developers (by the way, you can always help!).</p>" );

  $faq->addQuestion( "Is Step ready to be used? When is will be released?",
      "<p>Currently 80% of initial feature set is implemented (check <a href=\"http://edu.kde.org/step/#features\">Step Features</a>) but some features still need more polishing. Step will be released with KDE 4.1 as part of KDEEDU.</p>" );

  $faq->addQuestion( "What new features are planned? Where is the TODO list?",
      "<p>A TODO list can be found <a href=\"http://websvn.kde.org/trunk/KDE/kdeedu/step/TODO?view=markup\">here</p>" );

  $faq->addQuestion( "Why was StepCore written? What's wrong with existing libraries?",
      "<p>There are several open-source (and proprietary) physical simulation libraries (most known are ODE and Bullet). They are really good for things they are designed for, but not suitable for a program such as Step because:</p>
<ul><li>Most of them are designed for games, this means quick, stable, realistic but inaccurate simulation. Step is an educational program, it is intended to show how things are in real world, so accuracy is really important.</li>
<li>They do not provide error estimation (since it requires additional resources and is useless for games), but this is important for Step. If Step can't show how things are in real world it should at least warn the user.</li>
<li>Most of them focus mainly on rigid body simulation, the scope of Step is wider.</li>
<li>Most of them can handle only three-dimensional simulations. Using them for two-dimensional simulations is wasting computer resources.</li>
<li>Most of them are not easily extensible, complex bodies such as massive springs are hard to implement with them.</li>
<li>The other end is scientific simulation libraries, but they are too big and complex to be used with Step.</li></ul>");

  $faq->addQuestion( "What are StepCore design goals?",
      "<ul><li>Accuracy and error estimations.</li>
       <li>Support for both 2d and (in the future) 3d simulations.</li>
       <li>Extensibility by adding new solver types, body types, forces.</li>
       <li>Extensibility by adding new simulation types which can possibly interact with each other (for example liquid simulation using Boltzmann equation which can interract with mechanical bodies).</li>
       </ul>" );

  $faq->show();
?>

<?php
  include( "footer.inc" );
?>
