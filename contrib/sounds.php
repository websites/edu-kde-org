<?php
  $site_root = "../";
  $page_title = 'Contributed Sound Files';
  
  include ( "header.inc" );
?>

  <p>Ludovic Grossard has created a set of sounds that are freely available for anyone to use. They
  are:</p>

  <h2>Generic Sounds</h2>

  <p>There are some general purpose sounds, suitable for all sorts of things:</p>

  <table width="75%" style="margin-left:auto;margin-right:auto">
    <colgroup width="1*" span="4"></colgroup>
    <tr>
      <th colspan="4">MP3 format</th>
    </tr>

    <tr>
      <td><a href="sounds/grossard/splash/splash2.mp3">splash2.mp3</a></td>

      <td><a href="sounds/grossard/splash/splash3.mp3">splash3.mp3</a></td>

      <td><a href="sounds/grossard/splash/splash4.mp3">splash4.mp3</a></td>

      <td><a href="sounds/grossard/splash/splash5.mp3">splash5.mp3</a></td>
    </tr>

    <tr>
      <td><a href="sounds/grossard/splash/splash6.mp3">splash6.mp3</a></td>

      <td><a href="sounds/grossard/splash/splash7.mp3">splash7.mp3</a></td>

      <td><a href="sounds/grossard/splash/splash8.mp3">splash8.mp3</a></td>

      <td></td>
    </tr>

    <tr>
      <td colspan="4"><a href="sounds/grossard/splash.mp3.tar.gz">All files in .tar.gz
      format</a></td>
    </tr>
  </table>

  <br />

  <table width="75%" style="margin-left:auto;margin-right:auto">
    <colgroup width="1*" span="4"></colgroup>
    <tr>
      <th colspan="4">MP3 format</th>
    </tr>

    <tr>
      <td><a href="sounds/grossard/splash/splash2.ogg">splash2.ogg</a></td>

      <td><a href="sounds/grossard/splash/splash3.ogg">splash3.ogg</a></td>

      <td><a href="sounds/grossard/splash/splash4.ogg">splash4.ogg</a></td>

      <td><a href="sounds/grossard/splash/splash5.ogg">splash5.ogg</a></td>
    </tr>

    <tr>
      <td><a href="sounds/grossard/splash/splash6.ogg">splash6.ogg</a></td>

      <td><a href="sounds/grossard/splash/splash7.ogg">splash7.ogg</a></td>

      <td><a href="sounds/grossard/splash/splash8.ogg">splash8.ogg</a></td>

      <td></td>
    </tr>

    <tr>
      <td colspan="4"><a href="sounds/grossard/splash.ogg.tar.gz">All files in .tar.gz
      format</a></td>
    </tr>
  </table>
  
  <br />
  
  <h2>KHangman Sounds</h2>

  <p>There are also some sounds suitable for <a href="/applications/language/khangman/">KHangman</a>:</p> 

  <table width="75%" style="margin-left:auto;margin-right:auto">
    <colgroup width="1*" span="4"></colgroup>
    <tr>
      <th colspan="3">MP3 format</th>
    </tr>

    <tr>
      <td><a href="sounds/grossard/khangman/click.mp3">click.mp3</a></td>

      <td><a href="sounds/grossard/khangman/found.mp3">found.mp3</a></td>

      <td><a href="sounds/grossard/khangman/loose.mp3">loose.mp3</a></td>
    </tr>
    
    <tr>
      <td><a href="sounds/grossard/khangman/new_game.mp3">new_game.mp3</a></td>

      <td><a href="sounds/grossard/khangman/not_found.mp3">not_found.mp3</a></td>

      <td><a href="sounds/grossard/khangman/splash.mp3">splash.mp3</a></td>

    </tr>
    
    <tr>
      <td colspan="3"><a href="sounds/grossard/khangman_mp3.tar.gz">All files in .tar.gz format</a></td>
    </tr>

  </table>
  
<?php include "footer.inc"; ?>