<?php
  $page_title = 'Contributed Learning Files';
  
  include ( "header.inc" );
?>
  <p>The learning files listed here are intended for use with <a href=
  "/applications/language/parley/">Parley</a>, <a href=
  "/applications/language/kvoctrain/">KVocTrain</a> and <a href=
  "/applications/language/kwordquiz/">KWordQuiz</a> which are both part of the KDE Education
  project. The files can also be used with <a href=
  "/applications/language/flashkard/">FlashKard</a>, a program that was included in earlier
  versions of KDE. To some extent these files also work with <a href=
  "/applications/language/khangman/">KHangMan</a> and <a href=
  "/applications/language/kanagram/">Kanagram</a>.</p>

  <h2>Creating kvtml Files</h2>

  <ul>
    <li>You can create a kvtml file using <a href=
    "/applications/language/kwordquiz/">KWordQuiz</a>. You add the back and front for each cards
    and then you save your data. It will be saved using the kvtml extension.</li>

    <li>KVocTrain is similar in that aspect but adds some complexity. Create and edit kvtml file
    using <a href="/applications/language/kvoctrain/">KVocTrain</a>.</li>

    <li>If you are on KDE4 already consider using <a href=
    "/applications/language/parley/">Parley</a>. Parley offers a wealth of editing possibilities
    without being as difficult to master as KVocTrain.</li>

    <li>Please see <a href="https://cdn.kde.org/edu/kvtml/sample.txt">here</a> for an example of a kvtml file if you want
    to create one with an editor.</li>
  </ul>

  <h2>Share Your Files</h2>

  <p>Please upload files you created to <a href=
  "http://kde-files.org/content/add.php">kde-files.org</a>. They will appear on the page and also
  show up in the download dialogs of the applications. The files can be found here:</p>

  <ul>
    <li><a href="http://kde-files.org/index.php?xcontentmode=687">Parley files on
    kde-files.org</a></li>

    <li><a href="http://kde-files.org/index.php?xcontentmode=694">KWordQuiz files on
    kde-files.org</a></li>

    <li><a href="http://kde-files.org/index.php?xcontentmode=693">KHangMan files on
    kde-files.org</a></li>
  </ul>

  <p>If you have questions or need help, ask the <a href="mailto:kde-edu@kde.org">the KDE-Edu
  mailing list</a>.</p>

  <h2>Areas:</h2>

  <ul>
    <li><a href="#vocab">Vocabulary / Languages</a></li>

    <li><a href="#exam">Exam Preparation</a></li>
  </ul><a name="vocab" id="vocab"></a>

  <h2>Vocabulary / Languages</h2><!-- language_1+2   filename   description   last_update -->

  <table class="kvtml">
    <tr>
      <th>Language Vocabulary</th>

      <th>Description</th>

      <th>Author</th>

      <th>Last Update</th>

      <th>Dowload</th>
    </tr><?php
        include "table_language.inc"
      ?>
  </table><a name="exam" id="exam"></a>

  <h2>Exam Preparation</h2>

  <table class="kvtml">
    <tr>
      <th>Test Preparation</th>

      <th>Description</th>

      <th>Author</th>

      <th>Last Update</th>

      <th>Dowload</th>
    </tr><?php
        include "table_exam.inc"
      ?>
  </table>

  <h2>More vocabulary files</h2>

  <p>For KDE 3 many files exist that work perfectly fine with the new versions of the programs.
  Have a look at <a href="../kvtml.php">Vocabulary files for KDE
  3</a>.</p>
  
<?php include "footer.inc"; ?>
