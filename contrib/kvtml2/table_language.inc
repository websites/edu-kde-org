    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/englisch.kvtml">English<br />
      German<br /></a></td>

      <td><b>en-de</b><br />
      <br />
      Green Line, 1696 Entries</td>

      <td>Marco Schlicht</td>

      <td>2009-09-06</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/englisch.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/en-esp-no-01.kvtml">English<br />
      Spanish<br />
      Norwegian (Bokmål)<br /></a></td>

      <td><b>en-es-no</b><br />
      <br />
      5 Lessons, 138 Entries</td>

      <td>Daikichi Komusubi</td>

      <td>2009-09-01</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/en-esp-no-01.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/GeorgianAlphabet.kvtml">Georgian<br />
      National<br /></a></td>

      <td><b>Georgian Alphabet</b><br />
      Includes Georgian Mkhedruli (მხედრული) alphabet, 2 common transliteration systems, and the
      IPA symbols for the most common phonetical realizations of the letters.<br />
      7 Lessons, 66 Entries</td>

      <td>Ryan Smith</td>

      <td>2009-08-13</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/GeorgianAlphabet.kvtml"><img src="../../images/icons/parley_22.png"
      alt="download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/cat-es-ger.kvtml">Catalan<br />
      German<br />
      Spanish<br /></a></td>

      <td><b>Català-Castellà-Aleman</b><br />
      Irregular German verbs<br />
      6 Lessons, 388 Entries</td>

      <td>Matthias Brugger</td>

      <td>2009-08-13</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/cat-es-ger.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/danish.kvtml">Danish<br />
      English<br /></a></td>

      <td><b>English - Danish</b><br />
      <br />
      1 Lessons, 2000 Entries</td>

      <td>Sean Bronée</td>

      <td>2009-08-13</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/danish.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/dansk4000.kvtml">English<br />
      Danish<br /></a></td>

      <td><b>Dansk</b><br />
      <br />
      1 Lessons, 12762 Entries</td>

      <td>Niall Mooney</td>

      <td>2009-08-13</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/dansk4000.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/de-uk-es-ar2.kvtml">Russian<br />
      German<br />
      English<br />
      Arabic<br />
      Spanish<br /></a></td>

      <td><b>en_de_es_ar</b><br />
      The start of an extensive collection in four languages. Please note that it's not finished
      and contact Florian in case you find mistakes. mail AT floyd-online DOT com<br />
      35 Lessons, 6432 Entries</td>

      <td>Florian Schmid (En, De, Ar, Ru); Falk Dechent (Ru)</td>

      <td>2008-04-26</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/de-uk-es-ar2.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/de_irregular_verbs.kvtml">Verb<br />
      español<br />
      Präteritum, Partizip<br /></a></td>

      <td><b>deutsche unregelmäβige Verben, spanische Übersetzung</b><br />
      some irregular verbs for A2, and spanish translation<br />
      13 Lessons, 202 Entries</td>

      <td>René Aedo</td>

      <td>2009-08-13</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/de_irregular_verbs.kvtml"><img src="../../images/icons/parley_22.png"
      alt="download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/el-en.kvtml">Greek<br />
      English<br /></a></td>

      <td><b>Greek.kvtml</b><br />
      <br />
      25 Lessons, 1705 Entries</td>

      <td>sky croeser</td>

      <td>2009-01-12</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/el-en.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/en-de-es-ar-ru.tar.bz2">Russian<br />
      German<br />
      English<br />
      Arabic<br />
      Spanish<br /></a></td>

      <td><b>en de es ar ru</b><br />
      The start of an extensive collection in four languages. Contact Florian (mail AT floyd-online
      DOT com) if you find mistakes. Includes pictures.<br />
      36 Lessons, 6429 Entries</td>

      <td>Florian Schmid (En, De, Es, Ar, Ru); Falk Dechent (Ru); Mathias Krüger (Ru)</td>

      <td>2007-12-11</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/en-de-es-ar-ru.tar.bz2"><img src="../../images/icons/parley_22.png"
      alt="download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/en-nb.kvtml">Norwegian Bokmål<br />
      English<br /></a></td>

      <td><b>Norwegian Bokmål</b><br />
      Should be editted by others for better and more accurate results and learning.There are more
      kind of times of the verbs in norwegian than those who appear here.<br />
      2 Lessons, 120 Entries</td>

      <td>Stian S. D.</td>

      <td>2009-01-12</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/en-nb.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/graecum.kvtml">Greek<br />
      German<br /></a></td>

      <td><b>Graecum (German - Ancient Greek)</b><br />
      No spiritus asper/lenis (didn't work on my machine)<br />
      9 Lessons, 5594 Entries</td>

      <td>Frank C. Eckert</td>

      <td>2007-10-18</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/graecum.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/kanji-english.tar.bz2">English<br />
      Kanji<br /></a></td>

      <td><b>Kanji</b><br />
      This document contains a subset of KANJIDIC, namely the 1945 kanji (jouyou kanji) taught in
      Japanese school, grades 1 to 6, and in secondary school. It was generated from Jim Breen's
      KANJIDIC (http://www.csse.monash.edu.au/~jwb/kanjidic.html) by Tomas Åkesson on 26 May 2008.
      This file is distributed in accordance with the licence provisions of the Electronic
      Dictionaries Research Group. See http://www.csse.monash.edu.au/~jwb/kanjidic.html and
      http://www.edrdg.org/<br />
      46 Lessons, 3890 Entries</td>

      <td>Jim Breen (original author of KANJIDIC). Tomas Åkesson (conversion into kvtml).</td>

      <td>2008-05-28</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/kanji-english.tar.bz2"><img src="../../images/icons/parley_22.png"
      alt="download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/latinum.kvtml">German<br />
      Latin<br /></a></td>

      <td><b>Salve</b><br />
      <br />
      9 Lessons, 6314 Entries</td>

      <td>Frank C. Eckert</td>

      <td>2007-10-18</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/latinum.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/language/radicals.kvtml">Pinyin (Mandarin)<br />
      Jyutping (Cantonese)<br />
      English<br />
      Chinese<br /></a></td>

      <td><b>Untitled</b><br />
      <br />
      12 Lessons, 478 Entries</td>

      <td></td>

      <td></td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/language/radicals.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>
