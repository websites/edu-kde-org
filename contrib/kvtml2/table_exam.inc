
    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/18th%20Century%20Prime%20Ministers.kvtml">Date<br /></a></td>

      <td><b>18th Century Prime Ministers</b><br />
      <br />
      1 Lessons, 32 Entries</td>

      <td>Stefan Carney</td>

      <td>2009-01-14</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/18th%20Century%20Prime%20Ministers.kvtml"><img src=
      "../../images/icons/parley_22.png" alt="download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/19th%20Century%20Prime%20Ministers.kvtml">Date<br /></a></td>

      <td><b>19th Century Prime Ministers</b><br />
      <br />
      1 Lessons, 66 Entries</td>

      <td>Stefan Carney</td>

      <td>2009-01-14</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/19th%20Century%20Prime%20Ministers.kvtml"><img src=
      "../../images/icons/parley_22.png" alt="download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/20th%20Century%20Prime%20Ministers.kvtml">Date<br /></a></td>

      <td><b>20th Century Prime Ministers</b><br />
      <br />
      1 Lessons, 52 Entries</td>

      <td>Stefan Carney</td>

      <td>2009-01-14</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/20th%20Century%20Prime%20Ministers.kvtml"><img src=
      "../../images/icons/parley_22.png" alt="download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/CA%20VDV%20Cert.kvtml">English<br />
      American English<br /></a></td>

      <td><b>CA - Voice Data Video Certification</b><br />
      <br />
      3 Lessons, 330 Entries</td>

      <td>Scott Aldinger</td>

      <td>2009-02-27</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/CA%20VDV%20Cert.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/Monarchs%20of%20England.kvtml">Date<br /></a></td>

      <td><b>Monarchs of England</b><br />
      <br />
      1 Lessons, 136 Entries</td>

      <td>Stefan Carney</td>

      <td>2009-01-14</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/Monarchs%20of%20England.kvtml"><img src=
      "../../images/icons/parley_22.png" alt="download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/SI_prefixes.kvtml">10^x<br /></a></td>

      <td><b>SI Prefixes</b><br />
      <br />
      1 Lessons, 40 Entries</td>

      <td>Erlend Hamberg</td>

      <td>2009-08-13</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/SI_prefixes.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/chimie.kvtml">Element Symbol<br /></a></td>

      <td><b>Chimie</b><br />
      <br />
      1 Lessons, 82 Entries</td>

      <td>Alexandre Mattenet</td>

      <td>2009-01-12</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/chimie.kvtml"><img src="../../images/icons/parley_22.png" alt="download"
      width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/lrc-ab-2007-10-01.kvtml">Fragen<br />
      Antworten<br /></a></td>

      <td><b>Long Range Certificate (LRC)</b><br />
      <br />
      5 Lessons, 224 Entries</td>

      <td>Malte Doersam</td>

      <td>2008-03-30</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/lrc-ab-2007-10-01.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/pyro.kvtml">Fragen<br />
      Antworten<br /></a></td>

      <td><b>Fachkundenachweis für Seenotsignalmittel gemäß Sprengstoffrecht</b><br />
      <br />
      1 Lessons, 120 Entries</td>

      <td>Malte Doersam</td>

      <td>2008-05-28</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/pyro.kvtml"><img src="../../images/icons/parley_22.png" alt="download"
      width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/src-ab-2007-10-01.kvtml">Fragen<br />
      Antworten<br /></a></td>

      <td><b>Short Range Certificate (SRC)</b><br />
      <br />
      5 Lessons, 514 Entries</td>

      <td>Malte Doersam</td>

      <td>2008-03-30</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/src-ab-2007-10-01.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/ubi-ab-1.10.07.kvtml">Fragen<br />
      Antworten<br /></a></td>

      <td><b>UKW-Sprechfunkzeugnis (UBI)</b><br />
      <br />
      4 Lessons, 396 Entries</td>

      <td>Malte Doersam</td>

      <td>2008-03-30</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/ubi-ab-1.10.07.kvtml"><img src="../../images/icons/parley_22.png" alt=
      "download" width="22" height="22" /></a></td>
    </tr>

    <tr>
      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/ubi-erweiterung-auf-src-lrc-ab-1.10.07.kvtml">Fragen<br />
      Antworten<br /></a></td>

      <td><b>UKW-Sprechfunkzeugnis (UBI)</b><br />
      <br />
      3 Lessons, 396 Entries</td>

      <td>Malte Doersam</td>

      <td>2008-03-30</td>

      <td><a href="https://cdn.kde.org/edu/kvtml2/exam/ubi-erweiterung-auf-src-lrc-ab-1.10.07.kvtml"><img src=
      "../../images/icons/parley_22.png" alt="download" width="22" height="22" /></a></td>
    </tr>
