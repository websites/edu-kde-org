<?php
  $translation_file = "edu-kde-org";
  include_once( "functions.inc" );
  $page_title = i18n_var( 'Contributions' );
  include ( "header.inc" );
?>

<p><?php i18n( 'Here we present some contributions to our applications.' ); ?></p>

<p><?php i18n( 'All the files are distributed under free licenses. If not otherwise stated this is the GNU General Public License. You are encouraged to download and use the files according to their license.' ); ?></p>

<p><?php i18n( 'And you are even more encouraged to contribute your own files or improve the existing ones! Visit the <a href="../get_involved/">Get Involved page</a>.' ); ?></p>

<ul>
  <li><a href="kvtml.php"><?php i18n( 'Vocabulary Files' ); ?></a> <?php i18n( '(old KDE3 style)' ); ?></li>
  <li><a href="kvtml2.php"><?php i18n( 'Vocabulary Files' ); ?></a> <?php i18n( '(current KDE4 style)' ); ?></li>
  <li><a href="kturtle/"><?php i18n( 'Logo Scripts' ); ?></a></li>
  <li><a href="kig/"><?php i18n( 'Kig Macros' ); ?></a></li>
</ul>

<?php
  include "footer.inc";
?>
