<?php
  $site_root = "../";
  $page_title = "Kig FAQ";
  

  include( "header.inc" );
?>

<br />
<h3>Frequently Asked Questions</h3>
<p>This section is meant to answer frequently asked questions about Kig that I
  get from users. If you still have any questions or think a question is missing
  here, please let <a href="mailto:toscano.pino@tiscali.it">me</a> know.</p>

<?php
  $faq = new FAQ();

  $faq->addSection( "Compiling Kig" );

  $faq->addQuestion( "After correctly installing Kig on Mandrake or Red Hat, I
get an error saying that Kig is missing certain libraries.",
      "Kig&#39;s functionality is implemented as a KPart, a loadable library.
This allows it to make use of the KDE architecture for various features.
However, the Kig program needs to know where to find its library, and to do
that, it relies on KDE being properly installed and set up. Unfortunately, this
doesn&#39;t seem to be the case on Mandrake or Red Hat.</p>\n
<p>I&#39;m aware of two workarounds:</p>\n
<ul type=\"disc\">\n
<li>Uninstall and compile Kig manually typing <code>./configure
--prefix=/usr</code>, <code>make</code> and then <code>su -c &quot;make
install&quot;</code>.</li>\n
<li>type <code>export KDEDIRS=/usr/local; kbuildsycoca; kig</code> in a shell.
This way, Kig should load correctly.</li>\n
</ul>\n
<p>I think the proper fix would be something like adding to
<code>kglobalrc</code> a section</p>\n
<pre>
[Directories]
prefixes=/usr/local,/usr
</pre>\n
<p>I&#39;m not too sure of that though..." );

  $faq->addSection( "Using Kig" );

  $faq->addQuestion( "How can I import a macro file?",
      "Once you have a macro file, you can import it into Kig using one of
these instructions (tailored for a typical Linux system):</p>
<p><b>Global installation</b> (requires the root privilegies), install a macro
for every Kig users on that machine: place it in
\$KDEDIR/share/apps/kig/builtin-macros/. The new macro(s) will be available in
Kig the next time you start Kig.</p>
<p><b>Local installation</b>, install a macro only for the user which installed
it: in Kig, select Types->Manage Types, click on the Import button, select the
macro file(s) you want to import, and you're done.</p>
<p>You can find some Kig macros in the <a href=\"macros.php\">Macro
Repository</a>.</p>");
  $faq->addQuestion( "Is it possible to edit a Python script? How?",
      "It is possible with Kig 0.10.6 (shipped with KDE 3.5.4) and above
versions. To edit a Python script, just right click on the object produced by
the script, and select &quot;Edit Script...&quot;. Then edit the script and
a click on Finish will apply your changes.");

  $faq->show();
?>

<?php
  include( "footer.inc" );
?>
