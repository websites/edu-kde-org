<?php
  $site_root = "../";
  $page_title = 'Rocs';
  
  include ( "header.inc" );
?>

<?php
  include("rocs.inc");
  $appinfo->showIconAndCopyright();
?>

<br />
<div id="quicklinks">
[
  <a href="#description">Description</a> |
  <a href="#news">Latest news</a> 
]
</div>

<center>
<a href="pics/rocs-full.png"><img border="0" width="480" height="332" src="pics/rocs-full_480.png"></a>
 </center>

<h3><a name="description">Description</a></h3>

<p>Rocs aims to be a Graph Theory IDE for helping professors to show the
results of a graph algorithm and also helping students to do the
algorithms.</para>

<para>Rocs has a scripting module, done in Qt Script, that interacts with the
drawn graph and every change in the graph with the script is reflected on the drawn one.</p>

<p>Rocs' main features are:
<ul>
  <li>Support for several graphs</li>
  <li>Automated graphs</li>
  <li>Oriented graphs</li>
  <li>Script support (Qt Script)</li>
  <li>Qt Script debugging</li>
</ul>
</p>

<?php
  kde_general_news("./news.rdf", 10, true);
 ?>

<br />
<br />

<hr width="30%" align="center" />
<p>
Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>
<?php
  include "footer.inc";
?>
