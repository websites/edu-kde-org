<?php
  $page_title = "How to obtain Rocs";
  $site_root = "../";
  
  include( "header.inc" );
?>

<p>
Rocs will be included with your distribution in the kdeedu package with KDE 4.4.0.
<br />
Latest stable version is in SVN trunk/KDE/kdeedu, in the kdeedu module and will be shipped with KDE 4.4.0.
</p>


<p>For advanced users who want to build Rocs from svn, here are the instructions:</p>
<?php
  show_obtain_instructions( "Rocs", "trunk", "false" );
?>


<br />
<hr width="30%" align="center" />
<p>Author: Anne-Marie Mahfouf<br />
Last update: <?php echo date ("Y-m-d", filemtime(__FILE__)); ?>
</p>

<?php
  include "footer.inc";
?>


