<?php
  $translation_file = "edu-kde-org";
  require_once( "functions.inc" );
  $page_title = i18n_noop( "Creating Parley Themes" );
  include "header.inc"; 
?>

<img src="../images/parley_theme.png" style="width:200px;height:432px;float:right;" />
<p><?php i18n( 'To create a Parley theme you mainly have to draw or better redraw a SVG file (scalable vector graphics). We suggest to use <a href="http://www.inkscape.org/">Inkscape</a> for this.' ); ?></p>

<p><?php i18n( 'Download the <a href="https://projects.kde.org/projects/kde/kdeedu/parley/repository/revisions/master/changes/themes/theme_reference.svgz">theme_reference.svgz</a>. 
Once you have opened it you will find a tutorial how to proceed.' ); ?></p>

<br style="clear:right;" />

<?php 
  include "footer.inc"; 
?>
