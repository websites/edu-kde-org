<?php
  $site_root = "../";
  $page_title = "First KDE-Edu Meeting - Paris - 1st& 2nd December 2007";
  

  include( "header.inc" );
?>

<p><a href="http://www.mandriva.com" title="Mandriva"><img src="logo.png" width="272" height="68" alt="Mandriva"/></a></p>
<p><b>"En route" for KDE 4.0!</b></p>
<p>Hosted by <a href="http://www.mandriva.com">Mandriva</a>, sponsored by <a href="http://ev.kde.org">KDE eV</a> for travel and lodging, <b>the first KDE-Edu meeting</b>, 1st and 2nd December 2007, aimed to polish educational applications for the KDE 4.0 release and to start planning KDE 4.1. Debugging, testing, exchanging ideas, shaping libraries, getting to know everyone was the main focus of the meeting.</p>
<p><b>Agenda: </b><a href="http://wiki.30doradus.org/index.php/Agenda">http://wiki.30doradus.org/index.php/Agenda</a></p>
<p><b>Participants: </b><a href="http://wiki.30doradus.org/index.php/Travel">http://wiki.30doradus.org/index.php/Travel</a></p>
<p><b>KDE-Edu Roadmap for KDE 4.1</b> quickly put together by annma: <a href="../../presentations/roadmap.pdf">pdf file</a><br />
<a href="../../presentations/roadmap.odp">ODP file</a></p>
<p><b>Press, Blogs and Pictures:</b> <a href="http://wiki.30doradus.org/index.php/Press_&amp;&amp;_Blogs">wiki page.</a></p>

<p><br/>
<b>Group Picture</b><br/><br/>
<img src="groupEdu2007.jpg" width="500" height="438" alt="the group"/>
</p>
<p><b>From left to right, second row</b>: Anne-Marie Mahfouf, Jeremy Whiting, Carsten Niehaus, Patrick Spendrin, Mauricio Piacentini, Vladimir Kuznetsov, Oliena Kuznetsova<br />
<b>From left to right, first row</b>: Frederik Gladhorn, Jure Repinc, Albert Astals Cid, Peter Murdoch, Johannes Simon, Benoît Jacob, Aleix Pol</p>
<br />
<?php
  include( "footer.inc" );
?>








