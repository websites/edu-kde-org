<?php
  $page_title = "KEduca - Screenshots";
  $site_root = "../";
  

  include( "header.inc" );

  $gallery = new EduGallery( "KEduca" );
  $gallery->addImage("pics/keducapart_sm.png", "pics/keducapart.png", 115, 113,  "[Screenshot]", "", "KEduca 3.5 part embedded in Konqueror");
  $gallery->show();

  include("footer.inc");
?>

