<!-- saved from:
file:/home/henrique/keduca_experiments/keduca.html -->
<HTML>
  <HEAD>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <TITLE>
      KEduca Icons
    </TITLE>
  </HEAD>
  <BODY BGCOLOR="#EEEEEA" LINK="#00aaaa" VLINK="#009900" ALINK="#00ff00" TEXT="#33336A">
    <H1>
      KEduca Icons
    </H1>
    <H2>
      Introduction
    </H2>
    <P>
       KEduca, part of the KDE-Edu package, is a program for dealing with interactive tests and exams. Currently, it relies on colors to tell you whether your answer is correct or not. However, this is bad accessibility-wise, since color-blind people might not be able to distinguish between the colors used (green for correct answers and red for incorrect answers. Therefore, new images are needed. 
    </P>
    <H2>
      The current images
    </H2>
    <P>
       This are the current images KEduca is using: 
    </P>
    <DIV STYLE="text-align: center">
      <IMG SRC="pics/keduca_correct_64.png" ALT="Correct Answer Image">
       
      <CENTER>
        <EM>
          The image shown for correct answers
        </EM>
      </CENTER>
       
    </DIV>
    <DIV STYLE="text-align: center">
      <IMG SRC="pics/keduca_incorrect_64.png" ALT="Incorrect Answer Image">
       
      <CENTER>
        <EM>
          The image shown for incorrect answers
        </EM>
      </CENTER>
       
    </DIV>
    <DIV STYLE="text-align: center">
      <IMG SRC="pics/cr48-app-keduca.png" ALT="Application Icon">
       
      <CENTER>
        <EM>
          KEduca's Icons
        </EM>
      </CENTER>
       
    </DIV>
    <H2>
      What needs to be done
    </H2>
    <P>
       As explained above, the images for correct/incorrect answers need to be redone in order to eliminate all the problems. The application icon, which is similar to the other images, don't have accessibility problems, but might also be redone, to reflect the changes. There are also other images/icons in KEduca that receive some care. The only requirements for the images is that they don't rely on colors to pass information and that they fit nicely with KDE's default icon theme. 
    </P>
    <H2>
      I think I can help, what should I do
    </H2>
    <P>
       If you wish to help, please 
      <A HREF="mailto:henrique.pinto@kdemail.net">
        contact me
      </A>
      . In case more details are needed, I'll happily provide them. 
    </P>
    <P>
      Thank you! 
    </P>
  </BODY>
</HTML>
